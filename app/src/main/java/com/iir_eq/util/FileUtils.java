package com.iir_eq.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Environment;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;


public class FileUtils
{
	private static String mPath = Environment.getExternalStorageDirectory() + "/";
	private static String FILE_FOLD = "bes";
	public static String SPP_FILE_NAME = "spp";
	public static String BLE_FILE_NAME = "ble";
	public static String OTA_STATIC = "ota_static";//ota 统计记表
	public static String USB_OTA_FILE = "usb_ota.txt";
	public static String FLASH_CONTENT_DETAILS = "flash_content_details.txt";
	public static String OTA_INFO_REPORT = "ota_info_report.txt";


	/**
	 * 判断指定路径文件是否存在，如果不存在直接生成一个新的文件
	 *
	 * @param path ： 文件指定路径
	 */
	public static void isExist(String path)
	{
		File file = new File(path);
		// 判断文件夹是否存在,如果不存在则创建文件夹
		if (!file.exists())
		{
			synchronized (FileUtils.class)
			{
				file.mkdirs();
			}
		}
	}

	public static void updateMainPath(Context context){
		mPath = context.getExternalFilesDir(null) + "";
	}

	public static boolean isFileExist(String path)
	{
		File file = new File(path);
		return file.exists();
	}
	
	/**
	 * 获取本应用的数据存储路径
	 * @return
	 */
	public static String getFolderPath()
	{
		String pathString = mPath;
		isExist(pathString);
		pathString += "/";
		return pathString;
	}


	/**
	 * 删除程序本地文件夹中的文件，只需指定文件名称。
	 *
	 * @param fileName 如对应apk名称，对应软件包名称等等
	 */
	public static void deleteFile(String fileName)
	{
		File file = new File(getFolderPath() + fileName);
		if (file.exists())
		{
			file.delete();
		}
	}


	public static void writeTOfileAndActiveClear(String filename, String context)
	{
		String path = getFolderPath()+"BES/";
		isExist(path);
		path = path+"LogData/";
		isExist(path);
		File file = new File(path + filename);
		try
		{
			if (!file.exists())
			{
				file.createNewFile();
			}
			FileInputStream fis = new FileInputStream(file);
			long size = fis.available();
			fis.close();
			/**
			 * 当文件大小大于80MByte时，主动删除
			 */
			if (size >= 80000000)
			{
				file.delete();
				return;
			}

			FileOutputStream stream = new FileOutputStream(file, true);
			String temp = context + "\n";
			byte[] buf = temp.getBytes();
			stream.write(buf);
			stream.close();

		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String getStrTime(){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date curDate = new Date(System.currentTimeMillis());//获取当前时间
		String str  = formatter.format(curDate);
		return  str;
	}

	public static String  writeFlashContentfile()
	{
		Date dt = new Date();
		String str_time = getStrTime();
		String path = getFolderPath()+"BES/";
		isExist(path);
		path = path+"LogData/";
		isExist(path);
		path = path +str_time +""+ FLASH_CONTENT_DETAILS;
		File file = new File(path);
		try
		{
			if (!file.exists())
			{
				file.createNewFile();
			}
			FileInputStream fis = new FileInputStream(file);
			long size = fis.available();
			fis.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return path;
	}

	public static void deletefile(String path)
	{
		try {
			File file = new File(path);
			file.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String writeFlashContentfileAll()
	{
		String path = getFolderPath() + "BES/";
		isExist(path);
		path = path + "LogData/";
		isExist(path);
		path = path + "All" + FLASH_CONTENT_DETAILS;
		deletefile(path);
		File file = new File(path);
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			FileInputStream fis = new FileInputStream(file);
			long size = fis.available();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return path;
	}

	public static String writeOtaInfoReport()
	{
		String path = getFolderPath() + "BES/";
		isExist(path);
		path = path + "LogData/";
		isExist(path);
		path = path + OTA_INFO_REPORT;
		File file = new File(path);
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			FileInputStream fis = new FileInputStream(file);
			long size = fis.available();
			fis.close();
			/**
			 * 当文件大小大于80MByte时，主动删除
			 */
			if (size >= 80000000) {
				file.delete();
				return path;
			}

			FileOutputStream stream = new FileOutputStream(file, true);
			String temp = "\n"+"\n"+ getStrTime();
			stream.write(temp.getBytes("gbk"));
			stream.flush();
			stream.close();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return path;

	}

	
}
