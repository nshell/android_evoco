package com.iir_eq.util;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;

import org.json.JSONObject;

import java.util.Map;

public class backendLibrary {
    private final String officialServerDomain = "https://evocolabs.com/";
    public final String LOGIN_CHECK = "usr/logincheck/<token>/";
    public final String UPLOAD_AUDIOGRAM = "usr/upaudgrm/<token>/";
    public final String GET_AUDIOGRAM_LIST = "usr/getaudgrmlist/<token>/";
    public final String FIRMWARE_UPDATE = "app/fwupdate/<token>/";
    public final String GET_UER_INFO = "usr/getinfo/<token>/";
    public final String GET_GAIN_TABLE = "usr/getgain/<token>/";
    public final String UPDATE_USER_INFO = "usr/updateinfo/<token>/";
    public final String FIRMWARE_DOWNLOAD = "app/fwdownload/<token>/";
    public final String FIRMWARE_LOG_UPLOAD = "app/logupload/<token>/";

    private String defaultDomain;
    public backendLibrary(boolean usingOfficialServer){
        if (usingOfficialServer){
            defaultDomain=officialServerDomain;
        }else{
            String testServerDomain = "https://evocolabs.cn/";
            defaultDomain= testServerDomain;
        }

    }

    public backendLibrary(){
        this(false);
    }

    public String getFullUrl(String partUrl, String token){
        if(partUrl.substring(0,1)=="/"){
            partUrl = partUrl.substring(1);
        }
        String aimString = partUrl.replaceAll("<token>",token);
        aimString = defaultDomain + aimString;
        return aimString;
    }

    public String getFullUrl(String partUrl){
        String aimString=partUrl;
        if(aimString.substring(0,1)=="/"){
            aimString = aimString.substring(1);
        }
        aimString = defaultDomain + aimString;
        return aimString;
    }

    public String getFullUrl(String partUrl, String token, Map<String,String> params){
        String aimString = partUrl.replaceAll("<token>",token);
        aimString = defaultDomain + aimString;
        StringBuilder builder = new StringBuilder(aimString);
        boolean isFirst = true;
        for (String key : params.keySet()) {
            if (key != null && params.get(key) != null) {
                if (isFirst) {
                    isFirst = false;
                    builder.append("?");
                } else {
                    builder.append("&");
                }
                builder.append(key)
                        .append("=")
                        .append(params.get(key));
            }
        }
        return builder.toString();
    }




}
