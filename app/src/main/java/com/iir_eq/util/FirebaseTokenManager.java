package com.iir_eq.util;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;

public class FirebaseTokenManager {
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private FirebaseAuth.IdTokenListener mTokenListener;
    private String mToken;

    public FirebaseTokenManager(){
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mTokenListener = new FirebaseAuth.IdTokenListener() {
            @Override
            public void onIdTokenChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser currentUser = firebaseAuth.getCurrentUser();
                currentUser.getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {@Override
                    public void onComplete(@NonNull Task<GetTokenResult> task) {
                        if(task.isSuccessful()){
                            mToken = task.getResult().getToken();
                        }else{
                            //error
                        }
                    }
                });
            }
        };
    }

    public String getmToken() {
        return mToken;
    }
}
