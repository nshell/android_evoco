package com.iir_eq.util;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class FirebaseTokenHelper {
    private static FirebaseTokenManager instance;

    public static synchronized FirebaseTokenManager getInstance(){
        if (instance == null){
            instance = new FirebaseTokenManager();
        }
        return instance;
    }
}
