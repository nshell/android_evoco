package com.iir_eq.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;

import com.iir_eq.R;


public class ContactFragment extends Fragment {
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle){
        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        final EditText contact_text = (EditText)view.findViewById(R.id.contact_text);
        Button send_btn=(Button)view.findViewById(R.id.contact_send_btn);
        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str=contact_text.getText().toString();
                //TODO: send text to youwei email.
                Log.i("sendtext", str);
            }
        });
        ImageButton call_btn=(ImageButton)view.findViewById(R.id.call_btn);
        call_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: call someone.
                Log.i("call", "onClick:callbtn ");
            }
        });
        return view;
    }
}
