package com.iir_eq.ui.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;

import com.evocolabs.app.CmdLib;
import com.evocolabs.app.EvocoOneBleService;
import com.evocolabs.app.ServiceMessage;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.iir_eq.R;
import com.iir_eq.bluetooth.callback.Startpickfilecallback;
import com.iir_eq.util.FileUtils;
import com.iir_eq.util.FirebaseTokenHelper;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;
import androidx.core.app.ActivityCompat;

import android.os.Handler;
import android.os.Message;
import android.preference.DialogPreference;
import android.provider.Settings;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class evocoOneControl extends AppCompatActivity implements  Startpickfilecallback {

    private FirebaseAuth mAuth;
    private FirebaseUser fbUser;

    private final String TAG="EvocoOneControl";
    public static int STREAMING_MODE;
    public final static int STREAMING_BLUETOOTH=0;
    public final static int STREAMING_HEARINGAID=1;
    public static int DENOISE_MODE;
    public final static int DENOISE_POWERSAVING=0;
    public final static int DENOISE_AUTOMATIC=1;
    public final static int DENOISE_AGGRESSIVE=2;
    private static int running_services=0;

    private final static int BLE_CONNECTED = EvocoOneBleService.CONNECTION_STATUS_CONNECTED;
    private final static int BLE_CONNECTING = EvocoOneBleService.CONNECTION_STATUS_CONNECTING;
    private final static int BLE_DISCONNECTED = EvocoOneBleService.CONNECTION_STATUS_DISCONNECTED;
    private final static int BLE_SCANNING = EvocoOneBleService.CONNECTION_STATUS_SCANNING;
    private int serviceBleConnected = BLE_DISCONNECTED;

    private final static int DEVICE_MAX_VOLUME = 16;

    public Startpickfilecallback callactivityback;

    private static final int REQUEST_LOCATION_PERMISSION = 0x01;
    private  static final int  Request_Bluesetting = 0x12;

    private GestureDetector mDetector;
    private GestureDetector leftVolumeGestureDetector;
    private GestureDetector rightVolumeGestureDetector;
    private GestureDetector systemVolumeGestureDetector;

    private Handler mMsgHandler;
    private AudioManager mAudioMng;
    private HashMap maxVolume=new HashMap();
    private final int audioStreams[]={AudioManager.STREAM_ACCESSIBILITY,AudioManager.STREAM_DTMF,AudioManager.STREAM_NOTIFICATION,AudioManager.STREAM_ALARM,AudioManager.STREAM_MUSIC,AudioManager.STREAM_RING, AudioManager.STREAM_SYSTEM,AudioManager.STREAM_VOICE_CALL};

    private EvocoOneControlBroadcastReceiver mServiceBroadcastReceiver;
    private IntentFilter mServiceBroadcastFilter;

    private final static int MSG_START_SERVICE = 0x01;
    private final static int MSG_STOP_SERVICE =  0x02;
    private final static int MSG_CHECK_RUNNING_SERVICES = 0x03;


    @BindView(R.id.evoco_one_denoise_block_label) public TextView denoiseBlockLabel;
    @BindView(R.id.evoco_one_left_bat) public TextView leftBatteryText;
    @BindView(R.id.evoco_one_right_bat) public TextView rightBatteryText;
    @BindView(R.id.evoco_one_left_bat_icon) public ImageView leftBatteryIcon;
    @BindView(R.id.evoco_one_right_bat_icon) public ImageView rightBatteryIcon;

    @BindView(R.id.evoco_one_denoise_container) public ConstraintLayout denoiseBlockContainer;
    @BindView(R.id.evoco_one_denoise_powersaving_block) public LinearLayout modePowerSavingBlock;
    @BindView(R.id.evoco_one_denoise_powersaving_image) public ImageView modePowerSavingImage;
    @BindView(R.id.evoco_one_denoise_powersaving_label) public TextView modePowerSavingLabel;
    @BindView(R.id.evoco_one_denoise_automatic_block) public LinearLayout modeAutomaticBlock;
    @BindView(R.id.evoco_one_denoise_automatic_image) public ImageView modeAutomaticImage;
    @BindView(R.id.evoco_one_denoise_automatic_label) public TextView modeAutomaticLabel;

    @BindView(R.id.evoco_one_ha_volume_block_label) public TextView volumeBlockLablel;

    @BindView(R.id.evoco_one_left_volume_controller) protected ConstraintLayout leftVolumeController;
    @BindView(R.id.evoco_one_left_volume_container) protected ConstraintLayout leftVolumeContainer;
    @BindView(R.id.evoco_one_right_volume_controller) public ConstraintLayout rightVolumeController;
    @BindView(R.id.evoco_one_right_volume_container) public ConstraintLayout rightVolumeContainer;

    @BindView(R.id.evoco_one_control_system_volume_label) public TextView ystemVolumeLabel;
    @BindView(R.id.evoco_one_control_system_volume_container) public ConstraintLayout systemVolumeContainer;
    @BindView(R.id.evoco_one_control_system_volume_controller) public ConstraintLayout systemVolumeController;

    @BindView(R.id.evoco_one_control_unconnected_layout) public ConstraintLayout unconnectedContainer;
    @BindView(R.id.evoco_one_control_unconnected_status_image) public ImageView unconnectedImage;
    @BindView(R.id.evoco_one_control_unconnected_status_text) public TextView unconnectedText;

    @BindView(R.id.evoco_one_menu) public FloatingActionButton mMenuBtn;
    @BindView(R.id.evoco_one_control_stream_mode_switch) public FloatingActionButton mStreamSwitch;

    @BindView(R.id.evoco_one_control_bluebooth_mode_layout) public Group btLayout;
    @BindView(R.id.evoco_one_control_hearingaid_mode_layout) public Group haLayout;
    @BindView(R.id.evoco_one_control_battery_view_group) public Group battLayout;
    @BindView(R.id.evoco_one_control_unconnected_view_group) public Group unconnectLayout;


    private Animation mConnectingImageAnimation;

    private int left_volume=0;
    private int right_volume=0;
    private int left_battery=50;
    private int right_battery=50;
//
//
    @OnClick(R.id.evoco_one_control_stream_mode_switch)
    public void streamModeSwitch(){
        if (STREAMING_MODE==STREAMING_HEARINGAID){
            // Change Mode
            mStreamSwitch.setImageDrawable(getDrawable(R.drawable.streaming_3x));
            STREAMING_MODE=STREAMING_BLUETOOTH;
            //TODO: SEND SREAMING MODE SWITCH DATA HERE
            sendControl(EvocoOneBleService.TOGGLE_STREAMING_MODE);
        }else if (STREAMING_MODE==STREAMING_BLUETOOTH){
            mStreamSwitch.setImageDrawable(getDrawable(R.drawable.hearing_aids_3x));
            STREAMING_MODE=STREAMING_HEARINGAID;
            //TODO: SEND STREAMING MODE SWITCH DATA HERE
            sendControl(EvocoOneBleService.TOGGLE_STREAMING_MODE);
        }
        updateStreamingModeView();
    }

    @OnClick(R.id.evoco_one_logo)
    public void sendGetInfoMsg(){
        sendControl(EvocoOneBleService.CHECK_GENERAL_STATUS);
    }

    @OnClick(R.id.evoco_one_denoise_block_label)
    public void navToWelcome(){
        Intent mToWelcome = new Intent(this, evocoWelcome.class);
        startActivity(mToWelcome);
    }

    @OnClick(R.id.evoco_one_ha_volume_block_label)
    public void sendConnectControl(){
        if(serviceBleConnected == BLE_DISCONNECTED)sendControl(EvocoOneBleService.START_CONNECT);
    }

    @OnClick(R.id.evoco_one_menu)
    public void menuOnClick(){
        Intent mToMenuActivity = new Intent(this,evocoOneMenu.class);
        startActivity(mToMenuActivity);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.evoco_one_denoise_powersaving_block)
    public void selectPowerSaving(){
        if (DENOISE_MODE!=DENOISE_POWERSAVING) {
            sendControl(EvocoOneBleService.SET_DENOISE_MODE_PASSTHROUGH);
            updateDenoiseStatusView(DENOISE_POWERSAVING);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.evoco_one_denoise_automatic_block)
    public void selectAutomatic(View v){
        if (DENOISE_MODE!=DENOISE_AUTOMATIC) {
            sendControl(EvocoOneBleService.SET_DENOISE_MODE_AUTOMATIC);
            updateDenoiseStatusView(DENOISE_AUTOMATIC);
        }
    }

    @OnClick(R.id.evoco_one_control_unconnected_status_text)
    public void disconectTextClick(){
        sendControl(EvocoOneBleService.START_CONNECT);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseInit();
        setContentView(R.layout.activity_evoco_one_control);
        Toolbar toolbar = findViewById(R.id.evoco_one_toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ListenerInit();
        checkStatus();
        volumeInit();
        mServiceBroadcastFilter = new IntentFilter();
        mServiceBroadcastFilter.addAction(ServiceMessage.EVOCO_ONE_UI_ACTION);
        mServiceBroadcastReceiver = new EvocoOneControlBroadcastReceiver();
        this.registerReceiver(mServiceBroadcastReceiver,mServiceBroadcastFilter);

        mMsgHandler = new Handler(){
            @Override
            public void handleMessage(Message msg){
                Log.d(TAG, "handleMessage: " + msg.what);
                switch (msg.what){
                    case MSG_START_SERVICE:
                        Intent startServiceIntent = new Intent(evocoOneControl.this,EvocoOneBleService.class);
                        startService(startServiceIntent);
                        break;
                    case MSG_STOP_SERVICE:
                        sendControl(EvocoOneBleService.SET_STOP_SERVICE);
                        running_services=0;
                        break;
                    case MSG_CHECK_RUNNING_SERVICES:
                        Log.d(TAG, "mMsgHandler: "+"checking MSG");
                        if (running_services>1){
                            Log.d(TAG, "mMsgHandler: Multi Service, shut all.");
                            Message stopServiceMsg = new Message();
                            stopServiceMsg.what = MSG_STOP_SERVICE;
                            mMsgHandler.sendMessage(stopServiceMsg);
                            running_services = 1;
                            Message startServiceMsg = new Message();
                            startServiceMsg.what = MSG_START_SERVICE;
                            mMsgHandler.sendMessage(startServiceMsg);
                        } else if (running_services==0) {
                            Log.d(TAG, "mMsgHandler: No Service, Start One");
                            Message startServiceMsg = new Message();
                            startServiceMsg.what = MSG_START_SERVICE;
                            mMsgHandler.sendMessage(startServiceMsg);
                        }else{
                            Log.d(TAG, "mMsgHandler: Get Single Service, Send Message");
                            if (serviceBleConnected == BLE_CONNECTED){
                                sendControl(EvocoOneBleService.CHECK_GENERAL_STATUS);
                            }else if(serviceBleConnected == BLE_DISCONNECTED){
                                sendControl(EvocoOneBleService.START_CONNECT);
                            }
                        }
                        break;
                }
            }
        };

        initViewGroup();
    }

    @Override
    public void onDestroy(){
        Log.d(TAG, "onDestroy: ");
        unregisterReceiver(mServiceBroadcastReceiver);
        super.onDestroy();
    }

    @OnTouch(R.id.evoco_one_left_volume_container)
    public boolean leftVolumeTouch(View view, MotionEvent motionEvent) {
//        Log.i(TAG, "leftVolumeTouch: "+motionEvent.toString());
        leftVolumeGestureDetector.onTouchEvent(motionEvent);
        return false;
    }

    @OnTouch(R.id.evoco_one_right_volume_container)
    public boolean rightVolumeTouch(View view, MotionEvent motionEvent) {
//        Log.i(TAG, "rightVolumeTouch: "+motionEvent.toString());
        rightVolumeGestureDetector.onTouchEvent(motionEvent);
        return false;
    }

    @OnTouch(R.id.evoco_one_control_system_volume_container)
    public boolean systemVolumeTouch(View view, MotionEvent motionEvent) {
//        Log.i(TAG, "systemVolumeTouch: "+motionEvent.toString());
        systemVolumeGestureDetector.onTouchEvent(motionEvent);
        return false;
    }

    private void ListenerInit(){
        leftVolumeGestureDetector=new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX, float distanceY) {
                int leftVolumeHeight;
                int leftContainerHeight;
                leftVolumeHeight = leftVolumeController.getHeight();
                leftContainerHeight = leftVolumeContainer.getHeight();
                ViewGroup.LayoutParams leftControlParams = leftVolumeController.getLayoutParams();
                if (leftVolumeHeight + (int) distanceY < 1) {
                    leftControlParams.height = 1;
                } else if (leftVolumeHeight + (int) distanceY > leftContainerHeight) {
                    leftControlParams.height = leftContainerHeight;
                } else {
                    leftControlParams.height = leftVolumeHeight + (int) distanceY;
                }
                leftVolumeController.setLayoutParams(leftControlParams);
                int new_left_volume = (int) (leftVolumeHeight*1./leftContainerHeight * DEVICE_MAX_VOLUME);
                if (left_volume!=new_left_volume) {
                    left_volume = new_left_volume;
                    sendControl(EvocoOneBleService.SET_LEFT_VOLUME_VALUE,left_volume);
                }
                return true;
            }
        });
        rightVolumeGestureDetector=new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX, float distanceY) {
                int rightVolumeHeight;
                int rightContainerHeight;
                rightVolumeHeight = rightVolumeController.getHeight();
                rightContainerHeight = rightVolumeContainer.getHeight();
                ViewGroup.LayoutParams rightControlParams = rightVolumeController.getLayoutParams();
                if (rightVolumeHeight + (int) distanceY < 1) {
                    rightControlParams.height = 1;
                } else if (rightVolumeHeight + (int) distanceY > rightContainerHeight) {
                    rightControlParams.height = rightContainerHeight;
                } else {
                    rightControlParams.height = rightVolumeHeight + (int) distanceY;
                }
                rightVolumeController.setLayoutParams(rightControlParams);
                int new_right_volume = (int) (rightVolumeHeight*1./rightContainerHeight * 16);
                if (right_volume!=new_right_volume) {
                    right_volume = new_right_volume;
                    sendControl(EvocoOneBleService.SET_RIGHT_VOLUME_VALUE,right_volume);
                }
                return true;
            }
        });
        systemVolumeGestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX, float distanceY) {
                int systemVolumeHeight;
                int systemContainerHeight;
                systemVolumeHeight = systemVolumeController.getHeight();
                Log.d(TAG, "onScroll: systemVolumeHeight="+systemVolumeHeight);
                systemContainerHeight = systemVolumeContainer.getHeight();
                Log.d(TAG, "onScroll: systemContainerHeight="+systemContainerHeight);
                ViewGroup.LayoutParams systemControlParams = systemVolumeController.getLayoutParams();
                if (systemVolumeHeight + (int) distanceY < 1) {
                    systemControlParams.height = 1;
                } else if (systemVolumeHeight + (int) distanceY > systemContainerHeight) {
                    systemControlParams.height = systemContainerHeight;
                } else {
                    systemControlParams.height = systemVolumeHeight + (int) distanceY;
                }
                systemVolumeController.setLayoutParams(systemControlParams);
                int current_stream = getSystemStream();
                int maxVol = mAudioMng.getStreamMaxVolume(current_stream);
                int currentVol = mAudioMng.getStreamVolume(current_stream);
                Log.d(TAG, "onScroll: cuurentVol="+currentVol);
                int aimVol = (int) (systemControlParams.height*1./systemContainerHeight*maxVol);
                Log.d(TAG, "onScroll: aimVol="+aimVol);
                mAudioMng.setStreamVolume(current_stream,aimVol,0);
                return true;
            }
        });
    }

    private void initViewGroup(){
        Log.d(TAG, "initViewGroup: ");
        mConnectingImageAnimation = AnimationUtils.loadAnimation(this,R.anim.evoco_one_loading_rotation);
        mConnectingImageAnimation.setInterpolator(new LinearInterpolator());
        unconnectedImage.startAnimation(mConnectingImageAnimation);
    }

    private void updateVolumeView(int masterVol, int slaveVol){
        Log.d(TAG, "updateVolumeView with volume level: ");
        left_volume = masterVol;
        right_volume = slaveVol;
        updateVolumeView();
    }

    private void updateVolumeView(){
        Log.d(TAG, "updateVolumeView: ");
        if (serviceBleConnected==BLE_DISCONNECTED){
            return;
        }
        int masterVol = left_volume;
        int slaveVol = right_volume;
        if (STREAMING_MODE==STREAMING_HEARINGAID){
            int ContainerHeight;
            int aimHeight;

            /* Left/Master part */
            ContainerHeight = leftVolumeContainer.getHeight();
            ViewGroup.LayoutParams leftControlParams = leftVolumeController.getLayoutParams();
            aimHeight = (int) (masterVol*1./DEVICE_MAX_VOLUME * ContainerHeight);
            if (aimHeight==0){
                aimHeight=1;
            }
            leftControlParams.height = aimHeight;
            leftVolumeController.setLayoutParams(leftControlParams);

            /* Right/Slave part */
            ContainerHeight = rightVolumeContainer.getHeight();
            ViewGroup.LayoutParams rightControlParams = rightVolumeController.getLayoutParams();
            aimHeight = (int) (slaveVol*1./DEVICE_MAX_VOLUME * ContainerHeight);
            if (aimHeight==0){
                aimHeight=1;
            }
            rightControlParams.height = aimHeight;
            rightVolumeController.setLayoutParams(rightControlParams);
        }else{
            int ContainerHeight;
            Log.d(TAG, "updateVolumeView: controller visibility" + systemVolumeController.getVisibility());
            ContainerHeight = systemVolumeContainer.getHeight();
            int current_stream = getSystemStream();
            int currentVol = mAudioMng.getStreamVolume(current_stream);
            int maxVol = mAudioMng.getStreamMaxVolume(current_stream);
            ViewGroup.LayoutParams systemControlParams = systemVolumeController.getLayoutParams();
            int aimHeight = (int) (currentVol * 1. / maxVol * ContainerHeight);
            if (aimHeight==0){
                aimHeight=1;
            }
            systemControlParams.height = aimHeight;
            systemVolumeController.setLayoutParams(systemControlParams);
        }
    }

    private void updateBatteryView(int masterBattery, int slaveBattery){
        Log.d(TAG, "updateBatteryView with battery level: ");
        left_battery = masterBattery;
        right_battery = slaveBattery;
        updateBatteryView();
    }

    private void updateBatteryView(){
        Log.d(TAG, "updateBatteryView: ");
        if (serviceBleConnected == BLE_DISCONNECTED){
            return;
        }
        leftBatteryText.setText(left_battery+"%");
        rightBatteryText.setText(right_battery+"%");
        if(left_battery<40){
            leftBatteryIcon.setImageResource(R.drawable.low_batt);
        }else if(left_battery<70){
            leftBatteryIcon.setImageResource(R.drawable.mid_batt);
        }else{
            leftBatteryIcon.setImageResource(R.drawable.high_batt);
        }
        if(right_battery<40){
            rightBatteryIcon.setImageResource(R.drawable.low_batt);
        }else if(right_battery<70){
            rightBatteryIcon.setImageResource(R.drawable.mid_batt);
        }else{
            rightBatteryIcon.setImageResource(R.drawable.high_batt);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void updateDenoiseStatusView(int denoise_status){
        Log.d(TAG, "updateDenoiseStatusView with denoise_status: ");
        if (serviceBleConnected == BLE_DISCONNECTED){
            return;
        }
        switch (denoise_status){
            case DENOISE_AUTOMATIC:
                if(DENOISE_MODE!= DENOISE_AUTOMATIC){
                    DENOISE_MODE = DENOISE_AUTOMATIC;
                    updateDenoiseStatusView();
                    Log.d(TAG, "updateDenoiseStatusView: Updated View To " + "DENOISE_AUTOMATIC");
                }else{
                    Log.d(TAG, "updateDenoiseStatusView: DENOISE_MODE not changed, not updating the view");
                }
                break;
            case DENOISE_POWERSAVING:
                if(DENOISE_MODE!= DENOISE_POWERSAVING){
                    DENOISE_MODE = DENOISE_POWERSAVING;
                    updateDenoiseStatusView();
                    Log.d(TAG, "updateDenoiseStatusView: Updated View To " + "DENOISE_POWERSAVING");
                }else{
                    Log.d(TAG, "updateDenoiseStatusView: DENOISE_MODE not changed, not updating the view");
                }
                break;
            default:
                android.util.Log.e(TAG, "updateDenoiseStatusView: denoise_status DOES NOT MEET ANY DENOISE MODE");
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void updateDenoiseStatusView(){
        Log.d(TAG, "updateDenoiseStatusView: ");
        if (serviceBleConnected == BLE_DISCONNECTED){
            updateUnconnectedView();
            return;
        }
        switch (DENOISE_MODE){
            case DENOISE_AUTOMATIC:
                modePowerSavingBlock.setBackground(getDrawable(R.drawable.evoco_one_inactive_denoise_mode));
                modePowerSavingImage.setImageDrawable(getDrawable(R.drawable.power_saving_grey_3x));
                modePowerSavingLabel.setTextColor(getColor(R.color.evocoOneDenoiseInactive));
                modeAutomaticBlock.setBackground(getDrawable(R.drawable.evoco_one_active_denoise_mode));
                modeAutomaticImage.setImageDrawable(getDrawable(R.drawable.pinwheel_3x));
                modeAutomaticLabel.setTextColor(getColor(R.color.evocoOneDenoiseActive));
                break;
            case DENOISE_POWERSAVING:
                modePowerSavingBlock.setBackground(getDrawable(R.drawable.evoco_one_active_denoise_mode));
                modePowerSavingImage.setImageDrawable(getDrawable(R.drawable.power_saving_3x));
                modePowerSavingLabel.setTextColor(getColor(R.color.evocoOneDenoiseActive));
                modeAutomaticBlock.setBackground(getDrawable(R.drawable.evoco_one_inactive_denoise_mode));
                modeAutomaticImage.setImageDrawable(getDrawable(R.drawable.pinwheel_grey_3x));
                modeAutomaticLabel.setTextColor(getColor(R.color.evocoOneDenoiseInactive));
                break;
        }
    }


    private void updateStreamingModeView(){
        if (serviceBleConnected == BLE_DISCONNECTED){
            updateUnconnectedView();
            return;
        }
        Log.d(TAG, "updateStreamingModeView: "+STREAMING_MODE);
        int btLayoutIds[] = btLayout.getReferencedIds(); // Bluetooth Layout IDs
        int haLayoutIds[] = haLayout.getReferencedIds(); // Hearing Aid Layout IDs
        if (STREAMING_MODE==STREAMING_BLUETOOTH){
//            for (int i=0; i<btLayoutIds.length;i++){
//                findViewById(btLayoutIds[i]).setVisibility(VISIBLE);
//            }
//            for (int i=0; i<haLayoutIds.length;i++){
//                findViewById(haLayoutIds[i]).setVisibility(GONE);
//            }
            btLayout.setVisibility(View.VISIBLE);
            haLayout.setVisibility(View.GONE);
        }else if(STREAMING_MODE==STREAMING_HEARINGAID){
//            for (int i=0; i<btLayoutIds.length;i++){
//                findViewById(btLayoutIds[i]).setVisibility(GONE);
//            }
//            for (int i=0; i<haLayoutIds.length;i++){
//                findViewById(haLayoutIds[i]).setVisibility(VISIBLE);
//            }
            btLayout.setVisibility(View.GONE);
            haLayout.setVisibility(View.VISIBLE);
            systemVolumeController.setVisibility(VISIBLE);
        }
        updateVolumeView();
    }

    private void updateUnconnectedView(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (serviceBleConnected) {
                    case BLE_CONNECTED:
                        unconnectedText.setText("Connected, waiting for device status message");
                        btLayout.setVisibility(GONE);
                        haLayout.setVisibility(VISIBLE);
                        unconnectLayout.setVisibility(GONE);
                        battLayout.setVisibility(VISIBLE);
                        mMsgHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
//                                sendControl(EvocoOneBleService.CHECK_GENERAL_STATUS);
                            }
                        }, 500);
                        break;
                    case BLE_CONNECTING:
                        unconnectedText.setText("Connecting, please wait for a while");
                        btLayout.setVisibility(GONE);
                        haLayout.setVisibility(GONE);
                        battLayout.setVisibility(GONE);
                        unconnectLayout.setVisibility(VISIBLE);
                        unconnectedImage.setVisibility(VISIBLE);
                        unconnectedImage.clearAnimation();
                        unconnectedImage.startAnimation(mConnectingImageAnimation);
                        break;
                    case BLE_DISCONNECTED:
                        btLayout.setVisibility(GONE);
                        haLayout.setVisibility(GONE);
                        battLayout.setVisibility(GONE);
                        unconnectLayout.setVisibility(VISIBLE);
                        unconnectedContainer.setVisibility(VISIBLE);
                        unconnectedText.setText("Disconnected, tap to connect the device");
                        unconnectedImage.clearAnimation();
                        unconnectedImage.setVisibility(View.INVISIBLE);
                        break;
                    case BLE_SCANNING:
                        btLayout.setVisibility(GONE);
                        haLayout.setVisibility(GONE);
                        battLayout.setVisibility(GONE);
                        unconnectLayout.setVisibility(VISIBLE);
                        unconnectedText.setText("Scanning for devices");
                        unconnectedImage.setVisibility(VISIBLE);
                        unconnectedImage.clearAnimation();
                        unconnectedImage.startAnimation(mConnectingImageAnimation);
                        break;
                    default:
                        Log.d(TAG, "run: "+serviceBleConnected);
                        break;
                }
            }
        });
    }

    private void checkStatus(){
        Log.d(TAG, "checkStatus: ");
        STREAMING_MODE=STREAMING_HEARINGAID;
        updateStreamingModeView();
        DENOISE_MODE=DENOISE_AUTOMATIC;
    }

    // SYSTEM VOICE CONTROL
    private void volumeInit(){
        Log.d(TAG, "volumeInit: ");
        int systemContainerHeight=systemVolumeContainer.getHeight();
        ViewGroup.LayoutParams systemControlParams = systemVolumeController.getLayoutParams();
        mAudioMng = (AudioManager) getSystemService(AUDIO_SERVICE);
        for (int i=0; i<audioStreams.length;i++){
            maxVolume.put(audioStreams[i],mAudioMng.getStreamMaxVolume(audioStreams[i]));
        }
    }

    private int getSystemStream(){
        Log.d(TAG, "getSystemStream: ");
        int audioMode = mAudioMng.getMode();
        int currentStream = mAudioMng.USE_DEFAULT_STREAM_TYPE;
        switch (audioMode){
            case AudioManager.MODE_IN_CALL:
                currentStream = mAudioMng.STREAM_VOICE_CALL;
                break;
            case AudioManager.MODE_NORMAL:
                currentStream = mAudioMng.STREAM_MUSIC;
                break;
            case AudioManager.MODE_RINGTONE:
                currentStream = mAudioMng.STREAM_RING;
                break;
            default:
                break;
        }
        return currentStream;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event){
        int action = event.getAction();
        switch (action){
            case KeyEvent.ACTION_DOWN:
                updateVolumeView();
                break;
            case KeyEvent.ACTION_UP:
                updateVolumeView();
                break;
            default:
                break;
        }
        return super.dispatchKeyEvent(event);
    }

    @Override
    protected void onStart(){
        super.onStart();
        Log.d(TAG, "onStart: ");
        fbUser = mAuth.getCurrentUser();
        if (fbUser==null){
            Intent toWelcome = new Intent(this, evocoWelcome.class);
            startActivity(toWelcome);
        }else{
            Log.i(TAG, "onStart: "+fbUser.toString());
            Log.i(TAG, "onStart: "+fbUser.getUid());
        }
        initLocation();
        initPermission();
        initService();
        updateUnconnectedView();
    }

    private void initService(){
        Log.d(TAG, "initService: ");
        // Check if the service is running
        running_services = 0;
        sendControl(EvocoOneBleService.CHECK_SERVICE_ALIVE);
        Message msg = new Message();
        msg.what = MSG_CHECK_RUNNING_SERVICES;
        mMsgHandler.sendMessageDelayed(msg,500);
    }

    private void firebaseInit(){
        mAuth = FirebaseAuth.getInstance();
        FirebaseTokenHelper.getInstance();
    }

    private boolean initPermission() {
            Log.d(TAG, "initPermission: ");
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (ActivityCompat.shouldShowRequestPermissionRationale(evocoOneControl.this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.ble_location_permission_tip)
                    .setNegativeButton(R.string.no, null)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            ActivityCompat.requestPermissions(evocoOneControl.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_PERMISSION);
                        }
                    }).create().show();
        } else {
            ActivityCompat.requestPermissions(evocoOneControl.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_PERMISSION);
        }
        return false;
    }

    private boolean initLocation() {
        Log.d(TAG, "initLocation: ");
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        LocationManager manager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return true;
        }
        new AlertDialog.Builder(this).setMessage(R.string.ble_gps_enable_tip)
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent startMain = new Intent(Intent.ACTION_MAIN);
                        startMain.addCategory(Intent.CATEGORY_HOME);
                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(startMain);
                        System.exit(0);
                    }
                })
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        callactivityback=evocoOneControl.this;
                        callactivityback.startset(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    }
                }).create().show();
        return false;
    }

    @Override
    public void PickFileCallback(int request) {}

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: ");
        if (requestCode == Request_Bluesetting ) {
            int permissionFlag=0;
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "onActivityResult: "+"location permission denied");
                permissionFlag=1;
            }
            LocationManager manager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                Log.d(TAG, "onActivityResult: "+"gps permission denied");
                permissionFlag=1;
            }
            if(permissionFlag!=0){
                Toast.makeText(this,"Permission Denied, Quiting",Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onActivityResult: "+"some permission denied");
            }
        }
    }

    @Override
    public void startset(String setting) {
        Log.d(TAG, "startset: ");
        startActivityForResult(new Intent(setting),Request_Bluesetting);
    }

    private void sendControl(int controlCode){
        Log.d(TAG, "sendControl: ");
        Intent broadCastIntent= new Intent();
        broadCastIntent.setAction(EvocoOneBleService.EVOCO_ONE_BLE_SERVICE_ACTION);
        broadCastIntent.putExtra(EvocoOneBleService.CONTROL_COMMAND_FIELD, controlCode);
        sendBroadcast(broadCastIntent);
    }
    private void sendControl(int controlCode, int value){
        Log.d(TAG, "sendControl with value: ");
        Intent broadCastIntent= new Intent();
        broadCastIntent.setAction(EvocoOneBleService.EVOCO_ONE_BLE_SERVICE_ACTION);
        broadCastIntent.putExtra(EvocoOneBleService.CONTROL_COMMAND_FIELD, controlCode);
        broadCastIntent.putExtra(EvocoOneBleService.VOLUME_VALUE_FIELD, value);
        sendBroadcast(broadCastIntent);
    }

    public class EvocoOneControlBroadcastReceiver extends BroadcastReceiver{
        // Update View with message from service
        @Override
        public void onReceive(Context context, Intent intent){
            int messageName = intent.getIntExtra("SERVICE_MESSAGE",0xFF);

            Log.d(TAG, "onReceive: get broadcast "+messageName);
            switch (messageName){
                case ServiceMessage.MSG_SERVICE_STATUS:
                    Log.d(TAG, "onReceive: get a running service response");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG, "run: running_services=" + evocoOneControl.this.running_services);
                            evocoOneControl.this.running_services++;
                            Bundle mBundle = intent.getExtras();
                            evocoOneControl.this.serviceBleConnected = mBundle.getInt("connection_status");
                        }
                    });
                    break;
                case ServiceMessage.MSG_GENERAL_INFO:
                    Bundle dataBundle = intent.getExtras();
                    int masterVolume = dataBundle.getInt("master_volume_level");
                    int slaveVolume = dataBundle.getInt("slave_volume_level");
                    int masterBattery = dataBundle.getInt("master_battery_level");
                    int slaveBattery = dataBundle.getInt("slave_battery_level");
                    int denoiseMode = dataBundle.getInt("denoise_mode");
                    int streamingMode = dataBundle.getInt("streaming_mode");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            serviceBleConnected=BLE_CONNECTED;
                            updateUnconnectedView();
                            switch (streamingMode){
                                case 0:
                                    STREAMING_MODE=STREAMING_HEARINGAID;
                                    break;
                                case 1:
                                    STREAMING_MODE=STREAMING_BLUETOOTH;
                                    break;
                                default:
                                    android.util.Log.e(TAG,"STREAMING_MODE CODE DOESN'T MATCH ANY CASE");
                            }
                            updateVolumeView(masterVolume,slaveVolume);
                            Log.d(TAG, "run: masterBat: "+masterBattery+" ;slaveBat: "+slaveBattery);
                            updateBatteryView(masterBattery*10,slaveBattery*10);
                            switch(denoiseMode){
                                case 0:
                                    break;
                                case 1:
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        updateDenoiseStatusView(DENOISE_AUTOMATIC);
                                    }
                                    break;
                                case 2:
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        updateDenoiseStatusView(DENOISE_POWERSAVING);
                                    }
                                    break;
                                default:
                                    android.util.Log.e(TAG,"DENOISE_MODE CODE DOESN'T MATCH ANY CASE");
                                    break;
                            }
                            updateStreamingModeView();
                        }
                    });
                    break;
                case ServiceMessage.MSG_CONNECTION_STATUS:
                    Log.d(TAG, "onReceive - MSG_CONNECTION_STATUS: ");
                    Bundle mBundle = intent.getExtras();
                    int statusCode = mBundle.getInt("connection_status");
                    serviceBleConnected = statusCode;
                    updateUnconnectedView();
                    if(serviceBleConnected == BLE_CONNECTED)
                        mMsgHandler.postDelayed(()->{sendControl(EvocoOneBleService.CHECK_GENERAL_STATUS);},100);
                    break;
                default:
                    android.util.Log.e(TAG,"UNKOWN SERVICE_MESSAGE in EvocoOneControl");
            }
        }
    };
}
