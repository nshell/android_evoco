package com.iir_eq.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.evocolabs.app.EvocoOneBleService;
import com.evocolabs.app.ServiceMessage;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.iir_eq.R;
import com.iir_eq.util.backendLibrary;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class evocoOneOTA extends AppCompatActivity {

    @BindView(R.id.evoco_one_ota_loading) ImageView statusPic;
    @BindView(R.id.evoco_one_ota_info_text) TextView statusText;
    @BindView(R.id.evoco_one_ota_progress_text) TextView otaProgressText;

    private Animation myRotateLoading;
    private FirebaseAuth mAuth;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    public final static String TAG = "evocoOneOTA";
    private String current_fw_version = "";
    private String aim_fw_version = "";
    private Context context;
    private String mToken;
    private OtaBroadcastReceiver mBroadcastReceiver;
    private IntentFilter mIntentFilter;

    private final int NO_NEED_OTA = 0;
    private final int FETCH_FIRMWARE_FAILED = -1;
    private final int DOWNLOAD_FIRMWARE = 1;
    private final int DOWNLOAD_FINISH = 2;
    private final int DOWNLOAD_FAILED = -2;
    private final int START_OTA = 3;
    private final int GET_TOKEN_FAILED = -3;
    private final int OTA_SUCCESS = 4;
    private final int OTA_FAILED = -4;
    private final int CONNECT_SUCCESS = 5;
    private final int CONNECT_FAILED = -5;


    private int current_state = STATE_DISCONNECTED;
    protected static final int STATE_IDLE = OtaActivity.STATE_IDLE;
    protected static final int STATE_CONNECTING = OtaActivity.STATE_CONNECTING;
    protected static final int STATE_CONNECTED = OtaActivity.STATE_CONNECTED;
    protected static final int STATE_DISCONNECTING = OtaActivity.STATE_DISCONNECTING;
    protected static final int STATE_DISCONNECTED = OtaActivity.STATE_DISCONNECTED;
    protected static final int STATE_OTA_ING = OtaActivity.STATE_OTA_ING;
    protected static final int STATE_OTA_FAILED = OtaActivity.STATE_OTA_FAILED;
    protected static final int STATE_OTA_CONFIG = OtaActivity.STATE_OTA_CONFIG;
    protected static final int STATE_BUSY = OtaActivity.STATE_BUSY;
    protected static final int STATE_OTA_SUCCESS = OtaActivity.STATE_OTA_SUCCESS;



    private Handler uiThreadHandler = new Handler(){
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle bundle = msg.getData();
            switch (msg.what){
                case FETCH_FIRMWARE_FAILED:
                    Log.d(TAG, "handleMessage: FETCH_FIRMWARE_FAILED");
                    statusText.setText(R.string.evoco_one_ota_fetch_failed);
                    imageVisibleControl(View.INVISIBLE);
                    break;
                case DOWNLOAD_FINISH:
                    Log.d(TAG, "handleMessage: DOWNLOAD_FINISH");
                    editor.putString("fw_version",(String) msg.obj);
                    editor.commit();
                    break;
                case DOWNLOAD_FIRMWARE:
                    Log.d(TAG, "handleMessage: DOWNLOAD_FIRMWARE");
                    statusText.setText(R.string.evoco_one_ota_download_fw);
                    break;
                case DOWNLOAD_FAILED:
                    Log.d(TAG, "handleMessage: DOWNLOAD_FAILED");
                    statusText.setText(R.string.evoco_one_ota_download_failed);
                    imageVisibleControl(View.INVISIBLE);
                    break;
                case START_OTA:
                    Log.d(TAG, "handleMessage: START_OTA");
                    statusText.setText(R.string.evoco_one_ota_fw_ota);
                    break;
                case NO_NEED_OTA:
                    Log.d(TAG, "handleMessage: NO_NEED_OTA");
                    statusText.setText(R.string.evoco_one_ota_no_need_update);
                    imageVisibleControl(View.INVISIBLE);
                    break;
                case GET_TOKEN_FAILED:
                    Log.d(TAG, "handleMessage: GET_TOKEN_FAILED");
                    statusText.setText("No Net Work Available, Please Check Your Net Work");
                    imageVisibleControl(View.INVISIBLE);
                    break;
                case OTA_FAILED:
                    Log.d(TAG, "handleMessage: OTA_FAILED");
                    statusText.setText("OTA Failed, please try again");
                    imageVisibleControl(View.INVISIBLE);
                    break;
                case OTA_SUCCESS:
                    Log.d(TAG, "handleMessage: OTA_SUCCESS");
                    statusText.setText("OTA Succeeded. Please reboot your EVOCOs");
                    editor.putString("fw_version",aim_fw_version);
                    editor.commit();
                    imageVisibleControl(View.INVISIBLE);
                    break;
                case CONNECT_FAILED:
                    statusText.setText("Connect to device failed. Please Check if your Evcoco is properly connected.");
                    imageVisibleControl(View.INVISIBLE);
                    break;
                case CONNECT_SUCCESS:
                    statusText.setText("Device Connected, synchronizing with the device.");
                    break;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evoco_one_ota);
        ButterKnife.bind(this);
        myRotateLoading= AnimationUtils.loadAnimation(this,R.anim.evoco_one_loading_rotation);
        myRotateLoading.setInterpolator(new LinearInterpolator());
        statusPic.startAnimation(myRotateLoading);
        otaProgressText.setVisibility(View.INVISIBLE);
        mAuth = FirebaseAuth.getInstance();
        mIntentFilter = new IntentFilter();
        mBroadcastReceiver = new OtaBroadcastReceiver();
        mIntentFilter.addAction(ServiceMessage.EVOCO_ONE_UI_ACTION);
        this.registerReceiver(mBroadcastReceiver,mIntentFilter);
        context = this;
        sharedPref = context.getSharedPreferences(getString(R.string.evoco_one_preference_file_key), Context.MODE_PRIVATE);
        editor = sharedPref.edit();
    }

    @Override
    protected  void onStart(){
        super.onStart();
        sendControl(EvocoOneBleService.CHECK_HARDWARE_INFO);
        checkFirmwareUpdate();
    }

    @Override
    protected void onDestroy(){
        sendControl(EvocoOneBleService.CONTROL_STOP_OTA);
        unregisterReceiver(mBroadcastReceiver);
        super.onDestroy();
    }

    public void checkFirmwareUpdate(){
        FirebaseUser mUser = mAuth.getCurrentUser();
        mUser.getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
            @Override
            public void onComplete(@NonNull Task<GetTokenResult> task) {
                if (task.isSuccessful()){
                    mToken = task.getResult().getToken();
                    String token = task.getResult().getToken();
                    backendLibrary mbackLib = new backendLibrary();
                    Map requestData = new HashMap();
                    requestData.put("device_name","evoco_one");
                    Log.d(TAG, "onComplete: "+requestData.toString());
                    OkHttpClient client = new OkHttpClient();
                    FormBody formBody = new FormBody.Builder()
                            .add("device_name","evoco_one")
                            .build();
                    Log.d(TAG, "onComplete: "+mbackLib.getFullUrl(mbackLib.FIRMWARE_UPDATE,token,requestData));
                    Request updateFw = new Request.Builder()
                            .get()
                            .url(mbackLib.getFullUrl(mbackLib.FIRMWARE_UPDATE,token,requestData))
                            .build();
                    Call call = client.newCall(updateFw);
                    call.enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            Message msg = new Message();
                            String msgText = "get failed";
                            msg.what = FETCH_FIRMWARE_FAILED;
                            msg.obj = msgText;
                            uiThreadHandler.sendMessage(msg);
                            Log.d(TAG, "onFailure: "+e);
                        }

                        @Override
                        public void onResponse(Call call, final Response response) throws IOException {
                            final String res = response.body().string();
                            try {
                                JSONObject resultJson = new JSONObject(res);
                                Log.d(TAG, "onResponse: future version： "+ resultJson.getString("fw_version"));
                                String currentVersion = sharedPref.getString("fw_version","");
                                Log.d(TAG, "onResponse: current version： " + currentVersion);
                                aim_fw_version = resultJson.getString("fw_version");
                                if ((currentVersion.equals(current_fw_version))){
                                    OkHttpClient downloadClient = new OkHttpClient();
                                    backendLibrary mbackLib = new backendLibrary();
                                    Request downloadFw = new Request.Builder()
                                            .get()
                                            .url(mbackLib.getFullUrl(resultJson.getString("src")))
                                            .build();
                                    Call downloadcall = downloadClient.newCall(downloadFw);
                                    downloadcall.enqueue(new Callback() {
                                        @Override
                                        public void onFailure(Call call, IOException e) {
                                            Log.d(TAG, "onFailure: " + call.toString());
                                        }

                                        @Override
                                        public void onResponse(Call call, Response response) throws IOException {
                                            Log.d(TAG, "onResponse: "+"getResponse");
                                            Log.d(TAG, "onResponse: "+getExternalFilesDir(null).toString());
                                            String savePath = getExternalFilesDir(null).getAbsolutePath();
                                            InputStream is = null;
                                            byte[] buf = new byte[2048];
                                            int len = 0;
                                            FileOutputStream fos = null;
                                            // 储存下载文件的目录
                                            try {
                                                is = response.body().byteStream();
                                                long total = response.body().contentLength();
                                                File file = new File(savePath, "evoco_one.bin");
                                                fos = new FileOutputStream(file);
                                                long sum = 0;
                                                while ((len = is.read(buf)) != -1) {
                                                    fos.write(buf, 0, len);
                                                }
                                                fos.flush();
                                                Log.d(TAG, "onResponse: "+"finish saving bin file");
                                                Log.d(TAG, "onResponse: "+"finish writting pref");
                                                sendControl(EvocoOneBleService.CONTROL_START_OTA);
                                            } catch (Exception e) {
                                                Log.d(TAG, "onResponse: "+"sorry");
                                            } finally {
                                                try {
                                                    if (is != null)
                                                        is.close();
                                                } catch (IOException e) {
                                                }
                                                try {
                                                    if (fos != null)
                                                        fos.close();
                                                } catch (IOException e) {
                                                }

                                            }
                                        }
                                    });
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.d(TAG, "onResponse: "+res);
                        }
                    });
                }else{
                    Toast.makeText(evocoOneOTA.this,"Get Token Failed, Have You Signed In Yet?",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private class OtaBroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent){
            Bundle mBundle = intent.getExtras();
            switch (mBundle.getInt("SERVICE_MESSAGE",0xFF)){
                case ServiceMessage.MSG_OTA_STATUS:
                    updateOtaStatus(mBundle.getInt(EvocoOneBleService.OTA_STATUS_FIELD));
                    break;
                case ServiceMessage.MSG_OTA_PROGRESS:
                    mBundle = intent.getExtras();
                    updateOtaProgress(mBundle.getInt(EvocoOneBleService.OTA_PROGRESS_FIELD));
                    break;
                case ServiceMessage.MSG_FIRMWARE_INFO:
                    current_fw_version = mBundle.getInt("generation_number") + "." + mBundle.getInt("version_number") + "." + mBundle.getInt("build_version");
                    break;
                default:
                    Log.d(TAG, "onReceive: SERVICE_MESSAGE_IS: "+intent.getIntExtra("SERVICE_MESSAGE",0xFF));
                    break;
            }
        }
    }

    public void updateOtaStatus(int statusCode){
        Message mMsg = new Message();
        switch (statusCode){
            case STATE_CONNECTED:
                mMsg.what = CONNECT_SUCCESS;
                uiThreadHandler.sendMessage(mMsg);
                break;
            case STATE_DISCONNECTED:
                mMsg.what = CONNECT_FAILED;
                uiThreadHandler.sendMessage(mMsg);
                break;
            case STATE_OTA_ING:
                mMsg.what = START_OTA;
                uiThreadHandler.sendMessage(mMsg);
                break;
            case STATE_OTA_SUCCESS:
                mMsg.what = OTA_SUCCESS;
                uiThreadHandler.sendMessage(mMsg);
                break;
            default:
                mMsg.what = 0x90;
        }

    }

    public void updateOtaProgress(int progress){
        if (otaProgressText.getVisibility() != View.VISIBLE){
            otaProgressText.setVisibility(View.VISIBLE);
        }
        otaProgressText.setText(progress+"%");
    }

    public void imageVisibleControl(int visibility){
        if (statusPic.getVisibility() == View.VISIBLE && visibility == View.INVISIBLE){
            statusPic.clearAnimation();
            statusPic.setVisibility(View.INVISIBLE);
            otaProgressText.setVisibility(View.INVISIBLE);
        }else if (statusPic.getVisibility() == View.INVISIBLE && visibility == View.VISIBLE){
            statusPic.setVisibility(View.VISIBLE);
            statusPic.startAnimation(myRotateLoading);
        }
    }

    public void sendControl(int controlCode){
        Intent mIntent = new Intent();
        mIntent.setAction(EvocoOneBleService.EVOCO_ONE_BLE_SERVICE_ACTION);
        mIntent.putExtra(EvocoOneBleService.CONTROL_COMMAND_FIELD,controlCode);
        sendBroadcast(mIntent);
    }
}
