package com.iir_eq.ui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.PopupWindow;


import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.iir_eq.R;
import com.iir_eq.bluetooth.callback.Startpickfilecallback;
import com.iir_eq.ui.ControlFragment;
import com.iir_eq.ui.fragment.OtaDaulPickFileFragment;
import com.iir_eq.ui.login.LoginActivity;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import  com.iir_eq.ui.login.LoginActivity;


public  class MainActivity extends AppCompatActivity implements Startpickfilecallback {
    public static View view;
    public static View wait;
    public static  AlertDialog.Builder alertDialog;
    public static final String TAG = "MainActivity";
    public static LeScanActivity lescan;
    public static LeOtaActivity ota;
    public static AlertDialog  mshow;
    public static  MainActivity mainactivity;
    private  static final int  Request_Bluesetting = 0x12;
    public static AlertDialog connect_alertdialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);
        view=this.findViewById(R.id.container);
        ota= new LeOtaActivity();
        lescan=new LeScanActivity();
        ota.initView(view);
//        ota.initConfig();
//        view.findViewById(R.id.devices_text).setVisibility(View.GONE);
//        view.findViewById(R.id.deviceaddress_text).setVisibility(View.GONE);
//        view.findViewById(R.id.currentVersionDetails_text).setVisibility(View.GONE);
//        view.findViewById(R.id.ota_info_text).setVisibility(View.GONE);
//        view.findViewById(R.id.ota_info_list_text).setVisibility(View.GONE);


        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);


        startActivityForResult(new Intent(this, LoginActivity.class),1);

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "onActivityResult");

        if (requestCode == Request_Bluesetting ) {
            connect_alertdialog.cancel();
            alertDialog=new AlertDialog.Builder(this);
            View connect_view = View.inflate(this,R.layout.fragment_devices,null);
            wait = View.inflate(this,R.layout.wnd_info,null);
            alertDialog
                    .setTitle("bluetooth connect")
                    .setIcon(R.mipmap.ic_launcher)
                    .setView(connect_view)
                    .setCancelable(false);
            connect_alertdialog=alertDialog.create();
            mshow = alertDialog.show();
            lescan.LescaInit(LeScanActivity.MODE_OTA,connect_view,ota);

        }else if(requestCode==1 && resultCode == RESULT_OK)
        {
            alertDialog=new AlertDialog.Builder(this);
            View connect_view = View.inflate(this,R.layout.fragment_devices,null);
            wait = View.inflate(this,R.layout.wnd_info,null);
            alertDialog
                    .setTitle("bluetooth connect")
                    .setIcon(R.mipmap.ic_launcher)
                    .setView(connect_view)
                    .setCancelable(false);
            connect_alertdialog=alertDialog.create();
            mshow = alertDialog.show();
            lescan.LescaInit(LeScanActivity.MODE_OTA,connect_view,ota);
            mainactivity=this;
        }
    }


    @Override
    public void PickFileCallback(int request) {

    }

    @Override
    public void startset(String setting) {
        startActivityForResult(new Intent(setting),Request_Bluesetting);
    }
}
