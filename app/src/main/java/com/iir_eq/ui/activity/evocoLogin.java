package com.iir_eq.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.iir_eq.R;
import com.iir_eq.util.backendLibrary;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class evocoLogin extends AppCompatActivity {

    FirebaseAuth mAuth;
    public final String TAG = "evocoLogin";

    public static final int MODE_SIGNIN = 1;
    public static final int MODE_SIGNUP = 2;
    private int requestMode = MODE_SIGNIN;
    private RequestQueue mRequestQueue;

    @BindView(R.id.evoco_loginEmailInput) EditText emailAddress;
    @BindView(R.id.evoco_loginPasswordInput) EditText password;
    @BindView(R.id.login_signin_btn) Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evoco_login);
        setSupportActionBar((Toolbar) findViewById(R.id.evoco_one_login_toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        Intent prevInfo=getIntent();
        ButterKnife.bind(this);
        int requestMode = prevInfo.getIntExtra("MODE",MODE_SIGNIN);
        if (requestMode == MODE_SIGNUP) loginButton.setText(R.string.signup);
        mAuth = FirebaseAuth.getInstance();
        mRequestQueue = Volley.newRequestQueue(this);
    }

    @OnClick(R.id.login_signin_btn)
    public void signIn_signUpOnClick(){
        if (requestMode == MODE_SIGNIN){
            mAuth.signInWithEmailAndPassword(emailAddress.getText().toString(),password.getText().toString()).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithEmail:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        setResult(RESULT_OK);
                        backendLibrary mbackLib = new backendLibrary();
                        FirebaseUser fbUser = mAuth.getCurrentUser();
                        fbUser.getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                            @Override
                            public void onComplete(@NonNull Task<GetTokenResult> task) {
                                if (task.isSuccessful()){
                                    String idToken = task.getResult().getToken();
                                    Log.d(TAG, "onComplete: "+mbackLib.getFullUrl(mbackLib.LOGIN_CHECK, idToken));
                                    JsonObjectRequest loginCheck = new JsonObjectRequest(Request.Method.GET, mbackLib.getFullUrl(mbackLib.LOGIN_CHECK, idToken), null,
                                            new Response.Listener() {
                                                @Override
                                                public void onResponse(Object response) {
                                                    Log.d(TAG, "onResponse: "+response.toString());
                                                    finish();
                                                }
                                            }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Log.d(TAG, "onErrorResponse: "+error.toString());
                                        }
                                    });
                                    mRequestQueue.add(loginCheck);
                                }else{
                                    Toast.makeText(evocoLogin.this,"get Token Failed, Please Check Your Network",Toast.LENGTH_LONG).show();
                                }
                            }
                        });

                        //finish();
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithEmail:failure", task.getException());
                        Toast.makeText(evocoLogin.this, "Authentication failed.",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }else if (requestMode == MODE_SIGNUP){
            mAuth.createUserWithEmailAndPassword(emailAddress.getText().toString(),password.getText().toString()).addOnCompleteListener(this,new OnCompleteListener<AuthResult>(){
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithEmail:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        setResult(RESULT_OK);
                        backendLibrary mbackLib = new backendLibrary();
                        FirebaseUser fbUser = mAuth.getCurrentUser();
                        fbUser.getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                            @Override
                            public void onComplete(@NonNull Task<GetTokenResult> task) {
                                if (task.isSuccessful()){
                                    String idToken = task.getResult().getToken();
                                    Log.d(TAG, "onComplete: "+mbackLib.getFullUrl(mbackLib.LOGIN_CHECK, idToken));
                                    JsonObjectRequest loginCheck = new JsonObjectRequest(Request.Method.GET, mbackLib.getFullUrl(mbackLib.LOGIN_CHECK, idToken), null,
                                            new Response.Listener() {
                                                @Override
                                                public void onResponse(Object response) {
                                                    Log.d(TAG, "onResponse: "+response.toString());
                                                    finish();
                                                }
                                            }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Log.d(TAG, "onErrorResponse: "+error.toString());
                                        }
                                    });
                                    mRequestQueue.add(loginCheck);
                                }else{
                                    Toast.makeText(evocoLogin.this,"get Token Failed, Please Check Your Network",Toast.LENGTH_LONG).show();
                                }
                            }
                        });

                        //finish();
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithEmail:failure", task.getException());
                        Toast.makeText(evocoLogin.this, "Authentication failed.",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
