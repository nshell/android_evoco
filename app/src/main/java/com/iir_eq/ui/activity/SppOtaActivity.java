package com.iir_eq.ui.activity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.evocolabs.app.OTACallback;
import com.iir_eq.R;
import com.iir_eq.bluetooth.SppConnector;
import com.iir_eq.bluetooth.callback.ConnectCallback;
import com.iir_eq.contants.Constants;
import com.iir_eq.util.ArrayUtil;
import com.iir_eq.util.Logger;
import com.iir_eq.util.SPHelper;

/**
 * Created by zhaowanxing on 2017/4/23.
 */

public class SppOtaActivity extends OtaActivity implements ConnectCallback {

    private static final String KEY_OTA_DEVICE_NAME = "spp_ota_device_name";
    private static final String KEY_OTA_DEVICE_ADDRESS = "spp_ota_device_addr";

    private SppConnector mSppConnector;
    private boolean isOld = false;

    @Override
    public void initConfig(OTACallback cb) {
        super.initConfig(cb);
        mSppConnector = SppConnector.getConnector();
        mSppConnector.addConnectCallback(this);
        registerBtReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing()) {
            unregisterReceiver(mBtReceiver);
            if (mSppConnector != null) {
                mSppConnector.removeConnectCallback(this);
                mSppConnector.disconnect();
            }
        }
    }
    public void stopOta(){
        mContext.unregisterReceiver(mBtReceiver);
        if(mSppConnector != null){
            mSppConnector.removeConnectCallback(this);
            mSppConnector.disconnect();
        }
    }

    @Override
    public void controlStopOta(){
        stopOta();
        super.controlStopOta();
    }

    @Override
    protected void pickDevice(int request) {
        startActivityForResult(new Intent(mContext, ClassicScanActivity.class), request);
    }

    @Override
    protected String loadLastDeviceName() {
        return SPHelper.getPreference(mContext, KEY_OTA_DEVICE_NAME, "--").toString();
    }

    @Override
    protected void saveLastDeviceName(String name) {
        SPHelper.putPreference(mContext, KEY_OTA_DEVICE_NAME, name);
    }

    @Override
    protected String loadLastDeviceAddress() {
        return SPHelper.getPreference(mContext, KEY_OTA_DEVICE_ADDRESS, "--").toString();
    }

    @Override
    protected void saveLastDeviceAddress(String address) {
        SPHelper.putPreference(mContext, KEY_OTA_DEVICE_ADDRESS, address);
    }

    @Override
    protected void connect() {
        if (!mExit) {
            connectSpp();
        }
    }

    @Override
    protected void disconnect() {
        if (mSppConnector != null) {
            mSppConnector.disconnect();
        }
    }

    private void connectSpp() {
        boolean isOld  = (boolean)SPHelper.getPreference(mContext,Constants.KEY_FIRMWARE_TYPE,true);
        Log.e("isOld",isOld+"");
        if (!mExit) {
            if (mSppConnector.connect(mDevice,isOld)) {
                onConnecting();
            }
        }
    }

    private void onBonded() {
    }

    private void onBondNone() {
        //可以添加重新配对的操作
        updateInfo(R.string.bond_none);
    }

    @Override
    protected boolean sendData(byte[] data) {
        if (!mExit) {
            if (mSppConnector.write(data)) {
                onWritten();
                LOG(TAG , "sendData mConnector.write(data  , isResponse) send true mWritten reset to false "+"length"+data.length+" data "+ ArrayUtil.toHex(data));
                return true;
            }
            LOG(TAG, "sendData mConnector.write(data) return false failCount = "+sendMsgFailCount );
            return false;
        }
        return false;
    }

    @Override
    protected boolean isBle() {
        return false;
    }

    @Override
    protected void onWritten() {
        Logger.e(TAG, "onWritten");
        super.onWritten();
        boolean isOld  = (boolean)SPHelper.getPreference(mContext,Constants.KEY_FIRMWARE_TYPE,true);
        Log.e("onWritten",isOld+"");
        boolean ack_enable = (boolean)SPHelper.getPreference(mContext,Constants.KEY_ACK_ENABLE,false);
        if((isOld == true)&&(ack_enable == false))
        {
            otaNextDelayed(100);
        }
    }

    @Override
    public void onConnectionStateChanged(boolean connected) {
        if (!mExit) {
            super.onConnectionStateChanged(connected);
        }
    }

    private void registerBtReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        mContext.registerReceiver(mBtReceiver, filter);
    }

    private BroadcastReceiver mBtReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Logger.e(TAG, "onReceive " + intent);
            switch (intent.getAction()) {
                case BluetoothAdapter.ACTION_STATE_CHANGED:
                    break;
                case BluetoothDevice.ACTION_BOND_STATE_CHANGED:
                    onReceiveBondState((BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE), intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.BOND_NONE));
                    break;
            }
        }
    };

    private void onReceiveBondState(BluetoothDevice device, int state) {
        Logger.e(TAG, "onReceiveBondState " + state + "; device to connect " + mDevice + "; bond changed device " + device);
        if (!device.equals(mDevice)) {
            return;
        }
        if (state == BluetoothDevice.BOND_BONDED) {
            onBonded();
        } else if (state == BluetoothDevice.BOND_NONE) {
            onBondNone();
        }
    }

    @Override
    protected int getMtu(int stereo_flg) {
        if(stereo_flg == STEREO_NEW) {
            if(mMtu > DEFAULT_MTU_SPP) {
                return DEFAULT_MTU_SPP;
            }
            else if((mMtu > 0)&&(mMtu < DEFAULT_MTU_SPP))
            {
                return mMtu;
            }
            else
            {
                return DEFAULT_MTU_SPP;
            }
        }
        else {
            if(mMtu > DEFAULT_MTU ) {
                return DEFAULT_MTU;
            }
            else if((mMtu >0 )&& (mMtu <DEFAULT_MTU))
            {
                return mMtu;
            }
            else
            {
                return DEFAULT_MTU;
            }
        }

    }

    @Override
    protected String getActivityName()
    {
        return "SppOtaActivity";
    }



}
