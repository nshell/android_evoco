package com.iir_eq.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.iir_eq.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class evocoWelcome extends AppCompatActivity {

    public static final int SIGNIN_REQUEST=1;
    public static final int SIGNUP_REQUEST=2;
    public int backCount=0;

    @BindView(R.id.main_signin) Button mSignInBtn;
    @BindView(R.id.main_signup) Button mSignUpBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evoco_welcome);
        ButterKnife.bind(this);
    }

    @Override
    public void onBackPressed() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
        System.exit(0);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
            System.exit(0);
            return true;
        }else {
            return super.onKeyDown(keyCode, event);
        }

    }


    @OnClick(R.id.main_signup)
    public void signUpNav(){
        Intent mIntent = new Intent(this, evocoLogin.class);
        mIntent.putExtra("MODE",evocoLogin.MODE_SIGNUP);
        startActivityForResult(mIntent,SIGNUP_REQUEST);
    }

    @OnClick(R.id.main_signin)
    public void signInNav(){
        Intent mIntent = new Intent(this, evocoLogin.class);
        mIntent.putExtra("MODE",evocoLogin.MODE_SIGNIN);
        startActivityForResult(mIntent,SIGNIN_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode==RESULT_OK){
            super.onActivityResult(requestCode,resultCode,data);
            finish();
        } else if (resultCode == RESULT_CANCELED){
            super.onActivityResult(requestCode,resultCode,data);
        }else{
            super.onActivityResult(requestCode,resultCode,data);
        }
    }
}
