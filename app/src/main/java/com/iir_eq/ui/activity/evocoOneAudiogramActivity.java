package com.iir_eq.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.evocolabs.app.EvocoOneBleService;
import com.evocolabs.app.ServiceMessage;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.iir_eq.R;
import com.iir_eq.util.backendLibrary;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class evocoOneAudiogramActivity extends AppCompatActivity {

    private final String TAG = "evocoOneAudiogram";
    private FirebaseAuth mAuth;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private String mToken="";

    private IntentFilter mIntentFilter;
    private EvocoOneAudiogramBroadcastReceiver mBroadcastReceiver;
    private Handler mHandler;

    private static final int PRESET_MINIMAL = 0x00;
    private static final int PRESET_MILD = 0x01;
    private static final int PRESET_MODERATE = 0x02;
    private static final int PRESET_MODERATELY_SEVER = 0x04;

    @BindView(R.id.evoco_one_preset_r_seekbar) SeekBar right_preset;
    @BindView(R.id.evoco_one_preset_l_seekbar) SeekBar left_preset;
    @BindView(R.id.evoco_one_audiogram_toolbar) Toolbar mToolBar;
    @BindView(R.id.evoco_one_audiogram_sync_wrapper) ConstraintLayout syncAudiogram;
    @BindView(R.id.evoco_one_audiogram_preset_control_layout) ConstraintLayout presetLayout;
    @BindView(R.id.evoco_one_audiogram_preset_visibility_switch)
    Switch presetVisibilitySwitcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evoco_one_audiogram);
        setSupportActionBar((Toolbar)findViewById(R.id.evoco_one_audiogram_toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ButterKnife.bind(this);
        seekBarInit();
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(ServiceMessage.EVOCO_ONE_UI_ACTION);
        mBroadcastReceiver = new EvocoOneAudiogramBroadcastReceiver();
        this.registerReceiver(mBroadcastReceiver,mIntentFilter);
        mHandler = new Handler();
        mAuth = FirebaseAuth.getInstance();
        sharedPref = getSharedPreferences(getString(R.string.evoco_one_preference_file_key), Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        tokenInit();
        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // sendControl(EvocoOneBleService.CHECK_HARDWARE_INFO);
            }
        },100);
    }

    @Override
    public void onDestroy(){
        this.unregisterReceiver(mBroadcastReceiver);
        super.onDestroy();
    }

    private void tokenInit(){
        FirebaseUser mUser = mAuth.getCurrentUser();
        mUser.getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
            @Override
            public void onComplete(@NonNull Task<GetTokenResult> task) {
                if(task.isSuccessful()){
                    mToken = task.getResult().getToken();
                }else{
                    Toast.makeText(evocoOneAudiogramActivity.this,"No Network.",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void seekBarInit(){
        right_preset.setOnSeekBarChangeListener(presetChangeListenerSeekBar);
        left_preset.setOnSeekBarChangeListener(presetChangeListenerSeekBar);
    }

    @Override
    public void onStart() {
        super.onStart();
        boolean usingCustomGaintable = sharedPref.getBoolean("using_custom_gaintable",false);
        if (usingCustomGaintable){
            presetLayout.setVisibility(View.GONE);
            presetVisibilitySwitcher.setChecked(false);
        }else{
            presetLayout.setVisibility(View.VISIBLE);
            presetVisibilitySwitcher.setChecked(true);
        }
        sendControl(EvocoOneBleService.CHECK_GENERAL_STATUS);
    }

    @OnCheckedChanged(R.id.evoco_one_audiogram_preset_visibility_switch)
    public void presetSwitchChanged(boolean isChecked){
        if (isChecked){
            presetLayout.setVisibility(View.VISIBLE);
        }else{
            presetLayout.setVisibility(View.GONE);
        }
    }

    SeekBar.OnSeekBarChangeListener presetChangeListenerSeekBar = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        }
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            int progress = seekBar.getProgress();
            Log.i("Audiogram","progress:" + Integer.toString(progress));
            int left_command=0;
            int right_command=0;
            if (progress<=16){
                seekBar.setProgress(0);
                left_command = EvocoOneBleService.SET_LEFT_PRESET_MINIMAL;
                right_command = EvocoOneBleService.SET_RIGHT_PRESET_MINIMAL;
            }else if (progress<=50){
                seekBar.setProgress(33);
                left_command = EvocoOneBleService.SET_LEFT_PRESET_MILD;
                right_command = EvocoOneBleService.SET_RIGHT_PRESET_MILD;
            }else if (progress<=84){
                seekBar.setProgress(66);
                left_command = EvocoOneBleService.SET_LEFT_PRESET_MODERATE;
                right_command = EvocoOneBleService.SET_RIGHT_PRESET_MODERATE;
            }else{
                seekBar.setProgress(100);
                left_command = EvocoOneBleService.SET_LEFT_PRESET_MODERATELY_SEVER;
                right_command = EvocoOneBleService.SET_RIGHT_PRESET_MODERATELY_SEVER;
            }
            if (seekBar.getId()==R.id.evoco_one_preset_l_seekbar){
                sendControl(left_command);
            }else if(seekBar.getId()==R.id.evoco_one_preset_r_seekbar){
                sendControl(right_command);
            }
            editor.putBoolean("using_custom_gaintable",false);
            editor.commit();
        }
    };

    public void updateSeekbarProgressView(int masterPreset, int slavePreset){
        int leftProgress=0;
        int rightProgress=0;
        Log.d(TAG, "updateSeekbarProgressView: Left:"+masterPreset+" RIGHT:"+slavePreset);
        switch (masterPreset){
            case PRESET_MINIMAL:
                leftProgress=0;
                break;
            case PRESET_MILD:
                leftProgress=33;
                break;
            case PRESET_MODERATE:
                leftProgress=66;
                break;
            case PRESET_MODERATELY_SEVER:
                leftProgress=100;
                break;
            default:
                Log.d(TAG, "updateSeekbarProgressView: SOMETHING WENT WRONG WITH THE LEFT PRESET NUMBERS");
        }
        switch (slavePreset){
            case PRESET_MINIMAL:
                rightProgress=0;
                break;
            case PRESET_MILD:
                rightProgress=33;
                break;
            case PRESET_MODERATE:
                rightProgress=66;
                break;
            case PRESET_MODERATELY_SEVER:
                rightProgress=100;
                break;
            default:
                Log.d(TAG, "updateSeekbarProgressView: SOMETHING WENT WRONG WITH THE PRESET NUMBERS");
        }
        left_preset.setProgress(leftProgress);
        right_preset.setProgress(rightProgress);
    }

    private void requestAudiogramList(){
        backendLibrary mbackLib = new backendLibrary();
        Map requestData = new HashMap();
        requestData.put("device_name","evoco_one");
        Log.d(TAG, "onComplete: "+requestData.toString());
        OkHttpClient client = new OkHttpClient();
        FormBody formBody = new FormBody.Builder()
                .add("device_name","evoco_one")
                .build();
        Log.d(TAG, "onComplete: "+mbackLib.getFullUrl(mbackLib.GET_AUDIOGRAM_LIST,mToken,requestData));
        Request syncAudgrm = new Request.Builder()
                .get()
                .url(mbackLib.getFullUrl(mbackLib.GET_AUDIOGRAM_LIST,mToken,requestData))
                .build();
        Call call = client.newCall(syncAudgrm);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(TAG, "onFailure: " + "get Audiogram Failed");
            }
            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String res = response.body().string();
                Log.d(TAG, "onResponse: res"+res);
                if (res.contains("error") && res.contains("true")){
                    Log.d(TAG, "onResponse: contains Error"+res);
                }else{
                    try {
                        JSONArray audgrmList = new JSONArray(res);
                        JSONObject latestAudgrm = audgrmList.getJSONObject(0);
//                        if (latestAudgrm != null && !latestAudgrm.getString("audiogram_ID").equals(sharedPref.getString("audiogram_ID", ""))) {
                        if (latestAudgrm != null) {
                            Log.d(TAG, "RequestAudiogramList - onResponse: "+"start downloading audiogram: "+ latestAudgrm.getString("audiogram_ID"));
                            downLoadAudioGram(latestAudgrm.getString("audiogram_ID"));
                        }
                    } catch (JSONException e2){
                        Log.d(TAG, "RequestAudiogramList - onResponse: "+"dealing with json occur error");
                    }
                }
            }
        });
    }

    private void downLoadAudioGram(String audiogram_ID){
        backendLibrary mbackLib = new backendLibrary();
        OkHttpClient client = new OkHttpClient();
        Map getGainData = new HashMap();
        getGainData.put("audiogram_ID",audiogram_ID);
        Request getGain = new Request.Builder()
                .get()
                .url(mbackLib.getFullUrl(mbackLib.GET_GAIN_TABLE,mToken,getGainData))
                .build();
        Call getGainCall = client.newCall(getGain);
        getGainCall.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(TAG, "onFailure: "+"get Gain Table failed");
            }

            @Override
            public void onResponse(Call call, Response response2) throws IOException {
                Log.d(TAG, "onResponse: "+"get Gain Table Success");
                final String gainRes = response2.body().string();
                try{
                    JSONObject getGainResponse = new JSONObject(gainRes);
                    JSONObject leftGain = getGainResponse.getJSONObject("left");
                    JSONObject rightGain = getGainResponse.getJSONObject("right");
                    Log.d(TAG, "onResponse: "+leftGain.toString());
                    editor.putString("audiogram_ID",audiogram_ID);
                    editor.putString("left_gain_table",leftGain.getString("gain_table"));
                    editor.putInt("left_version",leftGain.getInt("version"));
                    editor.putString("left_eq",leftGain.getString("eq"));
                    editor.putString("right_gain_table",rightGain.getString("gain_table"));
                    editor.putInt("right_version",rightGain.getInt("version"));
                    editor.putString("right_eq",rightGain.getString("eq"));
                    editor.commit();
                    sendControl(EvocoOneBleService.CHECK_HARDWARE_INFO);

                    //compareGainTableVersion(leftGain.getInt("version"),rightGain.getInt("version"));
                } catch (JSONException e2){
                    Log.d(TAG, "onResponse: "+"dealing with json occurs error");
                }
            }
        });
    }

    public void compareGainTableVersion(int leftVersion, int rightVersion){
        int aimLeftGainTable = sharedPref.getInt("left_version",0);
        int aimRightGainTable = sharedPref.getInt("right_version",0);
        if (aimLeftGainTable>=leftVersion){
            String leftGainTable = sharedPref.getString("left_gain_table","");
            Log.d(TAG, "compareGainTableVersion: - leftGainTable: " + leftGainTable);
            Bundle leftGainBundle = new Bundle();
            leftGainBundle.putString("gain_table",leftGainTable);
            leftGainBundle.putInt("version",aimLeftGainTable);
            sendControl(EvocoOneBleService.SET_LEFT_GAIN_TABLE,leftGainBundle);
        }
        if(aimRightGainTable>=rightVersion){
            String rightGainTable = sharedPref.getString("right_gain_table","");
            Log.d(TAG, "compareGainTableVersion - rightGainTable: " + rightGainTable);
            Bundle rightGainBundle = new Bundle();
            rightGainBundle.putString("gain_table",rightGainTable);
            rightGainBundle.putInt("version",aimRightGainTable);
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    sendControl(EvocoOneBleService.SET_RIGHT_GAIN_TABLE,rightGainBundle);
                }
            },500);
        }
        editor.putBoolean("using_custom_gaintable",true);
        editor.commit();
    }

    @OnClick(R.id.evoco_one_audiogram_sync_wrapper)
    public void syncAudiogramOnClick(){
        requestAudiogramList();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class EvocoOneAudiogramBroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent){
            int messageName = intent.getIntExtra("SERVICE_MESSAGE",0xff);
            Log.d(TAG, "onReceive: ");
            switch (messageName){
                case ServiceMessage.MSG_GENERAL_INFO:
                    Bundle serviceStatusBundle = intent.getExtras();
                    int realMasterPreset = serviceStatusBundle.getInt("master_preset");
                    int realSlavePreset = serviceStatusBundle.getInt("slave_preset");
                    updateSeekbarProgressView(realMasterPreset,realSlavePreset);
                    break;
                case ServiceMessage.MSG_FIRMWARE_INFO:
                    Bundle firmwareInfo = intent.getExtras();
                    compareGainTableVersion(firmwareInfo.getInt("left_gaintable_version"),firmwareInfo.getInt("right_gaintable_version"));
                    break;
            }
        }
    }

    private void sendControl(int controlCode){
        Intent broadCastIntent= new Intent();
        broadCastIntent.setAction(EvocoOneBleService.EVOCO_ONE_BLE_SERVICE_ACTION);
        broadCastIntent.putExtra(EvocoOneBleService.CONTROL_COMMAND_FIELD, controlCode);
        sendBroadcast(broadCastIntent);
    }

    private void sendControl(int controlCode, Bundle bundle){
        Intent broadCastIntent= new Intent();
        broadCastIntent.setAction(EvocoOneBleService.EVOCO_ONE_BLE_SERVICE_ACTION);
        broadCastIntent.putExtra(EvocoOneBleService.CONTROL_COMMAND_FIELD, controlCode);
        broadCastIntent.putExtras(bundle);
        sendBroadcast(broadCastIntent);
    }
}
