package com.iir_eq.ui.activity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.iir_eq.bluetooth.BtHelper;
import com.iir_eq.bluetooth.SppConnector;
import com.iir_eq.bluetooth.callback.ConnectCallback;
import com.iir_eq.contants.Constants;
import com.iir_eq.util.LogUtils;
import com.iir_eq.util.SPHelper;

import java.util.List;

import static android.webkit.ConsoleMessage.MessageLevel.LOG;

public class OtaHelper {
    // SPP OTA Class Bundle

    private static String TAG = "OtaHelper";
    protected static final String KEY_OTA_FILE = "ota_file";
    private static final String KEY_OTA_DEVICE_NAME = "spp_ota_device_name";
    private static final String KEY_OTA_DEVICE_ADDRESS = "spp_ota_device_addr";
    private static final int APPLY_BOTH_EARBUD_IN_ONE = 2;

    private Context mContext;
    private BluetoothDevice mDevice;
    private String mName;
    private String mAddress;
    private boolean resume_enable = true;
    private int daul_apply = APPLY_BOTH_EARBUD_IN_ONE;
    private int resumeUpgradeWay = APPLY_BOTH_EARBUD_IN_ONE;
    private int two_bins_in_step_one = -1;

    private BluetoothAdapter bluetoothAdapter = null;
    private SppConnector mSppConnector;

    private int mState;
    protected static final int STATE_IDLE = 0x00;
    protected static final int STATE_CONNECTING = 0x01;
    protected static final int STATE_CONNECTED = 0x02;
    protected static final int STATE_DISCONNECTING = 0x03;
    protected static final int STATE_DISCONNECTED = 0x04;
    protected static final int STATE_OTA_ING = 0x05;
    protected static final int STATE_OTA_FAILED = 0x06;
    protected static final int STATE_OTA_CONFIG = 0x07;
    protected static final int STATE_BUSY = 0x0F;

    protected HandlerThread mCmdThread;
    protected CmdHandler mCmdHandler;
    protected byte[] mOtaResumeDataReq;


    private boolean isOld = false;

    OtaHelper(Context context){
        mContext = context;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mSppConnector = SppConnector.getConnector();
        //mSppConnector.addConnectCallback();
        two_bins_in_step_one = -1;
    }

    private void getConnectBtDetails(int flag)
    {
        bluetoothAdapter.getProfileProxy(mContext, new BluetoothProfile.ServiceListener()
        {
            @Override
            public void onServiceDisconnected(int profile)
            {
                Toast.makeText(mContext, profile + "", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onServiceConnected(int profile, BluetoothProfile proxy)
            {
                List<BluetoothDevice> mDevices = proxy.getConnectedDevices();
                if (mDevices != null && mDevices.size() > 0) {
                    for (BluetoothDevice device : mDevices) {
                        if (device.getName().toUpperCase().contains("EVOCO")||device.getName().toUpperCase().contains("VERSO")){
                            saveLastDeviceName(device.getName());
                            saveLastDeviceAddress(device.getAddress());
                            mDevice = BtHelper.getRemoteDevice(mContext, device.getAddress().toString());
                            mName = device.getName();
                            mAddress = device.getAddress();
                            LOG("getConnectBtDetails", "mDevice" + mDevice + "," + device.getAddress());
                            break;
                        }
                    }
                }
                else {
                    mName = "--";
                }
            }
        }, flag);
    }

    protected void saveLastDeviceName(String name) {
        SPHelper.putPreference(mContext, KEY_OTA_DEVICE_NAME, name);
    }

    protected void saveLastDeviceAddress(String address) {
        SPHelper.putPreference(mContext, KEY_OTA_DEVICE_ADDRESS, address);
    }

    protected void LOG(String TAG, String msg)
    {
        if (msg != null && TAG != null) {
            LogUtils.writeForClassicBt(TAG, msg);
        }
    }

    /******************
     *
     * ConnectCallback Interface
     *
     ******************/
//    @Override public void onConnectionStateChanged(boolean connected){
//        LOG(TAG, "onConnectionStateChanged " + connected + "; " + mState);
//        if (connected) {
//            onConnected();
//
//        }
//        else {
//            removeTimeout();
//            if (mState == STATE_CONNECTING) {
//                mState = STATE_DISCONNECTED;
//            }
//            else if (mState == STATE_OTA_ING) {
//                LOG(TAG, "mState == STATE_OTA_ING");
////                onOtaFailed();
//            }
//            else if (mState != STATE_IDLE) {
//                LOG(TAG, "mState != STATE_IDLE");
//                mState = STATE_DISCONNECTED;
////                onOtaFailed();
//            }
//            else if (mState == STATE_IDLE) {
//                LOG(TAG, "mState == STATE_IDLE");
//                mState = STATE_DISCONNECTED;
//            }
//        }
//    }

    protected void removeTimeout()
    {
        LOG(TAG, "removeTimeout");
//        mMsgHandler.removeMessages(MSG_OTA_TIME_OUT);
    }


    protected void onConnecting()
    {
        LOG(TAG, "onConnecting");
        LogUtils.writeForOTAStatic(TAG, "onConnecting ");
        mState = STATE_CONNECTING;
    }

    protected void onConnected(){
        LOG(TAG, "onConnected");
        boolean isOld = false;
        isOld  = (boolean)SPHelper.getPreference(mContext, Constants.KEY_FIRMWARE_TYPE,true);
//        sendCmdDelayed(CMD_SEND_HW_INFO, 0);
        LogUtils.writeForOTAStatic(TAG, "onConnected ");
        mState = STATE_CONNECTED;
    }



    protected void sendCmdDelayed(int cmd, long millis)
    {
        mCmdHandler.removeMessages(cmd);
        if (millis == 0) {
            mCmdHandler.sendEmptyMessage(cmd);
        }
        else {
            mCmdHandler.sendEmptyMessageDelayed(cmd, millis);
        }
    }

    public class CmdHandler extends Handler
    {
        public CmdHandler(Looper looper)
        {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg)
        {
//            switch (msg.what) {
//                case CMD_CONNECT:
//                    connect();
//                    break;
//                case CMD_DISCONNECT:
//                    disconnect();
//                    break;
//                case CMD_LOAD_FILE:
//                    loadFile();
//                    break;
//                case CMD_OTA_NEXT:
//                    LOG(TAG, "handleMessage CMD_OTA_NEXT");
//                    otaNext();
//                    break;
//                case CMD_START_OTA:
//                    LOG("CMD_START_OTA", "CMD_START_OTA");
//                    startOta();
//                    break;
//                case CMD_SEND_FILE_INFO:
//                    sendFileInfo();
//                    break;
//                case CMD_LOAD_OTA_CONFIG:
//                    loadOtaConfig();
//                    break;
//                case CMD_START_OTA_CONFIG:
//                    startOtaConfig();
//                    break;
//                case CMD_OTA_CONFIG_NEXT:
//                    otaConfigNext();
//                    break;
//                case CMD_LOAD_FILE_FOR_NEW_PROFILE:
//                    loadFileForNewProfile();
//                    break;
//                case CMD_LOAD_FILE_FOR_NEW_PROFILE_SPP:
//                    loadFileForNewProfileSPP();
//                    break;
//                case CMD_RESEND_MSG:
//                    LOG(TAG, "resend the msg");
//                    sendCmdDelayed(CMD_OTA_NEXT, 0);
//                    break;
//                case CMD_RESUME_OTA_CHECK_MSG:
//                    Log.e(TAG, "CMD_RESUME_OTA_CHECK_MSG");
//                    sendBreakPointCheckReq();
//                    break;
//                case CMD_SEND_HW_INFO:
//                    LOG(TAG, "CMD_SEND_HW_INFO");
//                    handleGetCurrentVersion();
//                    break;
//                case CMD_APPLY_THE_IMAGE_MSG:
//                    LOG("CMD_APPLY_THE_IMAGE_MSG", "CMD_APPLY_THE_IMAGE_MSG");
//                    handleApplyTheImage();
//                    break;
//                case CMD_APPLY_CHANGE:
//                    LOG("CMD_APPLY_CHANGE", "CMD_APPLY_CHANGE");
//                    handleChangeApply();
//                    break;
//                case CMD_OVERWRITING_CONFIRM:
//                    LOG("CMD_OVERWRITING_CONFIRM", "CMD_OVERWRITING_CONFIRM");
//                    handleConfirmOverWriting();
//                    break;
//                case CMD_GET_BUILD_INFO_ADDRESS:
//                    GetBuildInfoAddress(version_address_point_address, "04");//依据固件宏控定义 #define __APP_IMAGE_FLASH_OFFSET__ 20000 只要32个字节
//                    break;
//                case CMD_READY_FLASH_CONTENT:
//                    ReadyFlashContent();
//                    break;
//                case CMD_STOP_FLASH_CONTENT:
//                    GetFlashContentStop();
//                    break;
//                default:
//                    break;
//            }
        }
    }

}
