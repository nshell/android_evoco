package com.iir_eq.ui.activity;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.PersistableBundle;
import android.text.TextUtils;
import android.util.Log;

import android.view.MenuItem;
import android.view.View;

import android.widget.Button;

import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.evocolabs.app.OTACallback;
import com.iir_eq.R;
import com.iir_eq.bluetooth.BtHelper;
import com.iir_eq.bluetooth.callback.ConnectCallback;
import com.iir_eq.contants.Constants;
import com.iir_eq.ui.fragment.OtaConfigFragment;
import com.iir_eq.ui.fragment.OtaDaulPickFileFragment;
import com.iir_eq.util.ArrayUtil;
import com.iir_eq.util.FileUtils;
import com.iir_eq.util.LogUtils;
import com.iir_eq.util.Logger;
import com.iir_eq.util.ProfileUtils;
import com.iir_eq.util.SPHelper;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import com.google.android.material.bottomnavigation.BottomNavigationView;



/**
 * Created by zhaowanxing on 2017/7/12.
 */

public abstract class OtaActivity extends BaseActivity implements ConnectCallback
{

    protected static final String KEY_OTA_FILE = "ota_file";

    protected static final byte[] OTA_PASS_RESPONSE = new byte[]{0x11, 0x22};
    protected static final byte[] OTA_RESEND_RESPONSE = new byte[]{0x33, 0x44};

    protected static final byte[] OTA_DATA_ACK_FOR_SPP = new byte[]{(byte) 0x8B, 0x01};

    protected static final int DEFAULT_MTU = 512;
    protected static final int DEFAULT_MTU_SPP = 661;

    public static final int REQUEST_OTA_FILE = 0X00;
    public static final int REQUEST_DEVICE = 0x01;

    private static final int REQUEST_OTA_FILE_DAUL = 0x02;

    private static final int MSG_UPDATE_INFO = 0x00;
    private static final int MSG_UPDATE_PROGRESS = 0x01;
    private static final int MSG_OTA_TIME_OUT = 0x02;
    private static final int MSG_SEND_INFO_TIME_OUT = 0x03;
    private static final int MSG_UPDATE_RESULT_INFO = 0x04;
    private static final int MSG_UPDATE_VERSION = 0x05;
    private static final int MSG_UPDATE_OTA_DAUL_FILE_INFO = 0x06;
    private static final int MSG_UPDATE_OTA_CONNECT_STATE = 0x07;
    private static final int MSG_UPDATE_BT_CONNECTED_ADDRESS = 0x08;
    private static final int MSG_UPDATE_BT_CONNECTED_NAME = 0x09;

    private static final int MSG_GET_FIREWARE_VERSION_TIME_OUT = 0x10;
    private static final int MSG_RESUME_OTA_TIME_OUT = 0x11;

    private static final int MSG_UPDATE_FLASH_CONTENT_DETAILS = 0x12;
    private static final int MSG_UPDATE_FLASH_CONTENT_ITEM = 0x13;
    private static final int MSG_UPDATE_READ_VERSION_INFO = 0x14;
    private static final int MSG_HANDLE_OTA_INFO_FILE_REPORT = 0x15;
    protected static final int CMD_CONNECT = 0x80;
    protected static final int CMD_DISCONNECT = 0x81;
    protected static final int CMD_LOAD_FILE = 0x82;
    protected static final int CMD_START_OTA = 0x83;
    protected static final int CMD_OTA_NEXT = 0x84;
    protected static final int CMD_SEND_FILE_INFO = 0x85;
    protected static final int CMD_LOAD_FILE_FOR_NEW_PROFILE = 0x86;
    protected static final int CMD_RESEND_MSG = 0x88;
    protected static final int CMD_LOAD_FILE_FOR_NEW_PROFILE_SPP = 0x89;
    protected static final int CMD_RESUME_OTA_CHECK_MSG = 0x8C;   //resume
    protected static final int CMD_RESUME_OTA_CHECK_MSG_RESPONSE = 0x8D; //resume back
    protected static final int CMD_SEND_HW_INFO = 0x8E;   //   read current version
    protected static final int CMD_READ_CURRENT_VERSION_RESPONSE = 0x8F; // read version response

    protected static final int CMD_APPLY_THE_IMAGE_MSG = 0x99;   //   apply the image
    protected static final int CMD_APPLY_CHANGE = 0x9A;   //   apply the image
    protected static final int CMD_OVERWRITING_CONFIRM = 0x9B;


    protected static final int CMD_LOAD_OTA_CONFIG = 0x90;
    protected static final int CMD_START_OTA_CONFIG = 0x91;
    protected static final int CMD_OTA_CONFIG_NEXT = 0x92;

    protected static final int CMD_READY_FLASH_CONTENT = 0x93;
    protected static final int CMD_GET_BUILD_INFO_ADDRESS = 0x9C;
    protected static final int CMD_STOP_FLASH_CONTENT = 0x9D;



    protected static final int STATE_IDLE = 0x00;
    protected static final int STATE_CONNECTING = 0x01;
    protected static final int STATE_CONNECTED = 0x02;
    protected static final int STATE_DISCONNECTING = 0x03;
    protected static final int STATE_DISCONNECTED = 0x04;
    protected static final int STATE_OTA_ING = 0x05;
    protected static final int STATE_OTA_FAILED = 0x06;
    protected static final int STATE_OTA_CONFIG = 0x07;
    protected static final int STATE_BUSY = 0x0F;

    // YYF MODEFIED
    protected static final int STATE_OTA_SUCCESS = 0x0E;
    // FYY

    protected static final byte IMAGE_STEREO = (byte) 0x00;
    protected static final byte IMAGE_LEFT_EARBUD = (byte) 0x01;
    protected static final byte IMAGE_RIGHT_EARBUD = (byte) 0x10;
    protected static final byte IMAGE_BOTH_EARBUD_IN_ONE = (byte) 0x11;

    public static final int APPLY_STEREO = -1;
    protected static final int APPLY_LEFT_EARBUD_ONLY = 0;
    protected static final int APPLY_RIGHT_EARBUD_ONLY = 1;
    protected static final int APPLY_BOTH_EARBUD_IN_ONE = 2;
    protected static final int APPLY_BOTH_EARBUD_IN_TWO = 3;
    public static final int APPLY_STEREO_OLD_VERSION = 4;
    protected static final int APPLY_STEREO_UNDEFINED = 5;

    protected static final int DAUL_CONNECT_LEFT = 1;
    protected static final int DAUL_CONNECT_RIGHT = 2;
    protected static final int DAUL_CONNECT_STEREO = 0;

    protected volatile int mDaulConnectState = DAUL_CONNECT_STEREO;//stereo

    private boolean resume_enable = false;
    private String resume_file_path = null;

    private int upgradeStep = -1;
    private int resumeUpgradeWay = APPLY_STEREO_UNDEFINED;

    protected volatile int mState = STATE_IDLE;
    protected BluetoothDevice mDevice;

    protected boolean mExit = false;

    protected HandlerThread mCmdThread;
    protected CmdHandler mCmdHandler;
    protected byte[] mOtaResumeDataReq;
    protected byte[] mOtaResumeData;

    protected byte[][][] mOtaData;
    protected int mOtaPacketCount = 0;
    protected int mOtaPacketItemCount = 0;
    protected boolean mSupportNewOtaProfile = false;

    protected byte[][] mOtaConfigData;
    protected int mOtaConfigPacketCount = 0;

    protected int totalPacketCount = 0;

    protected Object mOtaLock = new Object();

    protected int mMtu;

    protected volatile boolean mWritten = true;

    private final String OTA_CONFIG_TAG = "ota_config";
    private OtaConfigFragment mOtaConfigDialog;

    private final String OTA_DAUL_PICK_FILE = "ota_daul_pick_file";
    private OtaDaulPickFileFragment motaDaulPickFileFragment;

    protected long castTime;//temp data for log
    protected long sendMsgFailCount = 0;//temp data for log
    protected long otaImgSize = 0;

    protected final int RECONNECT_MAX_TIMES = 5; // 5 times
    protected final int RECONNECT_SPAN = 3000; // 3 seconds
    protected int reconnectTimes = 0;
    protected boolean isgetDeviceInfo=false;

    protected int totalCount = 0;
    protected int failedCount = 0;
    protected int resumeSegment = 0;

    protected boolean resumeFlg = false;

    protected int segment_verify_error_time = 0;
    //    TextView mAddress;
//    TextView mName;
//    TextView mOtaFile;
//    TextView mOtaInfo;
    TextView mUpdateStatic;
    ProgressBar mOtaProgress;
    TextView mOtaStatus;

    /*evoco_jtt*/
    private String mOtaFile;
    private String mAddress;
    private String mName;
    private String mOtaInfo;
    private Button progress_ok;
    private Button progress_cancel;
    private String mcurrentVersionDetails;
    private String ota_info_list;
//    private String mUpdateStatic;
//    private String mOtaProgress;
//    private String mOtaStatus;

    private Switch ack_switch;
    private TextView ack_title;
    Button pickDevice, pickOtaFile, startOta,pickDevice1;
    public int daulApply = APPLY_STEREO_UNDEFINED; //-1为stereo状态 0，1，2，3为daul状态 5是待定状态
    private byte imageApply = IMAGE_STEREO;

    private AlertDialog.Builder  progress_show;
    private TextView progress_info,progress_info_list;

    /* YYF */
    Button left_volup,left_voldown,right_volup,right_voldown,get_battInfo;
    Button mild,mildToSever,sever,modeAutomatic,modeAggressive,modePassthrough;
    /* FYY */

    TextView send_details_info;
    private int daul_step = -1;
    private int stereo_flg = STEREO_OLD;
    protected static final int STEREO_NEW = 1;
    protected static final int STEREO_OLD = 0;
    protected static final int TWS = 2;
    private int dual_in_one_response_ok_time = 0;
    private int dual_apply_change_response = 0;
    private int over = 0;
    private String mOtaIngFile = "";
    public BluetoothAdapter bluetoothAdapter = null;

    protected static final int HARDWARE_UNDEFINE = 0;
    protected static final int HARDWARE_0x90 = 3;
    protected static final int HARDWARE_0x8C = 2;
    protected static final int HARDWARE_0x80 = 1;
    private int ota_response_ok = 0;
    private int img_overwriting_confirm_response_time = 0;
    private int hardWareType = HARDWARE_UNDEFINE;
 //   public TextView mcurrentVersionDetails;
    protected String activityName;
    protected Button connect;
    protected static final int FLASH_CONTENT_PACKITEM_NUM = 128;
    TextView flash_content;
    private int flash_content_byte_num = 0;
    private int flash_content_byte_total_num = 0;
    private int version_content_byte_num = 0;
    private int version_content_byte_total_num = 0;

    private String flash_content_str = "";
    private boolean flash_content_end = true;
    private String flash_content_file_path = null;
    private String flash_content_file_all_path = null;
    private byte[] flash_content_byte_from_fireware;
    private byte[] flash_content_byte;

    private int function = get_version_address;//read version : dump log;
    private final static int read_version = 0;
    private final static int dump_log = 1;
    private final static int get_version_address = 2;
    private Button btn_dump_log;

    private Button dump_log_read_fw_version;
    private final static String Version_length = "0400";
    private final static String version_address_point_address = "02000C"; //依据固件宏控定义 #define __APP_IMAGE_FLASH_OFFSET__ 20000 只要32个字节
    private byte[] version_content_all = new byte[1032];
    private byte[] version_content = new byte[1024];
    private TextView fw_version_info;
    private TextView fw_version_title;
    private int two_bins_in_one_step = -1;
    private int segment_right = 0;
    private int segment_left = 0;
//    private TextView ota_info_list;
    private TextView info_details;
    private String ota_info_String = "";
    public String ota_info_log_path = "";
    private int packnum = 0;
    public  boolean pick_new_file = false;
    public byte master_battery_level;
    public byte slave_battery_level;
    public byte master_volume_level;
    public byte slave_volume_level;
    public byte master_preset;
    public byte slave_preset;
    public byte denoise_mode;
    public byte streaning_mode;
    public byte is_music_playing;
    public boolean is_device_info_get;
    public boolean is_left_master=true;

// YYF EDIT
    public Context mContext;
    public OTACallback mOtaCallback;
// FYY END

    private  void clearAllBreakPointInfo()
    {
        byte[] clearbyte = new byte[32];
        for (int i = 0; i < clearbyte.length; i++) {
            clearbyte[i] = (byte) 0x00;
        }
        SPHelper.putPreference(mContext, Constants.KEY_OTA_RESUME_VERTIFY_RANDOM_CODE, ArrayUtil.toHex(clearbyte));
    }
//    public void onClick(View view)
//        {
//        switch (view.getId()) {
//            case R.id.pick_device:
//                if (isIdle()) {
//                    pickDevice(REQUEST_DEVICE);
//                }
//               break;
//            case R.id.pick_ota_file:
//                if (daulApply == APPLY_STEREO || daulApply == APPLY_STEREO_OLD_VERSION) {
//                    if (!(mState == STATE_OTA_ING)) {
//                        pickFile(REQUEST_OTA_FILE);
//                    }
//                }
//                else
//                {
//                    showChooseApplyDialog();
//                }
//                break;
//            case R.id.start_ota:
//                LOG("start_ota", daulApply + "");
//                if(pick_new_file == false)
//                {
//                    resume();
//                }
//                else
//                {
//                    pick_new_file = false;
//                }
//                if ((daulApply != APPLY_STEREO) && (daulApply != APPLY_STEREO_OLD_VERSION)) {
//                    readyOtaDaul();
//                }
//                else {
//                   readyOta();
//                }
//                break;
//            case R.id.dump_log_pick_device:
//                if (isIdle()) {
//                    pickDevice(REQUEST_DEVICE);
//                }
//                break;
//            case R.id.dump_log_read_fw_version:
//                function = get_version_address;
//                connectDevice();
//                break;
//            case R.id.connect_device_ota:
//                ota_info_log_path = FileUtils.writeOtaInfoReport();
//                connectDevice();
//                break;
//            case R.id.left_voldown:
//                leftVolDown();
//                break;
//            case R.id.left_volup:
//                leftVolUp();
//                break;
//            case R.id.right_voldown:
//                rightVolDown();
//                break;
//            case R.id.right_volup:
//                rightVolUp();
//                break;
//            case R.id.battery_info:
//                getDeviceInfo();
//                break;
//            case R.id.mild_btn:
//                setPresetMild();
//                break;
//            case R.id.mildToSever_btn:
//                setPresetMildToSever();
//                break;
//            case R.id.sever_btn:
//                setPresetSever();
//                break;
//            case R.id.aggressive:
//                setModeAggressive();
//                break;
//            case R.id.automatic:
//                setModeAutomatic();
//                break;
//            case R.id.passthrough:
//                setModePassthrough();
//                break;
//        }
//    }

    public void controlStartOta(){
        daulApply = APPLY_BOTH_EARBUD_IN_ONE;
        resumeUpgradeWay = APPLY_BOTH_EARBUD_IN_ONE;
        segment_right = 0;
        segment_left = 0;
        resume_enable = true;
        two_bins_in_one_step = -1;

        int i = getConnectBt();
        if (i == -1) {
            SPHelper.putPreference(mContext,Constants.KEY_FIRMWARE_TYPE,true);
            //pickDevice.setVisibility(View.VISIBLE);
            return;
        }
        else {
            //pickDevice.setVisibility(View.GONE);
            SPHelper.putPreference(mContext,Constants.KEY_FIRMWARE_TYPE,false);
            getConnectBtDetails(i);
        }
        SPHelper.putPreference(mContext, Constants.KEY_OTA_CONFIG_CLEAR_USER_DATA, false);
        SPHelper.putPreference(mContext, Constants.KEY_OTA_CONFIG_UPDATE_BT_ADDRESS, false);
        SPHelper.putPreference(mContext, Constants.KEY_OTA_CONFIG_UPDATE_BT_NAME, false);
        SPHelper.putPreference(mContext, Constants.KEY_OTA_CONFIG_UPDATE_BLE_ADDRESS, false);
        SPHelper.putPreference(mContext, Constants.KEY_OTA_CONFIG_UPDATE_BLE_NAME, false);
        mMsgHandler.postDelayed(new Runnable(){
            @Override
            public void run() {
                connectDevice();
            }
        },500);
        mMsgHandler.postDelayed(new Runnable(){
            @Override
            public void run() {
                pick_new_file = false;
                if(checkResumeState()==true)
                {

                    resume();
                }
            }
        },5000);
        mMsgHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SPHelper.putPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, mContext.getExternalFilesDir(null) + "/evoco_one.bin");
                resume_enable=true;
                readyOta();
            }
        },8000);
    }

    public void controlStopOta(){
        mOtaCallback=null;
    }

    public void resume()
    {
        String details = "";
        String tmp = "";
       if((resumeUpgradeWay == OtaActivity.APPLY_BOTH_EARBUD_IN_TWO)||(resumeUpgradeWay == OtaActivity.APPLY_LEFT_EARBUD_ONLY)||(resumeUpgradeWay == OtaActivity.APPLY_RIGHT_EARBUD_ONLY)||(resumeUpgradeWay == OtaActivity.APPLY_BOTH_EARBUD_IN_ONE))
       {
           if(resumeUpgradeWay == OtaActivity.APPLY_LEFT_EARBUD_ONLY )
           {
               packnum =(int)SPHelper.getPreference(mContext,Constants.KEY_PACK_NUM,0);
               if(packnum == 0)
               {
                   resume_enable = false;
                   return;
               }
               else {
                   resume_enable = true;
               }
               mOtaFile = mContext.getString(R.string.left_earbud_only) + ":\n" + resume_file_path;
               resume_enable = true;
               imageApply = IMAGE_LEFT_EARBUD;
               daulApply = resumeUpgradeWay;
           }
           else if(resumeUpgradeWay == OtaActivity.APPLY_BOTH_EARBUD_IN_ONE)
           {

               segment_right = 0;
               segment_left = 0;
               details = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
               details = mContext.getString(R.string.both_earbuds_in_one_bin) + ":\n" + details;
               mOtaFile = details;
               two_bins_in_one_step = 0;
               resume_enable = true;
               daulApply = resumeUpgradeWay;
               packnum =(int)SPHelper.getPreference(mContext,Constants.KEY_PACK_NUM,0);
               if(packnum == 0)
               {
                   resume_enable = false;
                   return;
               }
               else {
                   resume_enable = true;
               }
           }
           else if( resumeUpgradeWay == OtaActivity.APPLY_RIGHT_EARBUD_ONLY )
           {
               mOtaFile = mContext.getString(R.string.right_earbud_only) + ":\n" + resume_file_path;
               daulApply = resumeUpgradeWay;
               imageApply = IMAGE_RIGHT_EARBUD;
               resume_enable = true;
               packnum =(int)SPHelper.getPreference(mContext,Constants.KEY_PACK_NUM,0);
               if(packnum == 0)
               {
                   resume_enable = false;
                   return;
               }
               else {
                   resume_enable = true;
               }
           }
           else if(resumeUpgradeWay == OtaActivity.APPLY_BOTH_EARBUD_IN_TWO)
           {
               daulApply = resumeUpgradeWay;
               details =mContext.getString(R.string.both_earbuds_in_two_bins) + "\n";
               tmp = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
               details = details + mContext.getString(R.string.left_earbud_image) + ":" + tmp + "\n";
               tmp = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_RIGHT_FILE, "");
               details = details + mContext.getString(R.string.right_earbud_image) + ":" + tmp;
               mOtaFile = details;
               resume_enable = true;
               packnum =(int)SPHelper.getPreference(mContext,Constants.KEY_PACK_NUM,0);
               if(packnum == 0)
               {
                   resume_enable = false;
                   return;
               }
               else {
                   resume_enable = true;
               }
           }
       }
       else if((resumeUpgradeWay == OtaActivity.APPLY_STEREO)||(resumeUpgradeWay == OtaActivity.APPLY_STEREO_OLD_VERSION))
       {
           packnum =(int)SPHelper.getPreference(mContext,Constants.KEY_PACK_NUM,0);
           imageApply = IMAGE_STEREO;
           daulApply = resumeUpgradeWay;
           mOtaFile = SPHelper.getPreference(mContext, KEY_OTA_FILE, "").toString();
           if(packnum == 0)
           {
               resume_enable = false;
               return;
           }
           else {
               resume_enable = true;
           }
       }
    }

    public void readyOtaDaul()
    {
        timer_handler.sendEmptyMessage(1);
        if (TextUtils.isEmpty(mAddress)) {
            showToast(mContext.getString(R.string.pick_device_tips));
            return;
        }
        if (TextUtils.isEmpty(mOtaFile)) {
            showToast(mContext.getString(R.string.pick_File_tips));
            return;
        }
        else if (mState == STATE_CONNECTED && (daulApply != APPLY_STEREO)) {
            if(resume_enable == false) {
                daul_step = 0;
                dual_in_one_response_ok_time = 0;
                //mOtaConfigDialog.show(getSupportFragmentManager(), OTA_CONFIG_TAG);

                AlertDialog.Builder alertDialog=new AlertDialog.Builder(mContext);
                View connect_view = View.inflate(mContext,R.layout.ota_config,null);
                alertDialog
                        .setTitle("OTA config")
                        .setIcon(R.mipmap.ic_launcher)
                        .setView(connect_view)
                        .setCancelable(false)
                        .create();
                AlertDialog  mshow = alertDialog.show();
                mOtaConfigDialog = new OtaConfigFragment(exview,connect_view,mshow);
                mOtaConfigDialog.setOtaConfigCallback(mOtaConfigCallback);
            }
            else
            {
                if((daulApply == APPLY_LEFT_EARBUD_ONLY)||(daulApply==APPLY_RIGHT_EARBUD_ONLY))
                {
                    sendCmdDelayed(CMD_APPLY_THE_IMAGE_MSG, 0);
                }
                else if(daulApply==APPLY_BOTH_EARBUD_IN_ONE)
                {
                    two_bins_in_one_step = 0;
                    sendCmdDelayed(CMD_APPLY_THE_IMAGE_MSG, 0);
                }
                else if(daulApply==APPLY_BOTH_EARBUD_IN_TWO) {
                    daul_step = upgradeStep;
                    if (daul_step == 1) {
                        ota_response_ok = 1;
                    }
                    else {
                        daul_step = 0;
                        ota_response_ok = 0;
                    }
                    sendCmdDelayed(CMD_APPLY_THE_IMAGE_MSG, 0);
                }
            }
        }
    }

    public void connectDevice()
    {
        Log.i("connectDevice","mcurrentVersionDetails_jtt");
        if ((!((mAddress == "--") || mAddress == null)) || (mDevice != null)) {
            mcurrentVersionDetails = mContext.getString(R.string.old_ota_ways_version_tips);//Getting version information now, Pls wait.
             sendCmdDelayed(CMD_CONNECT, 0);
//            Log.d("connectDevice","mcurrentVersionDetails_jtt");
//            Log.d("connectDevice","mcurrentVersionDetails_jtt" + mContext.getString(R.string.old_ota_ways_version_tips));
        }
        else if (getConnectBt() == -1) {
            Log.d(TAG, "connectDevice: not connected");
            //showToast(mContext.getString(R.string.no_device_mention_tips));   //Pls pick the headphones that need to be upgraded in the settings and match them.
        }

    }

    private boolean checkResumeState()
    {
        String file = null;
        //resumeUpgradeWay = (int) SPHelper.getPreference(mContext, Constants.KEY_OTA_UPGRADE_WAY, OtaActivity.APPLY_STEREO_UNDEFINED);
        resumeUpgradeWay = APPLY_BOTH_EARBUD_IN_ONE;

        Log.e("checkResumeState", resumeUpgradeWay + "");
        if (resumeUpgradeWay == OtaActivity.APPLY_STEREO_UNDEFINED)   //2合1升级不适用断点续传
        {
            return false;
        }
        else if(resumeUpgradeWay == OtaActivity.APPLY_BOTH_EARBUD_IN_ONE)
        {
            file = mContext.getExternalFilesDir(null).getAbsolutePath()+"/evoco_one.bin";
            if(file == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else if(resumeUpgradeWay == OtaActivity.APPLY_BOTH_EARBUD_IN_TWO )
        {
            file = (String)SPHelper.getPreference(mContext,Constants.KEY_OTA_DAUL_RIGHT_FILE,"");
            if(file==null)
            {
                return false;
            }
            file = (String)SPHelper.getPreference(mContext,Constants.KEY_OTA_DAUL_LEFT_FILE,"");
            if(file == null)
            {
                return false;
            }
            upgradeStep = (int)SPHelper.getPreference(mContext,Constants.KEY_OTA_UPGRADE_STEP,-1);
            if(upgradeStep == -1) {
                return false;
            }
            else {
                daulApply = resumeUpgradeWay;
                daul_step = upgradeStep;
                Log.e("checkResumeState","daul_step = "+daul_step);
                return true;
            }

        }
        else if(resumeUpgradeWay==OtaActivity.APPLY_RIGHT_EARBUD_ONLY)
        {
            Log.e("checkResumeState","resumeUpgradeWay==OtaActivity.APPLY_RIGHT_EARBUD_ONLY");
            file = (String)SPHelper.getPreference(mContext,Constants.KEY_OTA_DAUL_RIGHT_FILE,"");
            if(file == null)
            {
                return false;
            }
            else
            {
                Log.e("file1111","file :"+file);
                daulApply = resumeUpgradeWay;
                resume_file_path = file;
                return true;
            }
        }
        else if(resumeUpgradeWay==OtaActivity.APPLY_LEFT_EARBUD_ONLY)
        {
            Log.e("checkResumeState","resumeUpgradeWay==OtaActivity.APPLY_LEFT_EARBUD_ONLY");
            file = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
            if((file == null)||(file==""))
            {
                Log.e("file1111","file == null");
                return false;
            }
            else
            {
                daulApply = resumeUpgradeWay;
                Log.e("file1111","file :"+file);
                resume_file_path = file;
                return true;
            }
        }
        return true;
    }

     public void initView(View view)
    {
            exview=view;

 //       getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        mAddress = view.findViewById(R.id.deviceaddress_text);
//        mName = view.findViewById(R.id.devices_text);
//        mOtaFile = (TextView) findViewById(R.id.ota_file);
//        mOtaInfo = view.findViewById(R.id.ota_info_text);
//        mUpdateStatic = (TextView) findViewById(R.id.update_static);
//        mOtaProgress = (ProgressBar) findViewById(R.id.ota_progress);
//        mOtaStatus = (TextView) findViewById(R.id.ota_status);
//
//        pickOtaFile = (Button) findViewById(R.id.pick_ota_file);
//        startOta = (Button) findViewById(R.id.start_ota);
//         mcurrentVersionDetails = view.findViewById(R.id.currentVersionDetails_text);
//        mOtaConnectDevice = (Button) findViewById(R.id.connect_device_ota);
//
//        pickOtaFile.setOnClickListener(this);
//        startOta.setOnClickListener(this);
//        mOtaConnectDevice.setOnClickListener(this);
//        mOtaFile.setText(SPHelper.getPreference(this, KEY_OTA_FILE, "").toString());
//        boolean ack_enable = (boolean)SPHelper.getPreference(getApplicationContext(),Constants.KEY_ACK_ENABLE,false);
 //       mName.setText(loadLastDeviceName());
 //       mAddress.setText(loadLastDeviceAddress());
//        mDevice = BtHelper.getRemoteDevice(this, mAddress.getText().toString());
//        ack_switch = (Switch)findViewById(R.id.ack_switch);
//        ack_title = (TextView)findViewById(R.id.ack_title);
///* YYF */
//        left_voldown = (Button)findViewById(R.id.left_voldown);
//        left_voldown.setOnClickListener(this);
//        left_volup = (Button)findViewById(R.id.left_volup);
//        left_volup.setOnClickListener(this);
//        right_voldown = (Button)findViewById(R.id.right_voldown);
//        right_voldown.setOnClickListener(this);
//        right_volup = (Button)findViewById(R.id.right_volup);
//        right_volup.setOnClickListener(this);
//        get_battInfo = (Button)findViewById(R.id.battery_info);
//        get_battInfo.setOnClickListener(this);
//        mild = (Button)findViewById(R.id.mild_btn);
//        mild.setOnClickListener(this);
//        mildToSever=(Button)findViewById(R.id.mildToSever_btn);
//        mildToSever.setOnClickListener(this);
//        sever=(Button)findViewById(R.id.sever_btn);
//        sever.setOnClickListener(this);
//        modeAggressive = (Button) findViewById(R.id.aggressive);
//        modeAggressive.setOnClickListener(this);
//        modeAutomatic = (Button) findViewById(R.id.automatic);
//        modeAutomatic.setOnClickListener(this);
//        modePassthrough = (Button) findViewById(R.id.passthrough);
//        modePassthrough.setOnClickListener(this);

///* FYY */
//        ack_title.setVisibility(View.GONE);
//        ack_switch.setVisibility(View.GONE);
//        ack_switch.setChecked(ack_enable);
//        ack_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
//        {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
//            {
//                if (isChecked)
//                {
//                    SPHelper.putPreference(getApplicationContext(),Constants.KEY_ACK_ENABLE,true);
//                    LOG("ack_enable = true;","ack_enable = true;");
//                }
//                else
//                {
//                    LOG("ack_enable = false;","ack_enable = false;");
//                    SPHelper.putPreference(getApplicationContext(),Constants.KEY_ACK_ENABLE,false);
//                }
//            }
//        });
//            mOtaConfigDialog = new OtaConfigFragment();
//        mOtaConfigDialog.setOtaConfigCallback(mOtaConfigCallback);
//
//

//
//        int i = getConnectBt();
//        if ((i == -1) || (activityName.equals("LeOtaActivity"))) {
//            pickDevice.setVisibility(View.VISIBLE);
//        }
//       else {
//            pickDevice.setVisibility(View.GONE);
//           getConnectBtDetails(i);
//        }
//        ota_info_list = view.findViewById(R.id.ota_info_list_text);
//        ota_info_String ="";
//		ota_info_log_path = "";
//		if(checkResumeState()==true)
//        {
//            resume();
//        }
    }
    public void initContext(Context context){
        mContext = context;
    }

    private Handler mMsgHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what) {
                case MSG_UPDATE_INFO:
                    LOG(TAG, "MSG_UPDATE_INFO");
                    mOtaInfo= msg.obj.toString();
                    ota_info_String = ota_info_String +"\n"+ msg.obj.toString();
                    ota_info_list = ota_info_String;
                    break;
                case MSG_UPDATE_RESULT_INFO:
                    LOG(TAG, "MSG_UPDATE_RESULT_INFO");
                    if (mUpdateStatic != null) {
                        mUpdateStatic.setText(msg.obj.toString());
                    }
                  //  progress_ok.setVisibility(View.VISIBLE);
                   // progress_cancel.setVisibility(View.VISIBLE);

                    break;
                case MSG_UPDATE_PROGRESS:
                    LOG(TAG, "MSG_UPDATE_PROGRESS");
                    if (mOtaProgress != null) {
                        mOtaProgress.setProgress((Integer) msg.obj);
                        mOtaStatus.setText((Integer) msg.obj + "%");
                    }
                    else {
                        LOG(TAG, "mOtaProgress is null");
                    }
                    break;
                case MSG_RESUME_OTA_TIME_OUT:
                    LOG("OtaActivity", "MSG_RESUME_OTA_TIME_OUT");
                    if (mOtaInfo != null) {
                        mOtaInfo= msg.arg1+"";
                        ota_info_String = ota_info_String +"\n"+ "MSG_RESUME_OTA_TIME_OUT";
                        ota_info_list = ota_info_String;
                    }
                    else {
                        LOG(TAG, "mOtaInfo is null");
                    }
                    sendCmdDelayed(msg.arg2, 0);
                    break;
                case MSG_GET_FIREWARE_VERSION_TIME_OUT:
                    LOG("OtaActivity", "MSG_GET_FIREWARE_VERSION_TIME_OUT");
                    mcurrentVersionDetails = "STEREO";
                    daulApply = APPLY_STEREO_OLD_VERSION;
                    //SPHelper.putPreference(OtaActivity.this, Constants.KEY_OTA_UPGRADE_WAY, APPLY_STEREO_OLD_VERSION);
                   // ack_switch.setVisibility(View.VISIBLE);
                    //ack_title.setVisibility(View.VISIBLE);
                    if (mOtaInfo != null) {
                        mOtaInfo = msg.arg1+"";
                        ota_info_String = ota_info_String +"\n"+ "MSG_GET_FIREWARE_VERSION_TIME_OUT";
                        ota_info_list =ota_info_String;
                    }
                    else {
                        LOG(TAG, "mOtaInfo is null");
                    }
                    break;
                case MSG_OTA_TIME_OUT:
                case MSG_SEND_INFO_TIME_OUT:
                    LOG("OtaActivity", "MSG_SEND_INFO_TIME_OUT|MSG_SEND_INFO_TIME_OUT time out");
                    LOG(TAG, "MSG_SEND_INFO_TIME_OUT|MSG_SEND_INFO_TIME_OUT time out");
                    if (mOtaInfo != null) {
                        mOtaInfo = msg.arg1+"";
                        ota_info_String = ota_info_String +"\n"+ "MSG_SEND_INFO_TIME_OUT";
                        ota_info_list =ota_info_String ;
                    }
                    else {
                        LOG(TAG, "mOtaInfo is null");
                    }
                    sendCmdDelayed(msg.arg2, 0);
                    break;
                case MSG_UPDATE_VERSION:
                    LOG(TAG, "MSG_UPDATE_VERSION");
                    mcurrentVersionDetails = msg.obj.toString();
                    break;

                case MSG_UPDATE_OTA_DAUL_FILE_INFO:
                    LOG(TAG, "MSG_UPDATE_OTA_DAUL_FILE_INFO");
                    HandleOtaFileShow();
                    String ota_file;
                    ota_file= mOtaFile;
                    if(pick_new_file == false)
                    {
                        resume();
                    }
                    else
                    {
                        pick_new_file = false;
                    }
                    if ((daulApply !=APPLY_STEREO) && (daulApply != APPLY_STEREO_OLD_VERSION)) {
                        readyOtaDaul();
                    }
                    else {
                        readyOta();

                    }
                    break;
                case MSG_UPDATE_OTA_CONNECT_STATE:
                    LOG(TAG, "MSG_UPDATE_OTA_DAUL_FILE_INFO");
                    if (activityName == "DumpLogActivity") {
                        return;
                    }
                    else {
                        if (msg.obj.toString().equals("true")) {
                        //    mOtaConnectDevice.setVisibility(View.GONE);
                            LOG(TAG, " mOtaConnectDevice.setVisibility(View.GONE)");
                        }
                        else {
                        //    mOtaConnectDevice.setVisibility(View.VISIBLE);
                            LOG(TAG, " mOtaConnectDevice.setVisibility(View.VISIBLE)");
                            mcurrentVersionDetails = "--";
                        }
                    }
                    break;
                case MSG_UPDATE_BT_CONNECTED_ADDRESS:
                    LOG(TAG, "MSG_UPDATE_BT_CONNECTED_ADDRESS");
                    mAddress = msg.obj.toString();

                    break;
                case MSG_UPDATE_BT_CONNECTED_NAME:
                    LOG(TAG, "MSG_UPDATE_BT_CONNECTED_NAME");
                    mName = msg.obj.toString();
                    break;
                case MSG_UPDATE_FLASH_CONTENT_DETAILS:
                    //flash_content.setText("正在获取数据中，请稍候...");

                    break;
                case MSG_UPDATE_FLASH_CONTENT_ITEM:
                    String str = msg.obj.toString() + "\n" + "文件路径:" + flash_content_file_path;
                    LOG("MSG_UPDATE_FL_ITEM str", str);
                    //flash_content.setText(str);
                    break;
                case MSG_UPDATE_READ_VERSION_INFO:
                    //fw_version_info.setVisibility(View.VISIBLE);
                   // fw_version_title.setVisibility(View.VISIBLE);
                   // fw_version_info.setText(msg.obj.toString());
                    Log.i("MSG_UPDATE_READ_INFO","fw_version_info "+ msg.obj.toString());
                    break;

                case MSG_HANDLE_OTA_INFO_FILE_REPORT:
                    MsgHandleOtaInfoFileReport();
                    break;


                default:// donot left the default , even nothing to do
            }
        }
    };

    private void MsgHandleOtaInfoFileReport()
    {
        try {
            FileOutputStream stream = new FileOutputStream(ota_info_log_path, true);
            stream.write(ota_info_String.getBytes("gbk"));
            stream.flush();
            stream.close();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        ota_info_String = "";
        ota_info_log_path = "";
    }

    private void HandleOtaFileShow()
    {
        String details = "";
        String tmp;
        if (daulApply == APPLY_LEFT_EARBUD_ONLY) {
            details = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");

            details =mContext.getString(R.string.left_earbud_only) + ":\n" + details;
            mOtaFile =details ;
            imageApply = IMAGE_LEFT_EARBUD;
            daul_step = -1;

        }
        else if (daulApply == APPLY_RIGHT_EARBUD_ONLY) {
            details = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_RIGHT_FILE, "");
            details = mContext.getString(R.string.right_earbud_only) + ":\n" + details;
            mOtaFile = details;
            imageApply = IMAGE_RIGHT_EARBUD;
            daul_step = -1;
        }
        else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE) {
            details = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
            details = mContext.getString(R.string.both_earbuds_in_one_bin) + ":\n" + details;
            mOtaFile = details;
            imageApply = IMAGE_BOTH_EARBUD_IN_ONE;
            daul_step = -1;
            dual_in_one_response_ok_time = 0;
            two_bins_in_one_step = 0;
			segment_right = 0;
            segment_left = 0;
        }
        else if (daulApply == APPLY_BOTH_EARBUD_IN_TWO) {
            details = mContext.getString(R.string.both_earbuds_in_two_bins) + "\n";
            tmp = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
            details = details + mContext.getString(R.string.left_earbud_image) + ":" + tmp + "\n";
            tmp = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_RIGHT_FILE, "");
            details = details + mContext.getString(R.string.right_earbud_image) + ":" + tmp;
            mOtaFile = details ;
            daul_step = 0;

        }
    }

    public boolean isIdle()
    {
        return mState == STATE_IDLE || mState == STATE_OTA_FAILED || mState == STATE_DISCONNECTED;
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
      super.onCreate(savedInstanceState);
//        LOG(TAG, "onCreate");
//        activityName = getActivityName();
//        LOG("onCreate", activityName);
//        pick_new_file = false;
//        if(getConnectBt() == -1)
//        {
//            SPHelper.putPreference(getApplicationContext(),Constants.KEY_FIRMWARE_TYPE,true);
//        }
//        else
//        {
//            SPHelper.putPreference(getApplicationContext(),Constants.KEY_FIRMWARE_TYPE,false);
//        }
//        if (activityName.equals("DumpLogActivity")) {
//            setContentView(R.layout.act_dumplog);
//            initDumpLogView();
//            initConfig();
//        }
//        else {
//            setContentView(R.layout.activity_main);
//            BottomNavigationView navView = findViewById(R.id.nav_view);
//            // Passing each menu ID as a set of Ids because each
//            // menu should be considered as top level destinations.
//            AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
//                    R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
//                    .build();
//            NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
//            NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
//            NavigationUI.setupWithNavController(navView, navController);
//            initView();
//            initConfig();
//        }
//
//        if (bluetoothAdapter == null) {
//            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
//        }

//
//
//
    }

    public void initDumpLogView()
    {
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        mAddress = (TextView) findViewById(R.id.dump_log_address);
//        mName = (TextView) findViewById(R.id.dump_log_name);
//        mOtaInfo = (TextView) findViewById(R.id.dump_log_ota_info);
//        pickDevice = (Button) findViewById(R.id.dump_log_pick_device);
//        dump_log_read_fw_version = (Button) findViewById(R.id.dump_log_read_fw_version);
//        flash_content = (TextView) findViewById(R.id.flash_content_details);
//        pickDevice.setOnClickListener(this);
//        dump_log_read_fw_version.setOnClickListener(this);
//        mName.setText(loadLastDeviceName());
//        mAddress.setText(loadLastDeviceAddress());
//        mDevice = BtHelper.getRemoteDevice(this, mAddress.getText().toString());
//        fw_version_info = (TextView) findViewById(R.id.fw_version_info);
//        fw_version_title = (TextView) findViewById(R.id.fw_version_title);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState, PersistableBundle persistentState)
    {
        super.onRestoreInstanceState(savedInstanceState, persistentState);
    }

    public void initConfig(OTACallback cb)
    {
        mCmdThread = new HandlerThread(TAG);
        mCmdThread.start();
        mCmdHandler = new CmdHandler(mCmdThread.getLooper());
        mOtaCallback = cb;
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        LOG(TAG, "onDestroy");
        sendCmdDelayed(CMD_DISCONNECT, 0);
        dual_in_one_response_ok_time = 0;
        daulApply = APPLY_STEREO_UNDEFINED;
        daul_step = -1;
        if (mMsgHandler != null) {
            mMsgHandler.removeMessages(MSG_SEND_INFO_TIME_OUT);
            mMsgHandler.removeMessages(MSG_OTA_TIME_OUT);
        }
        if (mCmdHandler != null) {
            mCmdHandler.removeMessages(CMD_OTA_NEXT);
            mCmdHandler.removeMessages(CMD_OTA_CONFIG_NEXT);
        }
        if (mCmdThread != null && mCmdThread.isAlive()) {
            mCmdThread.quit();
        }
        if (bluetoothAdapter != null) {
            bluetoothAdapter = null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
            case android.R.id.home:
                exit();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void exit()
    {
        LOG(TAG, "exit");
        if (isIdle()) {
            totalCount = 0;
            failedCount = 0;
            dual_in_one_response_ok_time = 0;
            daulApply = APPLY_STEREO_UNDEFINED;
            mMsgHandler.removeMessages(MSG_SEND_INFO_TIME_OUT);
            mExit = true;
            daul_step = -1;
            dual_in_one_response_ok_time = 0;
            dual_apply_change_response = 0;
            mDaulConnectState = DAUL_CONNECT_STEREO;
            imageApply = IMAGE_STEREO;
            mcurrentVersionDetails = "--" ;
            segment_right = 0;
            segment_left = 0;
            MsgHandleOtaInfoFileReport();
            pick_new_file = false;
        }
        else {
            showConfirmDialog(R.string.ota_exit_tips, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    exitOta();
                    finish();
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        LOG(TAG, "onActivityResult");
        if (requestCode == REQUEST_OTA_FILE) {
            onPickFile(resultCode, data);
        }
        else if (requestCode == REQUEST_DEVICE) {
            //onPickDevice(resultCode, data);
        }
    }

    @Override
    public void onConnectionStateChanged(boolean connected)
    {
        LOG(TAG, "onConnectionStateChanged " + connected + "; " + mState);
        if (connected) {
            onConnected();

        }
        else {
            removeTimeout();
            if (mState == STATE_CONNECTING) {
                updateConnectState("true");
                LOG(TAG, "mState == STATE_CONNECTING");
                reconnectTimes++;
                if (reconnectTimes > RECONNECT_MAX_TIMES) {
                    updateInfo(R.string.connect_failed);
                    mState = STATE_DISCONNECTED;
                    onOtaFailed();
                }
                else {
                    updateInfo(String.format(mContext.getString(R.string.connect_reconnect_try), reconnectTimes));
                    reconnect();
                }
            }
            else if (mState == STATE_OTA_ING) {
                updateConnectState("true");
                LOG(TAG, "mState == STATE_OTA_ING");
                onOtaFailed();
            }
            else if (mState != STATE_IDLE) {
                LOG(TAG, "mState != STATE_IDLE");
                updateInfo(R.string.disconnected);
                mState = STATE_DISCONNECTED;
                updateConnectState("false");
                onOtaFailed();
            }
            else if (mState == STATE_IDLE) {
                LOG(TAG, "mState == STATE_IDLE");
                updateInfo(R.string.disconnected);
                mState = STATE_DISCONNECTED;
                updateConnectState("false");
            }
        }
    }

    protected void updateInfo(int info)
    {
        updateInfo(mContext.getString(info));
    }

    protected void updateResultInfo(String info)
    {
        Message message = mMsgHandler.obtainMessage(MSG_UPDATE_RESULT_INFO);
        message.obj = info;
        mMsgHandler.sendMessage(message);
    }

    protected void updateInfo(String info)
    {
        Log.d(TAG, "updateInfo: "+info);;
        Message message = mMsgHandler.obtainMessage(MSG_UPDATE_INFO);
        message.obj = info;
        mMsgHandler.sendMessage(message);
    }

    protected void updateVersion(String info)
    {
        Message message = mMsgHandler.obtainMessage(MSG_UPDATE_VERSION);
        message.obj = info;
        mMsgHandler.sendMessage(message);
    }

    protected void updateDaulFile(String info)
    {
        Message message = mMsgHandler.obtainMessage(MSG_UPDATE_OTA_DAUL_FILE_INFO);
        message.obj = info;
        mMsgHandler.sendMessage(message);
    }

    protected void updateConnectState(String info)
    {
        Message message = mMsgHandler.obtainMessage(MSG_UPDATE_OTA_CONNECT_STATE);
        message.obj = info;
        mMsgHandler.sendMessage(message);
    }


    protected void updateProgress(int progress)
    {
        Message message = mMsgHandler.obtainMessage(MSG_UPDATE_PROGRESS);
        message.obj = progress;
        mMsgHandler.sendMessage(message);
    }

    protected void updateConnectedBtAddress(String address)
    {
        Message message = mMsgHandler.obtainMessage(MSG_UPDATE_BT_CONNECTED_ADDRESS);
        message.obj = address;
        mMsgHandler.sendMessage(message);
    }

    protected void updateConnectedBtName(String name)
    {
        Message message = mMsgHandler.obtainMessage(MSG_UPDATE_BT_CONNECTED_NAME);
        message.obj = name;
        mMsgHandler.sendMessage(message);
    }


    protected void sendCmdDelayed(int cmd, long millis)
    {
        mCmdHandler.removeMessages(cmd);
        if (millis == 0) {
            mCmdHandler.sendEmptyMessage(cmd);
        }
        else {
            mCmdHandler.sendEmptyMessageDelayed(cmd, millis);
        }
    }

    protected void sendTimeout(int info, int cmd, long millis)
    {
        LOG(TAG, "sendTimeout info " + info + " ; cmd " + cmd + " ; millis " + millis);
        Message message = mMsgHandler.obtainMessage(MSG_OTA_TIME_OUT);
        message.arg1 = info;
        message.arg2 = cmd;
        mMsgHandler.sendMessageDelayed(message, millis);
    }

    protected void removeTimeout()
    {
        LOG(TAG, "removeTimeout");
        mMsgHandler.removeMessages(MSG_OTA_TIME_OUT);
    }

    public void onPickDevice(int resultCode, BluetoothDevice data)
    {
//        mName = (TextView) findViewById(R.id.devices_text);
//        mAddress = (TextView) findViewById(R.id.deviceaddress_text);
        if (resultCode == RESULT_OK) {
            mDevice = data;
            if (mDevice != null) {
              //  saveLastDeviceName(mDevice.getName());
             //   saveLastDeviceAddress(mDevice.getAddress());
                Log.d("otaactivity","onPickDevice" + mDevice.getName());
                mAddress= mDevice.getAddress() ;
                mName= mDevice.getName();
            }
        }
    }

    public void onPickFile(int resultCode, Intent data)
    {
        if (resultCode == RESULT_OK) {
            clearAllBreakPointInfo();
            if(daulApply == APPLY_STEREO_OLD_VERSION) {
                ack_switch.setVisibility(View.VISIBLE);
                ack_title.setVisibility(View.VISIBLE);
            }
            else
            {
                ack_switch.setVisibility(View.GONE);
                ack_title.setVisibility(View.GONE);
            }
            resume_enable = false;
            mOtaData = null;
            String file = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
            SPHelper.putPreference(mContext, KEY_OTA_FILE, file);
            pick_new_file = true;
            mOtaFile=file;

        }
    }

    protected void onConnecting()
    {
        LOG(TAG, "onConnecting");
        LogUtils.writeForOTAStatic(TAG, "onConnecting ");
        castTime = System.currentTimeMillis();//startTime
        updateInfo(R.string.connecting_device);// wright log
        mState = STATE_CONNECTING;
        mOtaCallback.onOtaStateChanged(STATE_CONNECTING);
    }

    protected void onConnected()
    {
        LOG(TAG, "onConnected");
        boolean isOld = false;
        isOld  = (boolean)SPHelper.getPreference(mContext,Constants.KEY_FIRMWARE_TYPE,true);
        if (activityName == "DumpLogActivity") {
            if (function == get_version_address) {
                sendCmdDelayed(CMD_GET_BUILD_INFO_ADDRESS, 0);
                LogUtils.writeForOTAStatic(TAG, "onConnected ");
                updateInfo(R.string.connected);
                mState = STATE_CONNECTED;
                reconnectTimes = 0;
            }
        }
        else {
            updateConnectState("true");
            sendCmdDelayed(CMD_SEND_HW_INFO, 0);
            LogUtils.writeForOTAStatic(TAG, "onConnected ");
            updateInfo(R.string.connected);
            mState = STATE_CONNECTED;
            mOtaCallback.onOtaStateChanged(STATE_CONNECTED);
            reconnectTimes = 0;
        }


    }

    protected void onConnectFailed()
    {
        LOG(TAG, "onConnectFailed");
        LogUtils.writeForOTAStatic(TAG, "onConnectFailed " + ((System.currentTimeMillis() - castTime) / 1000));
        updateInfo(R.string.connect_failed);
        mState = STATE_DISCONNECTED;
    }

    protected void onLoadFileFailed()
    {
        LOG(TAG, "onLoadFileFailed");
        updateInfo(R.string.load_file_failed);
    }

    protected void onLoadFileSuccessfully()
    {
        LOG(TAG, "onLoadFileSuccessfully");
        updateInfo(R.string.load_file_successfully);

        sendCmdDelayed(CMD_START_OTA, 0);
    }

    protected void onLoadOtaConfigFailed()
    {
        LOG(TAG, "onLoadOtaConfigFailed");
        updateInfo(R.string.load_ota_config_failed);
    }

    protected void onLoadOtaConfigSuccessfully()
    {
        LOG(TAG, "onLoadOtaConfigSuccessfully");
        updateInfo(R.string.load_ota_config_successfully);
        sendCmdDelayed(CMD_START_OTA_CONFIG, 0);
    }

    protected void onOtaOver()
    {
        LOG(TAG, "onOtaOver");
        LOG("OtaActivity", "onOtaOver");
        totalCount++;
        mOtaCallback.onOtaStateChanged(STATE_OTA_SUCCESS);
        String result = "Result：OTA SUCCESSFULL !!! Total count = " + totalCount + "  Failure count = " + failedCount;
        updateResultInfo(result);
        LOG("OtaActivity", result);
        int updateTime = (int) ((System.currentTimeMillis() - castTime) / 1000);
        String msg = "Successful time-cost " + updateTime + " s" + " Retransmission count " + sendMsgFailCount + " Speed :" + otaImgSize / (updateTime == 0 ? otaImgSize : updateTime) + " B/s";
        LogUtils.writeForOTAStatic(TAG, msg);
        msg = "Successful time-cost " + updateTime + " s" + " Speed :" + otaImgSize / (updateTime == 0 ? otaImgSize : updateTime) + " B/s";
        updateInfo(msg);
        LOG("OtaActivity", msg);
        updateProgress(100);
        mOtaPacketCount = 0;
        resumeFlg = false;
        mOtaPacketItemCount = 0;
        mOtaConfigPacketCount = 0;
        mState = STATE_IDLE;
        daul_step = -1;
        dual_in_one_response_ok_time = 0;
        dual_apply_change_response = 0;
        SPHelper.putPreference(mContext,Constants.KEY_OTA_UPGRADE_WAY,OtaActivity.APPLY_STEREO_UNDEFINED);
        resume_enable = false;
        upgradeStep = -1;
        two_bins_in_one_step = -1;
        segment_right = 0;
        segment_left = 0;




    }

    protected void onOtaOverDaulOneStep()
    {
        LOG(TAG, "onOtaOverDaulOneStep");
        Log.e("OtaActivity", "onOtaOverDaulOneStep");
        totalCount++;
        mOtaCallback.onOtaStateChanged(STATE_OTA_SUCCESS);
        String result = "Result：Total count = " + totalCount + "  Failure count = " + failedCount;
        updateResultInfo(result);
        updateProgress(100);
        mOtaPacketCount = 0;
        mOtaPacketItemCount = 0;
        mOtaConfigPacketCount = 0;
        mOtaData = null;
        mState = STATE_IDLE;
    }

    protected void  onOtaOverSingleStep()
    {
        Log.e("OtaActivity", "onOtaOverSingleStep");
        String result = "Result：Total count = " + totalCount + "  Failure count = " + failedCount;
        updateResultInfo(result);
        updateProgress(100);
        mOtaPacketCount = 0;
        mOtaPacketItemCount = 0;
        mOtaConfigPacketCount = 0;
        resumeFlg = false;
        mOtaData = null;
        mState = STATE_IDLE;
    }

    protected void onOtaFailed()
    {
        Log.e("OtaActivity", "onOtaFailed");
        totalCount++;
        failedCount++;
        String result = "Result：Total count = " + totalCount + "  Failure count = " + failedCount;
        updateResultInfo(result);
        int updateTime = (int) ((System.currentTimeMillis() - castTime) / 1000);
        String msg = "";
        if (otaImgSize == 0) {
            msg = "Disconnected";
        }
        else {
            msg = "Failed time-cost " + updateTime + " s" + " Retransmission count " + sendMsgFailCount + " Speed :" + otaImgSize / (updateTime == 0 ? otaImgSize : updateTime) + " B/s";
        }
        LogUtils.writeForOTAStatic(TAG, msg);
        Log.e("OtaActivity", msg);
        if (otaImgSize == 0) {
            msg = "Disconnected";
        }
        else {
            msg = "Failed time-cost " + updateTime + " s" + " Speed :" + otaImgSize / (updateTime == 0 ? otaImgSize : updateTime) + " B/s";
        }
        updateInfo(msg);
        reconnectTimes = 0;
        mOtaPacketCount = 0;
        resumeFlg = false;
        mOtaPacketItemCount = 0;
        mOtaConfigPacketCount = 0;
        mState = STATE_OTA_FAILED;
        mOtaCallback.onOtaStateChanged(STATE_OTA_FAILED);
        mOtaData = null;
        SPHelper.putPreference(mContext,Constants.KEY_OTA_UPGRADE_STEP,daul_step);
        SPHelper.putPreference(mContext,Constants.KEY_OTA_UPGRADE_WAY,daulApply);
        daul_step = -1;
        dual_in_one_response_ok_time = 0;
        dual_apply_change_response = 0;
        two_bins_in_one_step = -1;
        segment_right = 0;
        segment_left = 0;

    }

    protected void onOtaConfigFailed()
    {
        updateInfo(R.string.ota_config_failed);
        mOtaCallback.onOtaStateChanged(STATE_OTA_FAILED);
        mOtaConfigData = null;
        mOtaConfigPacketCount = 0;
        mOtaConfigPacketCount = 0;
        mState = STATE_IDLE;
    }

    protected void onWritten()
    {
        mWritten = true;
        LOG(TAG, "onWritten mWritten = true");
    }

    protected void sendBreakPointCheckReq()
    {
        LOG("OtaActivity", "sendBreakPointCheckReq");
        if((daulApply == APPLY_BOTH_EARBUD_IN_ONE)&&(two_bins_in_one_step==0))
        {
            try {
                mOtaResumeDataReq = new byte[37 + 4 + 4];//依据协议BES_OTA_SPE_3.0.docx
                String randomCodestr = null;
                byte[] randomCode = new byte[32];
                randomCodestr = (String) SPHelper.getPreference(mContext, Constants.KEY_RESUME_VERTIFY_RANDOM_CODE_LEFT, "");
                if (randomCodestr == null || randomCodestr == "") {
                    for (int i = 0; i < randomCode.length; i++) {
                        randomCode[i] = (byte) 0x01;
                        mOtaResumeDataReq[i + 5] = (byte) 0x01;
                    }
                    SPHelper.putPreference(mContext, Constants.KEY_RESUME_VERTIFY_RANDOM_CODE_LEFT, ArrayUtil.toHex(randomCode));
                    Log.e("null fanxiaoli", ArrayUtil.toHex(randomCode));
                }
                else {
                    Log.e("randomCodestr", randomCodestr);
                    randomCode = ArrayUtil.toBytes(randomCodestr);
                    Log.e("sendBreakPoCodestr", "randomCodestr:" + randomCodestr);
                    Log.e("sendBreak PorandomCode", "fanxiaoli:" + ArrayUtil.toHex(randomCode));
                    for (int i = 0; i < randomCode.length; i++) {
                        mOtaResumeDataReq[i + 5] = randomCode[i];
                    }
                    Log.e("mOtaResumeDatnormally", "" + ArrayUtil.toHex(mOtaResumeDataReq));
                }

                mOtaResumeDataReq[0] = (byte) 0x8C;
                mOtaResumeDataReq[1] = (byte) 0x42;
                mOtaResumeDataReq[2] = (byte) 0x45;
                mOtaResumeDataReq[3] = (byte) 0x53;
                mOtaResumeDataReq[4] = (byte) 0x54;
                mOtaResumeDataReq[37] = (byte) 0x01;
                mOtaResumeDataReq[38] = (byte) 0x02;
                mOtaResumeDataReq[39] = (byte) 0x03;
                mOtaResumeDataReq[40] = (byte) 0x04;
                byte[] crc32_data = new byte[36];
                for (int i = 0; i < crc32_data.length; i++) {
                    crc32_data[i] = mOtaResumeDataReq[i + 5];
                }

                long crc32 = ArrayUtil.crc32(crc32_data, 0, 36);
                mOtaResumeDataReq[41] = (byte) crc32;
                mOtaResumeDataReq[42] = (byte) (crc32 >> 8);
                mOtaResumeDataReq[43] = (byte) (crc32 >> 16);
                mOtaResumeDataReq[44] = (byte) (crc32 >> 24);
                Log.e("mOtaResumeDataReq", "" + ArrayUtil.toHex(mOtaResumeDataReq));
                updateInfo(R.string.resume_request_verify);
                sendData(mOtaResumeDataReq);
                Message message = mMsgHandler.obtainMessage(MSG_RESUME_OTA_TIME_OUT);
                message.arg1 = R.string.old_ota_profile;
                message.arg2 = CMD_SEND_FILE_INFO;
                mMsgHandler.sendMessageDelayed(message, 5000);


            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Exception", e.getMessage().toString());
            } finally {

            }
        }
        else if((daulApply == APPLY_BOTH_EARBUD_IN_ONE)&&(two_bins_in_one_step== 1))
        {
            try {
                mOtaResumeDataReq = new byte[37 + 4 + 4];//依据协议BES_OTA_SPE_3.0.docx
                String randomCodestr = null;
                byte[] randomCode = new byte[32];
                randomCodestr = (String) SPHelper.getPreference(mContext, Constants.KEY_RESUME_VERTIFY_RANDOM_CODE_RIGHT, "");
                if (randomCodestr == null || randomCodestr == "") {
                    for (int i = 0; i < randomCode.length; i++) {
                        randomCode[i] = (byte) 0x01;
                        mOtaResumeDataReq[i + 5] = (byte) 0x01;
                    }
                    SPHelper.putPreference(mContext, Constants.KEY_RESUME_VERTIFY_RANDOM_CODE_RIGHT, ArrayUtil.toHex(randomCode));
                    Log.e("null fanxiaoli", ArrayUtil.toHex(randomCode));
                }
                else {
                    Log.e("randomCodestr", randomCodestr);
                    randomCode = ArrayUtil.toBytes(randomCodestr);
                    Log.e("sendBreakPoCodestr", "randomCodestr:" + randomCodestr);
                    Log.e("sendBreak PorandomCode", "fanxiaoli:" + ArrayUtil.toHex(randomCode));
                    for (int i = 0; i < randomCode.length; i++) {
                        mOtaResumeDataReq[i + 5] = randomCode[i];
                    }
                    Log.e("mOtaResumeDatnormally", "" + ArrayUtil.toHex(mOtaResumeDataReq));
                }

                mOtaResumeDataReq[0] = (byte) 0x8C;
                mOtaResumeDataReq[1] = (byte) 0x42;
                mOtaResumeDataReq[2] = (byte) 0x45;
                mOtaResumeDataReq[3] = (byte) 0x53;
                mOtaResumeDataReq[4] = (byte) 0x54;
                mOtaResumeDataReq[37] = (byte) 0x01;
                mOtaResumeDataReq[38] = (byte) 0x02;
                mOtaResumeDataReq[39] = (byte) 0x03;
                mOtaResumeDataReq[40] = (byte) 0x04;
                byte[] crc32_data = new byte[36];
                for (int i = 0; i < crc32_data.length; i++) {
                    crc32_data[i] = mOtaResumeDataReq[i + 5];
                }

                long crc32 = ArrayUtil.crc32(crc32_data, 0, 36);
                mOtaResumeDataReq[41] = (byte) crc32;
                mOtaResumeDataReq[42] = (byte) (crc32 >> 8);
                mOtaResumeDataReq[43] = (byte) (crc32 >> 16);
                mOtaResumeDataReq[44] = (byte) (crc32 >> 24);
                Log.e("mOtaResumeDataReq", "" + ArrayUtil.toHex(mOtaResumeDataReq));
                updateInfo(R.string.resume_request_verify);
                sendData(mOtaResumeDataReq);
                Message message = mMsgHandler.obtainMessage(MSG_RESUME_OTA_TIME_OUT);
                message.arg1 = R.string.old_ota_profile;
                message.arg2 = CMD_SEND_FILE_INFO;
                mMsgHandler.sendMessageDelayed(message, 5000);


            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Exception", e.getMessage().toString());
            } finally {

            }
        }
        else {
            try {
                mOtaResumeDataReq = new byte[37 + 4 + 4];//依据协议BES_OTA_SPE_3.0.docx
                String randomCodestr = null;
                byte[] randomCode = new byte[32];
                randomCodestr = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_RESUME_VERTIFY_RANDOM_CODE, "");
                if (randomCodestr == null || randomCodestr == "") {
                    for (int i = 0; i < randomCode.length; i++) {
                        randomCode[i] = (byte) 0x01;
                        mOtaResumeDataReq[i + 5] = (byte) 0x01;
                    }
                    SPHelper.putPreference(mContext, Constants.KEY_OTA_RESUME_VERTIFY_RANDOM_CODE, ArrayUtil.toHex(randomCode));
                    Log.e("null fanxiaoli", ArrayUtil.toHex(randomCode));
                }
                else {
                    Log.e("randomCodestr", randomCodestr);
                    randomCode = ArrayUtil.toBytes(randomCodestr);
                    Log.e("sendBreakPoCodestr", "randomCodestr:" + randomCodestr);
                    Log.e("sendBreak PorandomCode", "fanxiaoli:" + ArrayUtil.toHex(randomCode));
                    for (int i = 0; i < randomCode.length; i++) {
                        mOtaResumeDataReq[i + 5] = randomCode[i];
                    }
                    Log.e("mOtaResumeDatnormally", "" + ArrayUtil.toHex(mOtaResumeDataReq));
                }

                mOtaResumeDataReq[0] = (byte) 0x8C;
                mOtaResumeDataReq[1] = (byte) 0x42;
                mOtaResumeDataReq[2] = (byte) 0x45;
                mOtaResumeDataReq[3] = (byte) 0x53;
                mOtaResumeDataReq[4] = (byte) 0x54;
                mOtaResumeDataReq[37] = (byte) 0x01;
                mOtaResumeDataReq[38] = (byte) 0x02;
                mOtaResumeDataReq[39] = (byte) 0x03;
                mOtaResumeDataReq[40] = (byte) 0x04;
                byte[] crc32_data = new byte[36];
                for (int i = 0; i < crc32_data.length; i++) {
                    crc32_data[i] = mOtaResumeDataReq[i + 5];
                }

                long crc32 = ArrayUtil.crc32(crc32_data, 0, 36);
                mOtaResumeDataReq[41] = (byte) crc32;
                mOtaResumeDataReq[42] = (byte) (crc32 >> 8);
                mOtaResumeDataReq[43] = (byte) (crc32 >> 16);
                mOtaResumeDataReq[44] = (byte) (crc32 >> 24);
                Log.e("mOtaResumeDataReq", "" + ArrayUtil.toHex(mOtaResumeDataReq));
                updateInfo(R.string.resume_request_verify);
                sendData(mOtaResumeDataReq);
                Message message = mMsgHandler.obtainMessage(MSG_RESUME_OTA_TIME_OUT);
                message.arg1 = R.string.old_ota_profile;
                message.arg2 = CMD_SEND_FILE_INFO;
                mMsgHandler.sendMessageDelayed(message, 5000);


            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Exception", e.getMessage().toString());
            } finally {

            }
        }
    }

    protected void sendCmdSendHWInfo()
    {
        try {
            byte[] send = new byte[5];
            send[0] = (byte) 0x8C;
            send[1] = (byte) 0x42;
            send[2] = (byte) 0x45;
            send[3] = (byte) 0x53;
            send[4] = (byte) 0x54;
            Log.e("sendCmdSendHWInfo send", ArrayUtil.toHex(send));
            send_details_info.setText(ArrayUtil.toHex(send));
            sendData(send);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void sendFileInfo()
    {
        LOG(TAG, "sendFileInfo MSG_SEND_INFO_TIME_OUT");
        Log.e("daulApply", daulApply + "" + "sendFileInfo");
        String file_path = "";
        FileInputStream inputStream = null;
        try {
            if ((stereo_flg == STEREO_NEW)||(stereo_flg==STEREO_OLD)) {
                inputStream = new FileInputStream(mOtaFile);
            }
            else if (daulApply == APPLY_LEFT_EARBUD_ONLY) {
                file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
                Log.e("daulApply==0", file_path);
                inputStream = inputStream = new FileInputStream(file_path);
            }
            else if (daulApply == APPLY_RIGHT_EARBUD_ONLY) {
                file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_RIGHT_FILE, "");
                inputStream = inputStream = new FileInputStream(file_path);
                Log.e("daulApply==1", file_path);
            }
            else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE) {
                /***  YYF EDIT  ***/
                file_path = mContext.getExternalFilesDir(null).getAbsolutePath()+"/evoco_one.bin";
                inputStream = inputStream = new FileInputStream(file_path);
                Log.e("daulApply==2", file_path);
            }
            else if (daulApply == APPLY_BOTH_EARBUD_IN_TWO && daul_step == 0) {
                if (mDaulConnectState == DAUL_CONNECT_LEFT) {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_RIGHT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                }
                else if (mDaulConnectState == DAUL_CONNECT_RIGHT) {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                }
                Log.e("daulApply==3 s 0", file_path);
            }
            else if (daulApply == APPLY_BOTH_EARBUD_IN_TWO && daul_step == 1) {
                if (mDaulConnectState == DAUL_CONNECT_LEFT) {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                }
                else {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_RIGHT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                    Log.e("daulApply==3 s 1", file_path);
                }
            }
            else {
                showToast("请选择升级方式");
                return;
            }
            mOtaIngFile = file_path;
            int totalSize = inputStream.available();
            otaImgSize = totalSize ; //TODO :TEMP ADD
            int dataSize = totalSize - 4;
            int m = 0;
            if((dataSize%256)>0) {
                m = dataSize / 256;
                dataSize = (m+1)*256;
            }

            byte[] data = new byte[dataSize];
            inputStream.read(data, 0, dataSize);
            long crc32 = ArrayUtil.crc32(data, 0, dataSize);
            boolean ret  = sendData(new byte[]{(byte) 0x80, 0x42, 0x45, 0x53, 0x54, (byte) dataSize, (byte) (dataSize >> 8), (byte) (dataSize >> 16), (byte) (dataSize >> 24), (byte) crc32, (byte) (crc32 >> 8), (byte) (crc32 >> 16), (byte) (crc32 >> 24)});
            if(activityName=="LeOtaActivity"&&(ret==false)) {
                sendCmdDelayed(CMD_SEND_FILE_INFO, 3000);
            }
            else {
                Message message = mMsgHandler.obtainMessage(MSG_SEND_INFO_TIME_OUT);
                message.arg1 = R.string.old_ota_profile;
                message.arg2 = CMD_LOAD_FILE;
                mMsgHandler.sendMessageDelayed(message, 3000);
            }
        }catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void loadOtaConfig()
    {
        Logger.e(TAG, "loadOtaConfig");
        String file_path = "";
        FileInputStream inputStream = null;
        try {
            if ((stereo_flg == STEREO_NEW)||(stereo_flg==STEREO_OLD)){
                inputStream = new FileInputStream(mOtaFile);
            }
            else if (daulApply == APPLY_LEFT_EARBUD_ONLY) {
                file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
                Log.e("daulApply==0", file_path);
                inputStream = inputStream = new FileInputStream(file_path);
            }
            else if (daulApply == APPLY_RIGHT_EARBUD_ONLY) {
                file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_RIGHT_FILE, "");
                inputStream = inputStream = new FileInputStream(file_path);
                Log.e("daulApply==1", file_path);
            }
            else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE) {
                //file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
                file_path = mContext.getExternalFilesDir(null).getAbsolutePath()+"/evoco_one.bin";
                inputStream = inputStream = new FileInputStream(file_path);
                Log.e("daulApply==2", file_path);
            }
            else if (daulApply == APPLY_BOTH_EARBUD_IN_TWO && daul_step == 0) {
                if (mDaulConnectState == DAUL_CONNECT_LEFT) {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_RIGHT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                }
                else if (mDaulConnectState == DAUL_CONNECT_RIGHT) {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                }
                Log.e("daulApply==3 s 0", file_path);
            }
            else if (daulApply == APPLY_BOTH_EARBUD_IN_TWO && daul_step == 1) {
                if (mDaulConnectState == DAUL_CONNECT_LEFT) {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                }
                else {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_RIGHT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                    Log.e("daulApply==3 s 1", file_path);
                }
            }
            else {
                showToast("请选择升级方式");
                return;
            }
            mOtaIngFile = file_path;
            int totalSize = inputStream.available();
            int dataSize = totalSize;
            byte[] data = new byte[dataSize];
            inputStream.read(data, 0, dataSize);

            int configLength = 92;
            byte[] config = new byte[configLength];
            int lengthOfFollowingData = configLength - 4;
            config[0] = (byte) lengthOfFollowingData;
            config[1] = (byte) (lengthOfFollowingData >> 8);
            config[2] = (byte) (lengthOfFollowingData >> 16);
            config[3] = (byte) (lengthOfFollowingData >> 24);
            config[4] = data[dataSize - 4];
            config[5] = data[dataSize - 3];
            config[6] = data[dataSize - 2];

            boolean clearUserData = (boolean) SPHelper.getPreference(mContext, Constants.KEY_OTA_CONFIG_CLEAR_USER_DATA, Constants.DEFAULT_CLEAR_USER_DATA);
            boolean updateBtAddress = (boolean) SPHelper.getPreference(mContext, Constants.KEY_OTA_CONFIG_UPDATE_BT_ADDRESS, Constants.DEFAULT_UPDATE_BT_ADDRESS);
            boolean updateBtName = (boolean) SPHelper.getPreference(mContext, Constants.KEY_OTA_CONFIG_UPDATE_BT_NAME, Constants.DEFAULT_UPDATE_BT_NAME);
            boolean updateBleAddress = (boolean) SPHelper.getPreference(mContext, Constants.KEY_OTA_CONFIG_UPDATE_BLE_ADDRESS, Constants.DEFAULT_UPDATE_BLE_ADDRESS);
            boolean updateBleName = (boolean) SPHelper.getPreference(mContext, Constants.KEY_OTA_CONFIG_UPDATE_BLE_NAME, Constants.DEFAULT_UPDATE_BLE_NAME);
            byte enable = 0x00;
            enable |= (clearUserData ? 0x01 : 0x00);
            enable |= (updateBtName ? (0x01 << 1) : 0x00);
            enable |= (updateBleName ? (0x01 << 2) : 0x00);
            enable |= (updateBtAddress ? (0x01 << 3) : 0x00);
            enable |= (updateBleAddress ? (0x01 << 4) : 0x00);
            config[8] = enable;
            if (updateBtName) {
                String btName = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_CONFIG_UPDATE_BT_NAME_VALUE, "");
                byte[] btNameBytes = btName.getBytes();
                int btNameLength = btNameBytes.length;
                if (btNameLength > 32) {
                    btNameLength = 32;
                }
                for (int i = 0; i < btNameLength; i++) {
                    config[12 + i] = btNameBytes[i];
                }
            }
            if (updateBleName) {
                String bleName = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_CONFIG_UPDATE_BLE_NAME_VALUE, "");
                byte[] bleNameBytes = bleName.getBytes();
                int bleNameLength = bleNameBytes.length;
                if (bleNameLength > 32) {
                    bleNameLength = 32;
                }
                for (int i = 0; i < bleNameLength; i++) {
                    config[44 + i] = bleNameBytes[i];
                }
            }
            if (updateBtAddress) {
                String btAddress = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_CONFIG_UPDATE_BT_ADDRESS_VALUE, "");
                for (int i = 0; i < 6; i++) {
                    config[76 + 5 - i] = Integer.valueOf(btAddress.substring(i, i * 2 + 2), 16).byteValue();
                }
            }
            if (updateBleAddress) {
                String bleAddress = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_CONFIG_UPDATE_BLE_ADDRESS_VALUE, "");
                for (int i = 0; i < 6; i++) {
                    config[82 + 5 - i] = Integer.valueOf(bleAddress.substring(i, i * 2 + 2), 16).byteValue();
                }
            }
            long crc32 = ArrayUtil.crc32(config, 0, configLength - 4);
            config[88] = (byte) crc32;
            config[89] = (byte) (crc32 >> 8);
            config[90] = (byte) (crc32 >> 16);
            config[91] = (byte) (crc32 >> 24);

            int mtu = getMtu(stereo_flg);
            int packetPayload = mtu - 1;
            int packetCount = (configLength + packetPayload - 1) / packetPayload;
            mOtaConfigData = new byte[packetCount][];
            int position = 0;
            for (int i = 0; i < packetCount; i++) {
                if (position + packetPayload > configLength) {
                    packetPayload = configLength - position;
                }
                mOtaConfigData[i] = new byte[packetPayload + 1];
                mOtaConfigData[i][0] = (byte) 0x86;
                System.arraycopy(config, position, mOtaConfigData[i], 1, packetPayload);
                position += packetPayload;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            mOtaConfigData = null;
        } catch (IOException e) {
            e.printStackTrace();
            mOtaConfigData = null;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (mOtaConfigData != null) {
            onLoadOtaConfigSuccessfully();
        }
        else {
            onLoadOtaConfigFailed();
        }
    }

    protected void loadFileForNewProfile()
    {
        LOG(TAG, "loadFileForNewProfile");
        String file_path = "";
        mSupportNewOtaProfile = true;
        FileInputStream inputStream = null;
        try {
            if ((stereo_flg == STEREO_OLD)||(stereo_flg==STEREO_NEW)) {
                inputStream = new FileInputStream(mOtaFile);
            }
            else if (daulApply == APPLY_LEFT_EARBUD_ONLY) {
                file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
                Log.e("daulApply==0", file_path);
                inputStream = inputStream = new FileInputStream(file_path);
            }
            else if (daulApply == APPLY_RIGHT_EARBUD_ONLY) {
                file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_RIGHT_FILE, "");
                inputStream = inputStream = new FileInputStream(file_path);
                Log.e("daulApply==1", file_path);
            }
            else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE) {
                file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");

                inputStream = inputStream = new FileInputStream(file_path);
                Log.e("daulApply==2", file_path);
            }
            else if (daulApply == APPLY_BOTH_EARBUD_IN_TWO && daul_step == 0) {
                if (mDaulConnectState == DAUL_CONNECT_LEFT) {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_RIGHT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                }
                else if (mDaulConnectState == DAUL_CONNECT_RIGHT) {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                }
                Log.e("daulApply==3 s 0", file_path);
            }
            else if (daulApply == APPLY_BOTH_EARBUD_IN_TWO && daul_step == 1) {
                if (mDaulConnectState == DAUL_CONNECT_LEFT) {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                }
                else {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_RIGHT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                    Log.e("daulApply==3 s 1", file_path);
                }
            }
            else {
                showToast("请选择升级方式");
                return;
            }
            mOtaIngFile = file_path;
            int totalSize = inputStream.available();
            int dataSize = totalSize - 4;
            int m = 0;
            if((dataSize%256)>0) {
                m = dataSize / 256;
                dataSize = (m+1)*256;
            }
            int mtu = getMtu(stereo_flg);
            int packetPayload = ProfileUtils.calculateBLESinglePacketLen(dataSize, mtu, isBle(),stereo_flg);
            int totalPacketCount = (dataSize + packetPayload - 1) / packetPayload;
            int onePercentBytes = ProfileUtils.calculateBLEOnePercentBytes(dataSize,isBle(),stereo_flg);
            int crcCount = (dataSize + onePercentBytes - 1) / onePercentBytes;
            packnum = onePercentBytes;
            SPHelper.putPreference(mContext,Constants.KEY_PACK_NUM,packnum);
            this.totalPacketCount = totalPacketCount;
            mOtaData = new byte[crcCount + 1][][];
            Logger.e(TAG, "new profile imgeSize: " + dataSize + "; totalPacketCount " + totalPacketCount + "; onePercentBytes " + onePercentBytes + "; crcCount " + crcCount);
            byte[] data = new byte[dataSize];
            inputStream.read(data, 0, dataSize);
            int position = 0;
            for (int i = 0; i < crcCount; i++) {
                int crcBytes = onePercentBytes; //要校验百分之一的数据量
                if (packetPayload == 0) {
                    Log.e(TAG, ">>");
                }
                int length = (crcBytes + packetPayload - 1) / packetPayload; //根据MTU ，算出百分之一需要多少个包满足要求
                if (crcCount - 1 == i) { // 最后一包取余数
                    crcBytes = dataSize - position;
                    length = (crcBytes + packetPayload - 1) / packetPayload;
                }
                mOtaData[i] = new byte[length + 1][]; //加 1 为增加最后结束整包校验命令
                int realySinglePackLen = 0;
                int crcPosition = position;
                int tempCount = 0;
                for (int j = 0; j < length; j++) {
                    realySinglePackLen = packetPayload;
                    if (j == length - 1) { //每百分之一的最后一包取余数
                        realySinglePackLen = (crcBytes % packetPayload == 0) ? packetPayload : crcBytes % packetPayload;
                    }
                    mOtaData[i][j] = new byte[realySinglePackLen + 1];
                    System.arraycopy(data, position, mOtaData[i][j], 1, realySinglePackLen);
                    mOtaData[i][j][0] = (byte) 0x85;
                    position += realySinglePackLen;
                    tempCount += realySinglePackLen;
                }
                tempCount = 0;
                long crc32 = ArrayUtil.crc32(data, crcPosition, crcBytes);
                mOtaData[i][length] = new byte[]{(byte) 0x82, 0x42, 0x45, 0x53, 0x54, (byte) crc32, (byte) (crc32 >> 8), (byte) (crc32 >> 16), (byte) (crc32 >> 24)};
            }
            mOtaData[crcCount] = new byte[1][];
            mOtaData[crcCount][0] = new byte[]{(byte) 0x88};
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            mOtaData = null;
        } catch (IOException e) {
            e.printStackTrace();
            mOtaData = null;
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (mOtaData == null) {
            onLoadFileFailed();
        }
        else {
            onLoadFileSuccessfully();
        }
    }

    @Deprecated
    protected void loadFileForNewProfileSPP()
    {
        LOG(TAG, "loadFileForNewProfile");
        String file_path = "";
        mSupportNewOtaProfile = true;
        FileInputStream inputStream = null;
        try {
            if ((stereo_flg == STEREO_OLD)||(stereo_flg==STEREO_NEW)) {
                inputStream = new FileInputStream(mOtaFile);
            }
            else if (daulApply == APPLY_LEFT_EARBUD_ONLY) {
                file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
                Log.e("daulApply==0", file_path);
                inputStream = inputStream = new FileInputStream(file_path);
            }
            else if (daulApply == APPLY_RIGHT_EARBUD_ONLY) {
                file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_RIGHT_FILE, "");
                inputStream = inputStream = new FileInputStream(file_path);
                Log.e("daulApply==1", file_path);
            }
            else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE) {
//                file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
                file_path = mContext.getExternalFilesDir(null) + "/evoco_one.bin";
                inputStream = inputStream = new FileInputStream(file_path);
                Log.e("daulApply==2", file_path);
            }
            else if (daulApply == APPLY_BOTH_EARBUD_IN_TWO && daul_step == 0) {
                if (mDaulConnectState == DAUL_CONNECT_LEFT) {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_RIGHT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                }
                else if (mDaulConnectState == DAUL_CONNECT_RIGHT) {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                }
                Log.e("daulApply==3 s 0", file_path);
            }
            else if (daulApply == APPLY_BOTH_EARBUD_IN_TWO && daul_step == 1) {
                if (mDaulConnectState == DAUL_CONNECT_LEFT) {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                }
                else {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_RIGHT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                    Log.e("daulApply==3 s 1", file_path);
                }
            }
            else {
                showToast("请选择升级方式");
                return;
            }
            mOtaIngFile = file_path;
            int totalSize = inputStream.available();
            int dataSize = totalSize - 4;
            int mtu = getMtu(stereo_flg);
            int packetPayload = ProfileUtils.calculateSppSinglePacketLen(dataSize);
            int packetTotalCount = ProfileUtils.calculateSppTotalPacketCount(dataSize);
            int crcCount = ProfileUtils.calculateSppTotalCrcCount(dataSize);
            mOtaData = new byte[crcCount + 1][][];
            Logger.e(TAG, "new profile totalLength: " + totalSize + "; packetTotalCount " + packetTotalCount + "; packet payload " + packetPayload + "; crcCount " + crcCount);
            byte[] data = new byte[dataSize];
            inputStream.read(data, 0, dataSize);
            int position = 0;
            for (int i = 0; i < crcCount; i++) {
                int startIndex = (int) Math.ceil(i * 1.0 / crcCount * packetTotalCount);
                int endIndex = (int) Math.ceil((i + 1) * 1.0 / crcCount * packetTotalCount);
                int length = endIndex - startIndex;
                int crcPosition = position;
                mOtaData[i] = new byte[length + 1][];
                for (int j = 0; j < length; j++) {
                    if (position + packetPayload > dataSize) {
                        packetPayload = dataSize - position;
                    }
                    mOtaData[i][j] = new byte[packetPayload + 1];
                    System.arraycopy(data, position, mOtaData[i][j], 1, packetPayload);
                    mOtaData[i][j][0] = (byte) 0x85;
                    position += packetPayload;
                }
                long crc32 = ArrayUtil.crc32(data, crcPosition, position - crcPosition);
                mOtaData[i][length] = new byte[]{(byte) 0x82, 0x42, 0x45, 0x53, 0x54, (byte) crc32, (byte) (crc32 >> 8), (byte) (crc32 >> 16), (byte) (crc32 >> 24)};
            }
            mOtaData[crcCount] = new byte[1][];
            mOtaData[crcCount][0] = new byte[]{(byte) 0x88};
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            mOtaData = null;
        } catch (IOException e) {
            e.printStackTrace();
            mOtaData = null;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (mOtaData == null) {
            onLoadFileFailed();
        }
        else {
            onLoadFileSuccessfully();
        }
    }

    protected void loadFile()
    {
        LOG(TAG, "loadFile");
        mSupportNewOtaProfile = false;
        String file_path = "";
        FileInputStream inputStream = null;
        try {
            if ((stereo_flg == STEREO_OLD)||(stereo_flg==STEREO_NEW)) {
                inputStream = new FileInputStream(mOtaFile);
            }
            else if (daulApply == APPLY_LEFT_EARBUD_ONLY) {
                file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
                Log.e("daulApply==0", file_path);
                inputStream = inputStream = new FileInputStream(file_path);
            }
            else if (daulApply == APPLY_RIGHT_EARBUD_ONLY) {
                file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_RIGHT_FILE, "");
                inputStream = inputStream = new FileInputStream(file_path);
                Log.e("daulApply==1", file_path);
            }
            else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE) {
                // file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
                file_path = mContext.getExternalFilesDir(null) + "/evoco_on.bin";
                inputStream = inputStream = new FileInputStream(file_path);
                Log.e("daulApply==2", file_path);
            }
            else if (daulApply == APPLY_BOTH_EARBUD_IN_TWO && daul_step == 0) {
                if (mDaulConnectState == DAUL_CONNECT_LEFT) {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_RIGHT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                }
                else if (mDaulConnectState == DAUL_CONNECT_RIGHT) {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                }
                Log.e("daulApply==3 s 0", file_path);
            }
            else if (daulApply == APPLY_BOTH_EARBUD_IN_TWO && daul_step == 1) {
                if (mDaulConnectState == DAUL_CONNECT_LEFT) {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_LEFT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                }
                else {
                    file_path = (String) SPHelper.getPreference(mContext, Constants.KEY_OTA_DAUL_RIGHT_FILE, "");
                    inputStream = inputStream = new FileInputStream(file_path);
                    Log.e("daulApply==3 s 1", file_path);
                }
            }
            else {
                showToast(mContext.getString(R.string.pls_choose_the_way_to_upgrade));
                return;
            }
            mOtaIngFile = file_path;
            int totalSize = inputStream.available();
            int dataSize = totalSize - 4;
            int mtu = getMtu(stereo_flg);
            int packetPayload = 256; //mtu - 16;
            int packetCount = (dataSize + packetPayload - 1) / packetPayload;

            mOtaData = new byte[packetCount][][];
            int position = 0;
            Logger.e(TAG, "totalLength: " + totalSize + " packetCount " + packetCount + " packet payload " + packetPayload);
            for (int i = 0; i < packetCount; i++) {
                if (position + 256 > dataSize) {
                    packetPayload = dataSize - position;
                }
                mOtaData[i] = new byte[1][];
                mOtaData[i][0] = new byte[packetPayload + 16];
                inputStream.read(mOtaData[i][0], 16, packetPayload);
                long crc32 = ArrayUtil.crc32(mOtaData[i][0], 16, packetPayload);
                mOtaData[i][0][0] = (byte) 0xBE;
                mOtaData[i][0][1] = 0x64;
                mOtaData[i][0][2] = (byte) packetCount;
                mOtaData[i][0][3] = (byte) (packetCount >> 8);
                mOtaData[i][0][4] = (byte) packetPayload;
                mOtaData[i][0][5] = (byte) (packetPayload >> 8);
                mOtaData[i][0][6] = (byte) (packetPayload >> 16);
                mOtaData[i][0][7] = (byte) (packetPayload >> 24);
                mOtaData[i][0][8] = (byte) crc32;
                mOtaData[i][0][9] = (byte) (crc32 >> 8);
                mOtaData[i][0][10] = (byte) (crc32 >> 16);
                mOtaData[i][0][11] = (byte) (crc32 >> 24);
                mOtaData[i][0][12] = (byte) i;
                mOtaData[i][0][13] = (byte) (i >> 8);
                mOtaData[i][0][14] = 0x00;
                mOtaData[i][0][15] = (byte) ~ArrayUtil.checkSum(mOtaData[i][0], 15);
                position += packetPayload;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            mOtaData = null;
        } catch (IOException e) {
            e.printStackTrace();
            mOtaData = null;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (mOtaData == null) {
            onLoadFileFailed();
        }
        else {
            onLoadFileSuccessfully();
        }
    }

    public void readyOta()
    {
        timer_handler.sendEmptyMessage(1);
        LOG(TAG, "readyOta " + mState);
//        if (TextUtils.isEmpty(mAddress)) {
//            showToast(mContext.getString(R.string.pick_device_tips));
//            return;
//        }
//        else if (TextUtils.isEmpty(mOtaFile) || daulApply == APPLY_STEREO_UNDEFINED) {
//            showToast(mContext.getString(R.string.pick_File_tips));
//            return;
//        }
//        else
        if(resume_enable == true)
        {
            if (daulApply == APPLY_STEREO_OLD_VERSION) {
                sendCmdDelayed(CMD_RESUME_OTA_CHECK_MSG, 0);
            }
            else {
                sendCmdDelayed(CMD_APPLY_THE_IMAGE_MSG, 0);
            }
        }
        else {
           // mOtaConfigDialog.show(getSupportFragmentManager(), OTA_CONFIG_TAG);

        }
    }

    protected void reconnect()
    {
        LOG(TAG, "reconnect " + mState + " SPAN TIME IS " + RECONNECT_SPAN);
        LogUtils.writeForOTAStatic(TAG, "reconnect " + mState + " SPAN TIME IS " + RECONNECT_SPAN);
        mState = STATE_IDLE;
        if (isIdle()) {
            updateProgress(0);
            sendCmdDelayed(CMD_CONNECT, RECONNECT_SPAN);
        }
    }


    protected void startOta()
    {
        LOG(TAG, "startOta " + mSupportNewOtaProfile);
        if (daulApply == APPLY_BOTH_EARBUD_IN_TWO) {
            String str = mContext.getString(R.string.ota_file_on_going_is) + mOtaIngFile;
            updateInfo(str);
        }
        else {
            updateInfo(R.string.ota_ing);
        }
        mState = STATE_OTA_ING;
        mOtaCallback.onOtaProgressUpdated(STATE_OTA_ING);
        sendCmdDelayed(CMD_OTA_NEXT, 0);

    }

    protected void startOtaConfig()
    {
        Logger.e(TAG, "startOta " + mState);
        mState = STATE_OTA_CONFIG;
        mOtaCallback.onOtaProgressUpdated(STATE_OTA_CONFIG);
        sendCmdDelayed(CMD_OTA_CONFIG_NEXT, 0);
    }

    protected void pickFile(int request)
    {
        startActivityForResult(new Intent(this, FilePickerActivity.class), request);
    }

    protected void otaNext()
    {
        synchronized (mOtaLock) {
            Log.e("otaNext  -> mState ", mState + "");
            if (mOtaData == null) {
                Log.e("otaNext", "mOtaData == null");
            }
            if (mState != STATE_OTA_ING || mOtaData == null) {
                LOG(TAG, "otaNext  -> mState != STATE_OTA_ING || mOtaData == null ");
                return;
            }
            if (mOtaPacketCount == mOtaData.length) {
                LOG(TAG, "otaNext -> mState != STATE_OTA_ING || mOtaData == null ");
                return;
            }
            mOtaCallback.onOtaProgressUpdated(mOtaPacketCount);
            LOG(TAG, "otaNext totalPacketCount = " + totalPacketCount + " ; subCount " + mOtaPacketCount + "; " + mOtaPacketItemCount + "; " + mOtaData[mOtaPacketCount].length);

            if (mSupportNewOtaProfile || mWritten) {

                if ((mOtaPacketItemCount < mOtaData[mOtaPacketCount].length)) {
                    boolean sendRet = sendData(mOtaData[mOtaPacketCount][mOtaPacketItemCount]);
                    if (!sendRet) {
                        LOG(TAG, "otaNext write failed , try to resend");
                        sendCmdDelayed(CMD_OTA_NEXT, 40);
                    }
                    else {
                        if (!mSupportNewOtaProfile && mOtaPacketCount == mOtaData.length - 1) {
                            onOtaOver();
                            return;
                        }
                        if (mOtaPacketItemCount == 0) {
                            LOG(TAG, "---------------------------------START--------------------------------------");
                            LOG(TAG, ">>START " + ArrayUtil.toHex(mOtaData[mOtaPacketCount][mOtaPacketItemCount]));
                        }
                        else if (mOtaPacketItemCount == mOtaData[mOtaPacketCount].length - 1) {
                            LOG(TAG, "---------------------------------END--------------------------------------");
                            LOG(TAG, ">>END " + ArrayUtil.toHex(mOtaData[mOtaPacketCount][mOtaPacketItemCount]));
                        }
                        mOtaPacketItemCount++;
                        if (mOtaPacketItemCount == mOtaData[mOtaPacketCount].length) {
                            removeTimeout();
                            sendTimeout(R.string.ota_time_out, CMD_DISCONNECT,  30000);   //RESEND
                        }
                        else {
                            removeTimeout();
                            sendTimeout(R.string.ota_time_out, CMD_RESEND_MSG, 10000);   //RESEND
                        }
                    }
                }
            }
            else {
                LOG(TAG, "otaNext  -> (mSupportNewOtaProfile || mWritten) is false  " + mSupportNewOtaProfile + " ;" + mWritten);
            }
        }
    }

    protected void otaConfigNext()
    {
        synchronized (mOtaLock) {
            Log.e("otaConfigNext mState", mState + "");
            if (mOtaConfigData == null) {
                Log.e("ConfigNext  ConfigData", "==null");
            }
            if (mState != STATE_OTA_CONFIG || mOtaConfigData == null) {
                LOG(TAG, "otaConfigNext mState != STATE_OTA_CONFIG || mOtaConfigData == null");
                return;
            }
            if (mOtaConfigPacketCount == mOtaConfigData.length) {
                LOG(TAG, "otaConfigNext mOtaConfigPacketCount == mOtaConfigData.length");
                return;
            }
            LOG(TAG, "otaConfigNext " + mOtaConfigPacketCount + "; " + mOtaConfigData.length + " mWritten = " + mWritten);
            if (true) {
                if (!sendData(mOtaConfigData[mOtaConfigPacketCount])) {
                    Logger.e(TAG, "otaConfigNext write failed");
                    sendCmdDelayed(CMD_OTA_CONFIG_NEXT, 10);
                }
                else {
                    mOtaConfigPacketCount++;
                    if (mOtaConfigPacketCount == mOtaConfigData.length) {
                        sendTimeout(R.string.ota_config_time_out, CMD_DISCONNECT, 5000);
                    }
                }
            }
        }
    }

    protected void exitOta()
    {
        SPHelper.putPreference(mContext,Constants.KEY_OTA_UPGRADE_STEP,daul_step);
        removeTimeout();
        sendCmdDelayed(CMD_DISCONNECT, 0);
        dual_in_one_response_ok_time = 0;
        daul_step = 0;
        daulApply = APPLY_STEREO_UNDEFINED;
        mMsgHandler.removeMessages(MSG_SEND_INFO_TIME_OUT);
        mExit = true;
        resume_enable = false;
        mDaulConnectState = DAUL_CONNECT_STEREO;
        imageApply = IMAGE_STEREO;
        mcurrentVersionDetails = "--";
        segment_right = 0;
        segment_left = 0;
        MsgHandleOtaInfoFileReport();
        pick_new_file = false;
    }

    protected void otaNextDelayed(long millis)
    {
        boolean isOld  = (boolean)SPHelper.getPreference(mContext,Constants.KEY_FIRMWARE_TYPE,true);
        boolean ack_enable = (boolean)SPHelper.getPreference(mContext,Constants.KEY_ACK_ENABLE,false);
        synchronized (mOtaLock) {
            if (mState == STATE_OTA_ING) {
                if(isBle()||((isOld==true)&&(ack_enable==false))) {
                    sendCmdDelayed(CMD_OTA_NEXT, millis);
                }
            }
            else if (mState == STATE_OTA_CONFIG) {
                sendCmdDelayed(CMD_OTA_CONFIG_NEXT, millis);
            }
        }
    }

    //0x8E
    private void handleGetCurrentVersion()
    {
        LOG(TAG, "handleGetCurrentVersion");
        int time = 0;
        int resume_time = 0;
        try {

            boolean sendRet;
            sendRet = sendData(new byte[]{(byte) 0x8E, 0x42, 0x45, 0x53, 0x54});
            Message message = mMsgHandler.obtainMessage(MSG_GET_FIREWARE_VERSION_TIME_OUT);
            message.arg1 = R.string.old_ota_profile;
            message.arg2 = R.string.old_ota_profile;
            mMsgHandler.sendMessageDelayed(message, 3000);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }

    private void handleApplyTheImage()
    {
        LOG(TAG, "handleApplyTheImage " + "CMD_APPLY_THE_IMAGE_MSG");
        FileInputStream inputStream = null;
        //先升级对耳，再升级本耳
        if (daulApply == APPLY_BOTH_EARBUD_IN_TWO && daul_step == 0) {
            if (mDaulConnectState == DAUL_CONNECT_LEFT) {
                LOG(TAG,"daulApply == APPLY_BOTH_EARBUD_IN_TWO && daul_step == 0 mDaulConnectState == DAUL_CONNECT_LEFT");
                imageApply = IMAGE_RIGHT_EARBUD;
            }
            else if (mDaulConnectState == DAUL_CONNECT_RIGHT) {
                LOG(TAG,"daulApply == APPLY_BOTH_EARBUD_IN_TWO && daul_step == 0 mDaulConnectState == DAUL_CONNECT_RIGHT");
                imageApply = IMAGE_LEFT_EARBUD;
            }
        }
        else if (daulApply == APPLY_BOTH_EARBUD_IN_TWO && daul_step == 1) {
            if (mDaulConnectState == DAUL_CONNECT_LEFT) {
                LOG(TAG,"daulApply == APPLY_BOTH_EARBUD_IN_TWO && daul_step == 1 mDaulConnectState == DAUL_CONNECT_LEFT");
                imageApply = IMAGE_LEFT_EARBUD;
            }
            else if (mDaulConnectState == DAUL_CONNECT_RIGHT) {
                LOG(TAG,"daulApply == APPLY_BOTH_EARBUD_IN_TWO && daul_step == 1 mDaulConnectState == DAUL_CONNECT_RIGHT");
                imageApply = IMAGE_RIGHT_EARBUD;
            }
        }
        else if((daulApply == APPLY_BOTH_EARBUD_IN_ONE)&&(two_bins_in_one_step == 0))
        {
            imageApply = IMAGE_LEFT_EARBUD;
            dual_in_one_response_ok_time =  -1;
        }
        else if((daulApply == APPLY_BOTH_EARBUD_IN_ONE)&&(two_bins_in_one_step == 1))
        {
            imageApply = IMAGE_RIGHT_EARBUD;
            dual_in_one_response_ok_time =  -1;
        }
        else if((daulApply == APPLY_BOTH_EARBUD_IN_ONE)&&(two_bins_in_one_step==2))
        {
            imageApply = IMAGE_BOTH_EARBUD_IN_ONE;
            dual_in_one_response_ok_time = 0;
        }
        try {

            byte[] data = new byte[2];
            data[0] = (byte) 0x90;
            data[1] = imageApply;
            sendData(data);
            String str = ArrayUtil.toHex(data);
            LOG("handleApplyTheImage", str);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void handleChangeApply()
    {
        try {
            byte[] data = new byte[2];
            data[0] = (byte) 0x90;
            data[1] = 0x11;
            sendData(data);
            String str = ArrayUtil.toHex(data);
            LOG("handleChangeApply", str);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }

    }

    public void handleConfirmOverWriting()
    {
        LOG(TAG, "handleConfirmOverWriting");
        updateInfo(mContext.getString(R.string.ota_version_integrity_verification));
        boolean sendRet;
        try {
            sendRet = sendData(new byte[]{(byte) 0x92, 0x42, 0x45, 0x53, 0x54});

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }

    }


    @Override
    public void onReceive(byte[] data)
    {
        Log.e("onReceive",ArrayUtil.toHex(data));
        LOG(TAG, "onReceive data_jtt = " + ArrayUtil.toHex(data));
        boolean isOld  = (boolean)SPHelper.getPreference(mContext,Constants.KEY_FIRMWARE_TYPE,true);
        boolean ack_enable = (boolean)SPHelper.getPreference(mContext,Constants.KEY_ACK_ENABLE,false);
        LOG(TAG, "onReceive isOld = " + isOld);
        Log.d("onReceive","data.length:" + data.length);

        synchronized (mOtaLock) {
            if(isgetDeviceInfo == true)
            {
                isgetDeviceInfo=false;
                is_device_info_get=true;
                master_battery_level=data[0];
                slave_battery_level=data[1];
                master_volume_level=data[2];
                slave_volume_level=data[3];
                master_preset=data[4];
                slave_preset=data[5];
                denoise_mode=data[6];
                streaning_mode=data[7];
                is_music_playing=data[8];
                Log.d("onReceive","data.battery:" + data[0]);
            }
            if ((data[0] & 0xFF) == 0x8b)
            {
                if((ack_enable==true)||(isOld==false))
                {
                    removeTimeout();
                    sendCmdDelayed(CMD_OTA_NEXT, 0);
                }
            }
            else if (ArrayUtil.isEqual(OTA_PASS_RESPONSE, data)) {
                if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 1) {
                    dual_in_one_response_ok_time = 0;
                    removeTimeout();
                    mOtaPacketItemCount = 0;
                    mOtaPacketCount++;
                    updateProgress(mOtaPacketCount * 100 / mOtaData.length);
                    if(isBle()||((isOld==true)&&(ack_enable==false)))
                    {
                        sendCmdDelayed(CMD_OTA_NEXT, 0);
                    }
                }
                else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 0) {
                    removeTimeout();
                    dual_in_one_response_ok_time = 1;
                    return;
                }
                else {
                    removeTimeout();
                    mOtaPacketItemCount = 0;
                    mOtaPacketCount++;
                    updateProgress(mOtaPacketCount * 100 / mOtaData.length);
                    if(isBle()||((isOld==true)&&(ack_enable==false)))
                    {
                        sendCmdDelayed(CMD_OTA_NEXT, 0);
                    }

                }
            }
            else if (ArrayUtil.isEqual(OTA_RESEND_RESPONSE, data)) {
                if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 0) {
                    removeTimeout();
                    dual_in_one_response_ok_time = 1;
                    return;
                }
                else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 1) {
                    dual_in_one_response_ok_time = 0;
                    removeTimeout();
                    mOtaPacketItemCount = 0;
                    if(isBle()||((isOld==true)&&(ack_enable==false)))
                    {
                        sendCmdDelayed(CMD_OTA_NEXT, 0);
                    }
                }
                else {
                    removeTimeout();
                    mOtaPacketItemCount = 0;
                    if (isBle() || ((isOld == true) && (ack_enable == false))) {
                        sendCmdDelayed(CMD_OTA_NEXT, 0);
                    }
                }

            }
            else if (ArrayUtil.startsWith(data, new byte[]{(byte) 0x81, 0x42, 0x45, 0x53, 0x54})) {
                if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 1) {
                    Log.e("0x81", "dual_in_one_response_ok_time == 0");
                    dual_in_one_response_ok_time = 0;
                    return;
                }
                else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 0) {
                    Log.e("0x81", "dual_in_one_response_ok_time==1");
                    dual_in_one_response_ok_time = 1;
                    mMsgHandler.removeMessages(MSG_SEND_INFO_TIME_OUT);
                    int softwareVersion = ((data[5] & 0xFF) | ((data[6] & 0xFF) << 8));
                    int hardwareVersion = ((data[7] & 0xFF) | ((data[8] & 0xFF) << 8));
                    Logger.e(TAG, "softwareVersion " + Integer.toHexString(softwareVersion) + "; hardwareVersion " + Integer.toHexString(hardwareVersion));
                    mMtu = (data[9] & 0xFF) | ((data[10] & 0xFF) << 8);
                    SPHelper.putPreference(mContext, Constants.KEY_OTA_MTU_RESUME, mMtu);
                    int temp = (int)SPHelper.getPreference(mContext, Constants.KEY_OTA_MTU_RESUME,0);
                    LOG(TAG,"0x81 mtu 1 "+ temp);
                    sendCmdDelayed(CMD_LOAD_OTA_CONFIG, 0);
                }
                else {
                    Log.e("0x81", "ok");
                    mMsgHandler.removeMessages(MSG_SEND_INFO_TIME_OUT);
                    int softwareVersion = ((data[5] & 0xFF) | ((data[6] & 0xFF) << 8));
                    int hardwareVersion = ((data[7] & 0xFF) | ((data[8] & 0xFF) << 8));
                    Logger.e(TAG, "softwareVersion " + Integer.toHexString(softwareVersion) + "; hardwareVersion " + Integer.toHexString(hardwareVersion));
                    mMtu = (data[9] & 0xFF) | ((data[10] & 0xFF) << 8);
                    SPHelper.putPreference(mContext, Constants.KEY_OTA_MTU_RESUME, mMtu);
                    int temp = (int)SPHelper.getPreference(mContext, Constants.KEY_OTA_MTU_RESUME,0);
                    LOG(TAG,"0x81 mtu 2 "+temp);
                    sendCmdDelayed(CMD_LOAD_OTA_CONFIG, 0);

                }

            }
            else if ((data[0] & 0xFF) == 0x83) {
                if (data.length == 4 && (data[2] & 0xff) == 0x84) {
                    if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 0) {
                        dual_in_one_response_ok_time = 1;
                        return;
                    }
                    else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 1) {
                        dual_in_one_response_ok_time = 0;
                        removeTimeout();
                        if ((data[3] & 0xFF) == 0x01) {
                            sendCmdDelayed(CMD_OVERWRITING_CONFIRM, 0);
                        }
                        else if ((data[3] & 0xFF) == 0X00) {
                            onOtaFailed();
                            sendCmdDelayed(CMD_DISCONNECT, 0);
                        }
                        mOtaPacketItemCount = 0;
                    }
                    else if ((daulApply == APPLY_LEFT_EARBUD_ONLY || daulApply == APPLY_RIGHT_EARBUD_ONLY)) {
                        sendCmdDelayed(CMD_OVERWRITING_CONFIRM, 0);
                        mOtaPacketItemCount = 0;
                    }
                    else if (daulApply == APPLY_STEREO_OLD_VERSION) {
                        onOtaOver();
                        sendCmdDelayed(CMD_DISCONNECT, 0);
                        mOtaPacketItemCount = 0;
                    }
                    else {
                        removeTimeout();
                        if ((data[3] & 0xFF) == 0x01) {
                            sendCmdDelayed(CMD_OVERWRITING_CONFIRM, 0);
                        }
                        else if ((data[3] & 0xFF) == 0X00) {
                            onOtaFailed();
                            sendCmdDelayed(CMD_DISCONNECT, 0);
                        }
                        mOtaPacketItemCount = 0;
                    }
                }
                else {
                    if (((data[1] & 0xFF) == 0x01)&&(data.length==2))
                    {
                        if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 1) {
                            removeTimeout();
                            mOtaPacketCount++;
                            updateProgress(mOtaPacketCount * 100 / mOtaData.length);
                            dual_in_one_response_ok_time = 0;
                        }
                        else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 0) {
                            removeTimeout();
                            dual_in_one_response_ok_time = 1;
                            return;
                        }

                        else {
                            removeTimeout();
                            mOtaPacketCount++;
                            updateProgress(mOtaPacketCount * 100 / mOtaData.length);
                        }
                    }
                    else if(ArrayUtil.startsWith(data, new byte[]{(byte) 0x83, 0x01, (byte)0x83, 0x01})&&(data.length==4))
                    {
                        if (daulApply == APPLY_BOTH_EARBUD_IN_ONE) {
                            removeTimeout();
                            mOtaPacketCount++;
                            updateProgress(mOtaPacketCount * 100 / mOtaData.length);
                            dual_in_one_response_ok_time = 0;
                        }
                        else
                        {
                            removeTimeout();
                            mOtaPacketCount++;
                            updateProgress(mOtaPacketCount * 100 / mOtaData.length);
                        }
                    }
                    else if ((data[1] & 0xFF) == 0X00) {
                        removeTimeout();
                        if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 1) {
                            mOtaPacketCount = mOtaPacketCount; //虽然多余，保留协议可读性
                            updateProgress(mOtaPacketCount * 100 / mOtaData.length);
                            dual_in_one_response_ok_time = 0;
                        }
                        else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 0) {
                            dual_in_one_response_ok_time = 1;
                            return;
                        }
                        else {
                            mOtaPacketCount = mOtaPacketCount; //虽然多余，保留协议可读性
                            updateProgress(mOtaPacketCount * 100 / mOtaData.length);
                        }
                    }
                    mOtaPacketItemCount = 0;
                    Log.e("test", "befor time " + System.currentTimeMillis());
                    sendCmdDelayed(CMD_OTA_NEXT, 0);
                }
            }
            else if ((data[0] & 0xFF) == 0x84) {
                LOG(TAG,"(data[0] & 0xFF) == 0x84 "+ArrayUtil.toHex(data));
                removeTimeout();
                if ((data[1] & 0xFF) == 0x01) {
                    if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 0) {
                        dual_in_one_response_ok_time = 1;
                        LOG(TAG, "(daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 0)");
                        return;
                    }
                    else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 1) {
                        LOG(TAG, " (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 1) ");
                        dual_in_one_response_ok_time = 0;
                        sendCmdDelayed(CMD_OVERWRITING_CONFIRM, 0);
                    }
                    else if (daul_step == 0 && daulApply == APPLY_BOTH_EARBUD_IN_TWO) {
                        LOG(TAG, "(daul_step == 0 && daulApply == APPLY_BOTH_EARBUD_IN_TWO) ");
                        daul_step = 1;
                        onOtaOverDaulOneStep();
                        sendCmdDelayed(CMD_APPLY_THE_IMAGE_MSG, 0);
                        ota_response_ok = 1;
                        return;
                    }
                    else if (daul_step == 1 && daulApply == APPLY_BOTH_EARBUD_IN_TWO) {
                        LOG(TAG, " (daul_step == 1 && daulApply == APPLY_BOTH_EARBUD_IN_TWO)  ");
                        ota_response_ok = 2;
                        daul_step = 2;
                        sendCmdDelayed(CMD_APPLY_CHANGE, 0);
                    }
                    else if ((daulApply == APPLY_LEFT_EARBUD_ONLY || daulApply == APPLY_RIGHT_EARBUD_ONLY)) {
                        onOtaOverSingleStep();
                        sendCmdDelayed(CMD_OVERWRITING_CONFIRM, 0);
                    }
                    else if(daulApply==APPLY_STEREO)
                    {
                        sendCmdDelayed(CMD_OVERWRITING_CONFIRM, 0);
                    }
                    else if (daulApply == APPLY_STEREO_OLD_VERSION) {
                        onOtaOver();
                        sendCmdDelayed(CMD_DISCONNECT, 0);
                        sendCmdDelayed(MSG_HANDLE_OTA_INFO_FILE_REPORT,0);
                    }
                    else {
                        onOtaOver();
                        sendCmdDelayed(CMD_DISCONNECT, 0);
                        sendCmdDelayed(MSG_HANDLE_OTA_INFO_FILE_REPORT,0);

                    }
                }
                else if ((data[1] & 0xFF) == 0x00) {
                    if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 0) {
                        dual_in_one_response_ok_time = 1;
                        return;
                    }
                    else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 1) {
                        onOtaFailed();
                        sendCmdDelayed(CMD_DISCONNECT, 0);
                        dual_in_one_response_ok_time = 0;
                        sendCmdDelayed(MSG_HANDLE_OTA_INFO_FILE_REPORT,0);
                    }
                    else {
                        onOtaFailed();
                        sendCmdDelayed(CMD_DISCONNECT, 0);
                        sendCmdDelayed(MSG_HANDLE_OTA_INFO_FILE_REPORT,0);
                    }
                }
                else if ((data[1] & 0xFF) == 0x02) {
                    Log.d(TAG, "onReceive 84, 02: dual_in_one_response_ok_time = "+dual_apply_change_response);
                    if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 0) {
                        dual_in_one_response_ok_time = 1;
                        return;
                    }
                    else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 1) {
                        dual_in_one_response_ok_time = 0;
                        updateInfo(mContext.getString(R.string.received_size_error));
                        sendCmdDelayed(CMD_DISCONNECT, 0);

                    }
                    else {
                        updateInfo(mContext.getString(R.string.received_size_error));
                        sendCmdDelayed(CMD_DISCONNECT, 0);

                    }
                }
                else if ((data[1] & 0xFF) == 0x03) {
                    if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 0) {
                        dual_in_one_response_ok_time = 1;
                        return;
                    }
                    else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 1) {
                        dual_in_one_response_ok_time = 0;
                        updateInfo(mContext.getString(R.string.write_flash_offset_error));
                        sendCmdDelayed(CMD_DISCONNECT, 0);
                    }
                    else {
                        updateInfo(mContext.getString(R.string.write_flash_offset_error));
                        sendCmdDelayed(CMD_DISCONNECT, 0);
                    }
                }
                else if ((data[1] & 0xFF) == 0x04) {
                    if (daulApply == 2 && dual_in_one_response_ok_time == 0) {
                        dual_in_one_response_ok_time = 1;
                        return;
                    }
                    else if (daulApply == 2 && dual_in_one_response_ok_time == 1) {
                        dual_in_one_response_ok_time = 0;
                        segment_verify_error_time++;
                        if (segment_verify_error_time < 3) {
                            updateInfo(segment_verify_error_time + "times resend");
                            mOtaPacketCount = mOtaPacketCount - 1;//回退一位
                            sendCmdDelayed(CMD_LOAD_FILE_FOR_NEW_PROFILE, 0);
                        }
                        else {
                            updateInfo("Resend fail");
                            segment_verify_error_time = 0;
                            byte[] clearbyte = new byte[32];
                            for (int i = 0; i < clearbyte.length; i++) {
                                clearbyte[i] = (byte) 0x00;
                            }
                            SPHelper.putPreference(mContext, Constants.KEY_OTA_RESUME_VERTIFY_RANDOM_CODE, ArrayUtil.toHex(clearbyte));
                            updateInfo(mContext.getString(R.string.segment_verify_error));
                            sendCmdDelayed(CMD_DISCONNECT, 0);
                        }
                    }
                    else {
                        segment_verify_error_time++;
                        if (segment_verify_error_time < 3) {
                            updateInfo(segment_verify_error_time + "次重传");
                            mOtaPacketCount = mOtaPacketCount - 1;//回退一位
                            sendCmdDelayed(CMD_LOAD_FILE_FOR_NEW_PROFILE, 0);
                        }
                        else {
                            updateInfo("重传失败");
                            segment_verify_error_time = 0;
                            byte[] clearbyte = new byte[32];
                            for (int i = 0; i < clearbyte.length; i++) {
                                clearbyte[i] = (byte) 0x00;
                            }
                            SPHelper.putPreference(mContext, Constants.KEY_OTA_RESUME_VERTIFY_RANDOM_CODE, ArrayUtil.toHex(clearbyte));
                            updateInfo(mContext.getString(R.string.segment_verify_error));
                            sendCmdDelayed(CMD_DISCONNECT, 0);
                        }
                    }

                }
                else if ((data[1] & 0xFF) == 0x05) {
                    if (daulApply == 2 && dual_in_one_response_ok_time == 1) {
                        dual_in_one_response_ok_time = 0;
                        updateInfo(mContext.getString(R.string.breakpoint_error));
                        sendCmdDelayed(CMD_DISCONNECT, 0);
                    }
                    else if (daulApply == 2 && dual_in_one_response_ok_time == 0) {
                        dual_in_one_response_ok_time = 1;
                        return;
                    }
                    else {
                        updateInfo(mContext.getString(R.string.breakpoint_error));
                        sendCmdDelayed(CMD_DISCONNECT, 0);
                    }

                }
                mOtaPacketItemCount = 0;
            }

            else if ((data[0] & 0xFF) == 0x8D) {
                Log.e("fanxiaoli fanxiaoli 8d", ArrayUtil.toHex(data) + "");
                Log.e("onReceive", "CMD_RESUME_OTA_CHECK_MSG_RESPONSE");
                removeTimeout();
                mMsgHandler.removeMessages(MSG_RESUME_OTA_TIME_OUT);
                byte[] breakpoint = new byte[4];
                breakpoint = ArrayUtil.extractBytes(data, 1, 4);
                Log.e("extractBytes", ArrayUtil.toHex(breakpoint) + "");
                if (ArrayUtil.isEqual(breakpoint, new byte[]{(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF})) {
                    if((daulApply == APPLY_BOTH_EARBUD_IN_ONE) && (two_bins_in_one_step == 0))
                    {
                        two_bins_in_one_step = 1;
                        Log.e("resume", "from 0 fanxiaoli");
                        resumeFlg = false;
                        resumeSegment = 0;
                        LogUtils.writeForOTAStatic(TAG, "onConnected ");
                        mState = STATE_CONNECTED;
                        reconnectTimes = 0;
                        byte[] randomCode = new byte[32];
                        randomCode = ArrayUtil.extractBytes(data, 5, 32);
                        String randomCodeStr = ArrayUtil.toHex(randomCode);
                        LOG(TAG, "random_code_str  fanxiaoli= " + randomCodeStr);
                        SPHelper.putPreference(mContext, Constants.KEY_RESUME_VERTIFY_RANDOM_CODE_LEFT, randomCodeStr);
                        sendCmdDelayed(CMD_APPLY_THE_IMAGE_MSG,0);
                    }
                    else if((daulApply == APPLY_BOTH_EARBUD_IN_ONE) && (two_bins_in_one_step == 1))
                    {
                        two_bins_in_one_step = 2;
                        Log.e("resume", "from 0 fanxiaoli");
                        resumeFlg = false;
                        resumeSegment = 0;
                        LogUtils.writeForOTAStatic(TAG, "onConnected ");
                        mState = STATE_CONNECTED;
                        reconnectTimes = 0;
                        byte[] randomCode = new byte[32];
                        randomCode = ArrayUtil.extractBytes(data, 5, 32);
                        String randomCodeStr = ArrayUtil.toHex(randomCode);
                        LOG(TAG, "random_code_str  fanxiaoli= " + randomCodeStr);
                        SPHelper.putPreference(mContext, Constants.KEY_RESUME_VERTIFY_RANDOM_CODE_RIGHT, randomCodeStr);
                        sendCmdDelayed(CMD_APPLY_THE_IMAGE_MSG,0);
                    }
                    else
                    {
                        resumeSegment = 0;
                        resumeFlg = false;
                        sendCmdDelayed(CMD_SEND_FILE_INFO, 0);
                        LogUtils.writeForOTAStatic(TAG, "onConnected ");
                        mState = STATE_CONNECTED;
                        reconnectTimes = 0;
                        byte[] randomCode = new byte[32];
                        randomCode = ArrayUtil.extractBytes(data, 5, 32);
                        String randomCodeStr = ArrayUtil.toHex(randomCode);
                        LOG(TAG, "random_code_str  fanxiaoli= " + randomCodeStr);
                        SPHelper.putPreference(mContext, Constants.KEY_OTA_RESUME_VERTIFY_RANDOM_CODE, randomCodeStr);
                    }
                }
                else if (ArrayUtil.isEqual(breakpoint, new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00})) {
                    Log.e("daulApply ffffff", daulApply + "");
                    Log.e("dual_in_ok_time", dual_in_one_response_ok_time + "");
                    if((daulApply == APPLY_BOTH_EARBUD_IN_ONE) && (two_bins_in_one_step == 0))
                    {
                        two_bins_in_one_step = 1;
                        Log.e("resume", "from 0 fanxiaoli");
                        resumeFlg = false;
                        resumeSegment = 0;
                        LogUtils.writeForOTAStatic(TAG, "onConnected ");
                        mState = STATE_CONNECTED;
                        reconnectTimes = 0;
                        byte[] randomCode = new byte[32];
                        randomCode = ArrayUtil.extractBytes(data, 5, 32);
                        String randomCodeStr = ArrayUtil.toHex(randomCode);
                        LOG(TAG, "random_code_str  fanxiaoli= " + randomCodeStr);
                        SPHelper.putPreference(mContext, Constants.KEY_RESUME_VERTIFY_RANDOM_CODE_LEFT, randomCodeStr);
                        sendCmdDelayed(CMD_APPLY_THE_IMAGE_MSG,0);
                    }
                    else if((daulApply == APPLY_BOTH_EARBUD_IN_ONE) && (two_bins_in_one_step == 1))
                    {
                        two_bins_in_one_step = 2;
                        Log.e("resume", "from 0 fanxiaoli");
                        resumeFlg = false;
                        resumeSegment = 0;
                        LogUtils.writeForOTAStatic(TAG, "onConnected ");
                        mState = STATE_CONNECTED;
                        reconnectTimes = 0;
                        byte[] randomCode = new byte[32];
                        randomCode = ArrayUtil.extractBytes(data, 5, 32);
                        String randomCodeStr = ArrayUtil.toHex(randomCode);
                        LOG(TAG, "random_code_str  fanxiaoli= " + randomCodeStr);
                        SPHelper.putPreference(mContext, Constants.KEY_RESUME_VERTIFY_RANDOM_CODE_RIGHT, randomCodeStr);
                        sendCmdDelayed(CMD_APPLY_THE_IMAGE_MSG,0);
                    }
                    else {
                        if(resume_enable) {
                            daulApply = resumeUpgradeWay;
                        }
                        Log.e("resume", "from 0 fanxiaoli");
                        resumeFlg = false;
                        resumeSegment = 0;
                        sendCmdDelayed(CMD_SEND_FILE_INFO, 0);
                        LogUtils.writeForOTAStatic(TAG, "onConnected ");
                        mState = STATE_CONNECTED;
                        reconnectTimes = 0;
                        byte[] randomCode = new byte[32];
                        randomCode = ArrayUtil.extractBytes(data, 5, 32);
                        String randomCodeStr = ArrayUtil.toHex(randomCode);
                        LOG(TAG, "random_code_str  fanxiaoli= " + randomCodeStr);
                        SPHelper.putPreference(mContext, Constants.KEY_OTA_RESUME_VERTIFY_RANDOM_CODE, randomCodeStr);
                    }
                }
                else {
                    if ((daulApply == APPLY_BOTH_EARBUD_IN_ONE) && (two_bins_in_one_step == 1)) {
                        segment_right = ArrayUtil.bytesToIntLittle(breakpoint);
                        two_bins_in_one_step = 2;
                        resumeSegment = 0;
                        mState = STATE_CONNECTED;
                        reconnectTimes = 0;
                        sendCmdDelayed(CMD_APPLY_THE_IMAGE_MSG,0);
                    }
                    else if ((daulApply == APPLY_BOTH_EARBUD_IN_ONE) && (two_bins_in_one_step == 0)) {
                        segment_left = ArrayUtil.bytesToIntLittle(breakpoint);
                        two_bins_in_one_step = 1;
                        Log.e("resume", "from 0 fanxiaoli");
                        resumeFlg = false;
                        resumeSegment = 0;
                        mState = STATE_CONNECTED;
                        reconnectTimes = 0;
                        sendCmdDelayed(CMD_APPLY_THE_IMAGE_MSG,0);
                    }
                    else if((resumeUpgradeWay == APPLY_RIGHT_EARBUD_ONLY)||(resumeUpgradeWay == APPLY_LEFT_EARBUD_ONLY)||(resumeUpgradeWay == APPLY_BOTH_EARBUD_IN_TWO))
                    {
                        daulApply = resumeUpgradeWay;
                        int segment = ArrayUtil.bytesToIntLittle(breakpoint);
                        Log.e("segment", segment + "");
                        if (segment != 0) {
                            resumeFlg = true;
                            mOtaPacketCount = segment / packnum;
                            updateInfo(mContext.getString(R.string.resume_start));
                            mMtu = (int) SPHelper.getPreference(mContext, Constants.KEY_OTA_MTU_RESUME, 0);
                            LOG(TAG, "breakpoint check success 2 mtu" + mMtu);
                            sendCmdDelayed(CMD_LOAD_OTA_CONFIG, 0);
                            Log.e("mOtaData is null", "resume mOtaPacketCount" + mOtaPacketCount);
                            Log.e("resume", "resume mOtaPacketCount" + mOtaPacketCount);
                        }
                    }
                    else {
                        int segment = ArrayUtil.bytesToIntLittle(breakpoint);
                        Log.e("segment", segment + "");
                        if (segment != 0) {
                            resumeFlg = true;
                            mOtaPacketCount = segment / packnum;
                            updateInfo(mContext.getString(R.string.resume_start));
                            mMtu =(int)SPHelper.getPreference(mContext, Constants.KEY_OTA_MTU_RESUME, 0);
                            LOG(TAG,"breakpoint check success 2 mtu"+mMtu);
                            sendCmdDelayed(CMD_LOAD_OTA_CONFIG, 0);
                            Log.e("mOtaData is null", "resume mOtaPacketCount" + mOtaPacketCount);
                            resumeFlg = false;
                            Log.e("resume", "resume mOtaPacketCount" + mOtaPacketCount);
                        }
                    }


                }

            }
            else if ((data[0] & 0xFF) == 0x87) {
                removeTimeout();
                if ((data[1] & 0xFF) == 0x01) {
                    if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 0) {

                        Log.e("0x87 01", "dual_in_one_response_ok_time == 0");
                        dual_in_one_response_ok_time = 1;
                        return;
                    }
                    else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 1) {
                        Log.e("0x87 01", "dual_in_one_response_ok_time == 1");
                        dual_in_one_response_ok_time = 0;
                        sendCmdDelayed(CMD_LOAD_FILE_FOR_NEW_PROFILE, 0);
                    }
                    else {
                        sendCmdDelayed(CMD_LOAD_FILE_FOR_NEW_PROFILE, 0);
                    }
                }
                else {
                    if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 0) {
                        dual_in_one_response_ok_time = 1;
                        return;
                    }
                    else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 1) {
                        dual_in_one_response_ok_time = 0;
                        onOtaConfigFailed();
                        sendCmdDelayed(CMD_DISCONNECT, 0);
                        return;
                    }
                    else {
                        onOtaConfigFailed();
                        sendCmdDelayed(CMD_DISCONNECT, 0);
                    }
                }
            }
            else if (ArrayUtil.startsWith(data, new byte[]{(byte) 0x8F, 0x42, 0x45, 0x53, 0x54})) { //Get current version response packet:
                mMsgHandler.removeMessages(MSG_GET_FIREWARE_VERSION_TIME_OUT);
                String current_version = "";
                hardWareType = HARDWARE_0x90;
                if ((data[5] & 0xFF) == 0x00) {
                    Log.e("received 0x8f", "stereo device");
                    byte[] version = new byte[4];
                    version = ArrayUtil.extractBytes(data, 6, 4);
                    String version_str = ArrayUtil.bytesToVersion(version);
                    mDaulConnectState = DAUL_CONNECT_STEREO;
                    if (version_str == null) {
                        updateVersion(mContext.getString(R.string.version_number_error));
                        return;
                    }
                    else {
                        Log.e("version_str", version_str);
                        current_version = "stereo device version ：" + version_str;
                        updateVersion(current_version);
                        daulApply = APPLY_STEREO;
                        SPHelper.putPreference(mContext, Constants.KEY_OTA_UPGRADE_WAY, APPLY_STEREO);
                        imageApply = IMAGE_STEREO;
                        stereo_flg = STEREO_NEW;
                    }


                }
                else if ((data[5] & 0xFF) == 0x01) {
                    current_version = "current connected device is left earbud\n";
                    LOG("received 0x8f", " FWS device, current connected device is left earbud");
                    is_left_master=true;
                    byte[] version = new byte[4];
                    version = ArrayUtil.extractBytes(data, 6, 4);
                    String version_str = ArrayUtil.bytesToVersion(version);
                    current_version = current_version + "left earbud version :" + version_str + "\n";
                    version_str = ArrayUtil.bytesToVersion(ArrayUtil.extractBytes(data, 10, 4));
                    current_version = current_version + "right earbud version:" + version_str;
                    mDaulConnectState = DAUL_CONNECT_LEFT;
                    updateVersion(current_version);
                    daulApply = APPLY_STEREO_UNDEFINED;
                    stereo_flg = TWS;
                    //MainActivity.mshow.dismiss();
//                    timer_handler.sendEmptyMessage(0);

                }
                else if ((data[5] & 0xFF) == 0x02) {
                    current_version = "current connected device is right earbud\n";
                    LOG("received 0x8f", "FWS device, current connected device is right earbud");
                    is_left_master=false;
                    byte[] version = new byte[4];
                    version = ArrayUtil.extractBytes(data, 6, 4);
                    String version_str = ArrayUtil.bytesToVersion(version);
                    current_version = current_version + "left earbud version :" + version_str + "\n";
                    version_str = ArrayUtil.bytesToVersion(ArrayUtil.extractBytes(data, 10, 4));
                    current_version = current_version + "right earbud version:" + version_str;
                    mDaulConnectState = DAUL_CONNECT_RIGHT;
                    updateVersion(current_version);
                    daulApply = APPLY_STEREO_UNDEFINED;
                    stereo_flg = TWS;
                    MainActivity.mshow.dismiss();
                    timer_handler.sendEmptyMessage(0);
                }

            }
            else if ((data[0] & 0xFF) == 0x89) {
                if (data.length == 2 && (data[1] & 0xFF) == 0x01) {
                    LOG("0x89,0x01", "2");
                    if (function == dump_log) {
                        updateFlashContentDetails(dump_log);
                        sendCmdDelayed(CMD_READY_FLASH_CONTENT, 0);
                        flash_content_end = false;
                    }

                }
                else if ((data.length == 4) && ((data[1] & 0xFF) == 0x01) && ((data[2] & 0xFF) == 0x89) && ((data[3] & 0xFF) == 0x01)) {
                    Log.e("0x89,0x01,0x89,0x01", "4");
                    flash_content_end = true;
                }
            }
            else if ((data[0] & 0xFF) == 0x8A) {
                Log.e("fanxiaoli 0x8A", ArrayUtil.toHex(data));
                if (function == dump_log) {
                    updateFlashContent(data, ArrayUtil.toHex(data));
                }
                else if (function == get_version_address) {
                    getVersionAddress(data);
                }
                else if (function == read_version) {
                    readFwVersion(data);
                }
            }

            else if ((data[0] & 0xFF) == 0x91) {//New image apply result response:
                removeTimeout();
                if ((data[1] & 0xFF) == 0x01) {
                    if ((daulApply == APPLY_BOTH_EARBUD_IN_ONE)&&(two_bins_in_one_step == 0)) {
                        sendCmdDelayed(CMD_RESUME_OTA_CHECK_MSG,0);
                    }
                    else if ((daulApply == APPLY_BOTH_EARBUD_IN_ONE)&&(two_bins_in_one_step == 1)) {
                        sendCmdDelayed(CMD_RESUME_OTA_CHECK_MSG,0);
                    }
                    else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && two_bins_in_one_step == 2&&dual_in_one_response_ok_time == 1) {
                        dual_in_one_response_ok_time = 0;
                        two_bins_in_one_step = 0;
                        if((segment_left == segment_right)&&(segment_left != 0))
                        {
                            resumeFlg = true;
                            mOtaPacketCount = segment_left / packnum;
                            updateInfo(mContext.getString(R.string.resume_start));
                            mMtu = (int) SPHelper.getPreference(mContext, Constants.KEY_OTA_MTU_RESUME, 0);
                            LOG(TAG, "breakpoint check success 2 mtu" + mMtu);
                            sendCmdDelayed(CMD_LOAD_OTA_CONFIG, 0);
                            Log.e("mOtaData is null", "resume mOtaPacketCount" + mOtaPacketCount);
                            Log.e("resume", "resume mOtaPacketCount" + mOtaPacketCount);
                        }
                        else
                        {
                            Log.e("resume", "from 0 fanxiaoli");
                            resumeFlg = false;
                            resumeSegment = 0;
                            sendCmdDelayed(CMD_SEND_FILE_INFO, 0);
                            mState = STATE_CONNECTED;
                            reconnectTimes = 0;
                        }

                    }
                    else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && two_bins_in_one_step == 2&&dual_in_one_response_ok_time == 0)
                    {
                        dual_in_one_response_ok_time = 1;
                        return;
                    }
                    else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 0) {
                        Log.e("received 0x91", "isAppliedSuccessfully, 1 is pass,");
                        dual_in_one_response_ok_time = 1;
                        return;
                    }

                    else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 1) {
                        Log.e("received 0x91", "isAppliedSuccessfully, 2 is pass,");
                        dual_in_one_response_ok_time = 0;
                        sendCmdDelayed(CMD_RESUME_OTA_CHECK_MSG, 0);
                    }
                    else if ((daulApply == APPLY_BOTH_EARBUD_IN_TWO) && (ota_response_ok == 2) && (dual_apply_change_response == 1)) {
                        LOG(TAG,"(daulApply == APPLY_BOTH_EARBUD_IN_TWO) && (ota_response_ok == 2) && (dual_apply_change_response == 1)");
                        dual_apply_change_response = 0;
                        ota_response_ok = 0;
                        sendCmdDelayed(CMD_OVERWRITING_CONFIRM, 0);
                    }
                    else if ((daulApply == APPLY_BOTH_EARBUD_IN_TWO) && (ota_response_ok == 2) && (dual_apply_change_response == 0)) {
                        LOG(TAG,"(daulApply == APPLY_BOTH_EARBUD_IN_TWO) && (ota_response_ok == 2) && (dual_apply_change_response == 0)");
                        dual_apply_change_response = 1;
                        return;
                    }
                    else {
                        sendCmdDelayed(CMD_RESUME_OTA_CHECK_MSG, 0);
                    }
                }
                else if ((data[1] & 0xFF) == 0x00) {
                    Log.e("received 0x91", " 0 is fail");
                    if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 0) {
                        dual_in_one_response_ok_time = 1;
                        return;
                    }
                    else if (daulApply == APPLY_BOTH_EARBUD_IN_ONE && dual_in_one_response_ok_time == 1) {
                        dual_in_one_response_ok_time = 0;
                    }
                    else if ((daulApply == APPLY_BOTH_EARBUD_IN_TWO) && (ota_response_ok == 2) && (dual_apply_change_response == 1)) {
                        dual_apply_change_response = 0;
                        ota_response_ok = 0;
                    }
                    else if ((daulApply == APPLY_BOTH_EARBUD_IN_TWO) && (ota_response_ok == 2) && (dual_apply_change_response == 0)) {
                        dual_apply_change_response = 1;
                        return;
                    }
                    else {
                        Log.e("received 0x91", " 0 is fail");
                    }

                }

            }
            else if(ArrayUtil.startsWith(data,new byte[]{(byte) 0x93, 0x01, (byte)0x93, 0x01})&&(data.length==4)&&(daulApply == APPLY_BOTH_EARBUD_IN_TWO ))
            {
                img_overwriting_confirm_response_time = 0;
                onOtaOver();
                sendCmdDelayed(CMD_DISCONNECT, 0);
                sendCmdDelayed(MSG_HANDLE_OTA_INFO_FILE_REPORT,0);
            }
            else if(ArrayUtil.startsWith(data,new byte[]{(byte) 0x93, 0x00, (byte)0x93, 0x00})&&(data.length==4)&&(daulApply == APPLY_BOTH_EARBUD_IN_TWO ))
            {
                img_overwriting_confirm_response_time = 0;
                onOtaConfigFailed();
                sendCmdDelayed(CMD_DISCONNECT, 0);
            }
            else if ((data[0] & 0xFF) == 0x93&&data.length==2) {
                removeTimeout();
                if ((data[1] & 0xFF) == 0x01) {
                    updateInfo(mContext.getString(R.string.ota_version_verification_ok));
                    if ((daulApply == APPLY_BOTH_EARBUD_IN_TWO || daulApply == APPLY_BOTH_EARBUD_IN_ONE) && img_overwriting_confirm_response_time == 1) {
                        img_overwriting_confirm_response_time = 0;
                        onOtaOver();
                        sendCmdDelayed(CMD_DISCONNECT, 0);
                        sendCmdDelayed(MSG_HANDLE_OTA_INFO_FILE_REPORT,0);
                    }
                    else if (daulApply == APPLY_BOTH_EARBUD_IN_TWO && img_overwriting_confirm_response_time == 0) {
                        img_overwriting_confirm_response_time = 1;
                        return;

                    }
                    else if(daulApply == APPLY_STEREO)
                    {
                        onOtaOver();
                        sendCmdDelayed(CMD_DISCONNECT, 0);
                        sendCmdDelayed(MSG_HANDLE_OTA_INFO_FILE_REPORT,0);
                    }
                    else {
                        onOtaOver();
                        sendCmdDelayed(CMD_DISCONNECT, 0);
                        sendCmdDelayed(MSG_HANDLE_OTA_INFO_FILE_REPORT,0);
                    }
                }
                else if ((data[1] & 0xFF) == 0x00) {
                    Log.e("received 0x93", " 0 is fail");
                    if ((daulApply == APPLY_BOTH_EARBUD_IN_ONE || daulApply == APPLY_BOTH_EARBUD_IN_TWO) && (img_overwriting_confirm_response_time == 0)) {
                        img_overwriting_confirm_response_time = 1;
                        return;
                    }
                    else if ((daulApply == APPLY_BOTH_EARBUD_IN_ONE || daulApply == APPLY_BOTH_EARBUD_IN_TWO) && (img_overwriting_confirm_response_time == 1)) {
                        img_overwriting_confirm_response_time = 0;
                        onOtaConfigFailed();
                        sendCmdDelayed(CMD_DISCONNECT, 0);
                    }
                    else if(daulApply == APPLY_STEREO)
                    {
                        onOtaConfigFailed();
                        sendCmdDelayed(CMD_DISCONNECT, 0);
                        Log.e("received 0x93", " 0 is fail");
                    }
                    else {
                        onOtaConfigFailed();
                        sendCmdDelayed(CMD_DISCONNECT, 0);
                        Log.e("received 0x93", " 0 is fail");
                    }
                }
            }else{
                customCommand(data);//TODO: custom command processing
            }
        }
    }

    protected void getVersionAddress(byte[] data)
    {
        byte[] data_req = new byte[10];
        String str = ArrayUtil.toHex(data);
        Log.e("getFwVersion", str);
        byte[] begin = new byte[3];
        for (int i = 0; i < 3; i++) {
            data_req[2 + i] = data[1 + i];
        }
        byte[] length = new byte[4];
        length = ArrayUtil.hexStringToByte(Version_length);//选取1k
        for (int i = 0; i < length.length; i++) {
            data_req[6 + i] = length[i];
        }
        try {
            data_req[0] = (byte) 0x89;
            data_req[1] = (byte) 0x01;
            Log.e("data_req", ArrayUtil.toHex(data_req));
            sendData(data_req);
            function = read_version;
            version_content_byte_total_num = 0;
            version_content_byte_num = 0;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

    protected void readFwVersion(byte[] data)
    {
        Log.e("readFwVersion", ArrayUtil.toHex(data));
        Log.e("data.length", data.length + "");
        version_content_byte_total_num = (Integer.parseInt(Version_length, 16) * 129) / 128;
        Log.e("readFwVersion total", version_content_byte_total_num + "");
        if (version_content_byte_num < version_content_byte_total_num) {
            for (int i = 0; i < data.length; i++) {
                version_content_all[version_content_byte_num + i] = data[i];
            }
            version_content_byte_num = version_content_byte_num + data.length;
            if (version_content_byte_num == version_content_byte_total_num) {
                int index = 0;
                for (int i = 0; i < version_content_byte_total_num; i++) {
                    if ((i % 129) > 0) {
                        version_content[index] = version_content_all[i];
                        index++;
                    }
                }
                String version_content_str = ArrayUtil.toASCII(version_content);
                readyForGetFlashContent(version_content_str);
            }
        }
    }

    protected void readyForGetFlashContent(String version_content_str)
    {
        int str_index = version_content_str.indexOf("4W");
        version_content_str = version_content_str.substring(0, str_index);
        version_content_str = version_content_str.replaceFirst("REV_INFO", "VERSION_INFO");
        String[] details = new String[12];
        details = version_content_str.split("\\n");
        version_content_str = details[8] + "\n" + details[9] + "\n" + details[11] + "\n";
        updateReadVersionInfo(version_content_str);
        String dump_address = details[8];
        String dump_address_info;
        int len = dump_address.indexOf("0x3C");
        Log.e("len", len + "");
        dump_address_info = dump_address.substring(len + 4, dump_address.length());
        Log.e("dump_address_info", dump_address_info);
        byte[] flash_content_req = new byte[10];
        byte[] dump_address_req = new byte[4];
        dump_address_req = ArrayUtil.hexStringToByte(dump_address_info);
        Log.e("dump_address_req", ArrayUtil.toHex(dump_address_req));
        for (int i = 0; i < dump_address_req.length; i++) {
            flash_content_req[2 + i] = dump_address_req[i];
        }
        flash_content_req[5] = (byte) 0x00;
        String dump_size = details[9];
        len = dump_size.indexOf("0x");
        String dump_size_info = dump_size.substring(len + 2, dump_size.length());
        Log.e("dump_size_info", dump_size_info);
        if ((dump_size_info.length() % 2) > 0) {
            dump_size_info = "0" + dump_size_info;
        }
        byte[] dump_size_req = new byte[4];
        dump_size_req = ArrayUtil.hexStringToByte(dump_size_info);
        flash_content_byte_total_num = (Integer.parseInt(dump_size_info, 16) * 129) / 128;
        flash_content_byte_from_fireware = new byte[flash_content_byte_total_num];
        flash_content_byte = new byte[Integer.parseInt(dump_size_info, 16)];
        LOG(TAG, "flash_content_byte_total_num:" + flash_content_byte_total_num);
        for (int i = 0; i < dump_size_req.length; i++) {
            flash_content_req[6 + i] = dump_size_req[i];
        }
        try {
            flash_content_req[0] = (byte) 0x89;
            flash_content_req[1] = (byte) 0x01;
            Log.e("flash_content_req", ArrayUtil.toHex(flash_content_req));
            sendData(flash_content_req);
            function = dump_log;
            flash_content_byte_num = 0;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

    protected void updateFlashContentDetails(int i)
    {
        if (i == dump_log) {
            Message message = mMsgHandler.obtainMessage(MSG_UPDATE_FLASH_CONTENT_DETAILS);
            message.obj = "数据写入中";
            mMsgHandler.sendMessage(message);
            Log.e("updateFlashContent", "flash_content_str" + flash_content_str);
        }
    }

    protected void updateReadVersionInfo(String info)
    {
        Message message = mMsgHandler.obtainMessage(MSG_UPDATE_READ_VERSION_INFO);
        message.obj = info;
        mMsgHandler.sendMessage(message);
    }

    protected void updateFlashContent(byte[] bytes, String info)
    {
        if (flash_content_file_path == null) {
            return;
        }
        try {
            if (flash_content_byte_num < flash_content_byte_total_num) {
                for (int i = 0; i < bytes.length; i++) {
                    flash_content_byte_from_fireware[flash_content_byte_num + i] = bytes[i];
                }
                int num = bytes.length;
                flash_content_byte_num = flash_content_byte_num + num;
                LOG(TAG, flash_content_byte_num + "");

                if (flash_content_byte_num == flash_content_byte_total_num) {
                    int index = 0;
                    for (int i = 0; i < flash_content_byte_from_fireware.length; i++) {
                        if ((i % 129) > 0) {
                            flash_content_byte[index] = flash_content_byte_from_fireware[i];
                            index++;
                        }
                    }
                    LOG(TAG, "flash_content_byte = " + ArrayUtil.toHex(flash_content_byte));
                    FileOutputStream stream = new FileOutputStream(flash_content_file_path, true);
                    stream.write(flash_content_byte);
                    stream.flush();
                    stream.close();
                    Message message = mMsgHandler.obtainMessage(MSG_UPDATE_FLASH_CONTENT_ITEM);
                    message.obj = "get flash content info ready";
                    mMsgHandler.sendMessage(message);
                    sendCmdDelayed(CMD_DISCONNECT, 0);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    protected abstract void connect();

    protected abstract void disconnect();

    protected abstract int getMtu(int stereo_flg);

    protected abstract void pickDevice(int request);

    protected abstract String loadLastDeviceName();

    protected abstract void saveLastDeviceName(String name);

    protected abstract String loadLastDeviceAddress();

    protected abstract void saveLastDeviceAddress(String address);

    protected abstract boolean sendData(byte[] data);

    protected abstract boolean isBle();

    protected abstract String getActivityName();

    public class CmdHandler extends Handler
    {
        public CmdHandler(Looper looper)
        {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what) {
                case CMD_CONNECT:
                    connect();
                    break;
                case CMD_DISCONNECT:
                    disconnect();
                    break;
                case CMD_LOAD_FILE:
                    loadFile();
                    break;
                case CMD_OTA_NEXT:
                    LOG(TAG, "handleMessage CMD_OTA_NEXT");
                    otaNext();
                    break;
                case CMD_START_OTA:
                    LOG("CMD_START_OTA", "CMD_START_OTA");
                    startOta();
                    break;
                case CMD_SEND_FILE_INFO:
                    sendFileInfo();
                    break;
                case CMD_LOAD_OTA_CONFIG:
                    loadOtaConfig();
                    break;
                case CMD_START_OTA_CONFIG:
                    startOtaConfig();
                    break;
                case CMD_OTA_CONFIG_NEXT:
                    otaConfigNext();
                    break;
                case CMD_LOAD_FILE_FOR_NEW_PROFILE:
                    loadFileForNewProfile();
                    break;
                case CMD_LOAD_FILE_FOR_NEW_PROFILE_SPP:
                    loadFileForNewProfileSPP();
                    break;
                case CMD_RESEND_MSG:
                    LOG(TAG, "resend the msg");
                    sendCmdDelayed(CMD_OTA_NEXT, 0);
                    break;
                case CMD_RESUME_OTA_CHECK_MSG:
                    Log.e(TAG, "CMD_RESUME_OTA_CHECK_MSG");
                    sendBreakPointCheckReq();
                    break;
                case CMD_SEND_HW_INFO:
                    LOG(TAG, "CMD_SEND_HW_INFO");
                    handleGetCurrentVersion();
                    break;
                case CMD_APPLY_THE_IMAGE_MSG:
                    LOG("CMD_APPLY_THE_IMAGE_MSG", "CMD_APPLY_THE_IMAGE_MSG");
                    handleApplyTheImage();
                    break;
                case CMD_APPLY_CHANGE:
                    LOG("CMD_APPLY_CHANGE", "CMD_APPLY_CHANGE");
                    handleChangeApply();
                    break;
                case CMD_OVERWRITING_CONFIRM:
                    LOG("CMD_OVERWRITING_CONFIRM", "CMD_OVERWRITING_CONFIRM");
                    handleConfirmOverWriting();
                    break;
                case CMD_GET_BUILD_INFO_ADDRESS:
                    GetBuildInfoAddress(version_address_point_address, "04");//依据固件宏控定义 #define __APP_IMAGE_FLASH_OFFSET__ 20000 只要32个字节
                    break;
                case CMD_READY_FLASH_CONTENT:
                    ReadyFlashContent();
                    break;
                case CMD_STOP_FLASH_CONTENT:
                    GetFlashContentStop();
                    break;
                default:
                    break;
            }
        }
    }

    private void ReadyFlashContent()
    {
        flash_content_file_path = FileUtils.writeFlashContentfile();
        flash_content_str = "";
        flash_content_byte_num = 0;
        flash_content_end = false;
    }

    private void GetBuildInfoAddress(String address, String length)
    {
        byte[] data = new byte[10];
        Log.e("length", length);
        byte[] flash_length = new byte[4];
        flash_length = ArrayUtil.hexStringToByte(length);
        LOG(TAG, "GetBuildInfoAddress flash_length =" + ArrayUtil.toHex(flash_length));
        for (int i = 0; i < flash_length.length; i++) {
            data[6 + i] = flash_length[i];
        }
        byte[] addressbyte = new byte[4];
        addressbyte = ArrayUtil.hexStringToByte(address);
        for (int i = 0; i < addressbyte.length; i++) {
            data[2 + i] = addressbyte[i];
        }
        try {
            data[0] = (byte) 0x89;
            data[1] = (byte) 0x01;
            sendData(data);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

    private void GetFlashContentStop()
    {

    }

    private final OtaConfigFragment.OtaConfigCallback mOtaConfigCallback = new OtaConfigFragment.OtaConfigCallback()
    {
        @Override
        public void onOtaConfigOk()
        {
            init_progress_view();
            updateProgress(0);
            if (daulApply == APPLY_STEREO_OLD_VERSION) {
                //sendCmdDelayed(CMD_CONNECT, 0);
                sendCmdDelayed(CMD_RESUME_OTA_CHECK_MSG, 0);
            }
            else {
                sendCmdDelayed(CMD_APPLY_THE_IMAGE_MSG, 0);
                LOG("onOtaConfigOk", "CMD_APPLY_THE_IMAGE_MSG");
            }

        }

        @Override
        public void onOtaConfigCancel()
        {

        }
    };


    private final OtaDaulPickFileFragment.OtaPickFileCallback motaPickFileCallback = new OtaDaulPickFileFragment.OtaPickFileCallback()
    {


        @Override
        public void onOtaPickFileOk()
        {

            updateDaulFile("onOtaPickFileOk");
        }

        @Override
        public void onOtaPickFileCancel()
        {

        }
    };

    @Override
    public void onResume()
    {
        LOG(TAG, "onResume");
        super.onResume();
        int i = getConnectBt();
        if ((i == -1)||(activityName.equals("LeOtaActivity"))) {
            pickDevice.setVisibility(View.VISIBLE);
        }
        else {
            pickDevice.setVisibility(View.GONE);
            getConnectBtDetails(i);
        }
        pick_new_file = false;
        if(checkResumeState()==true)
        {
            resume();
        }

    }

    @Override
    protected void onStop()
    {
        LOG(TAG, "onStop");

        super.onStop();

    }


    protected void LOG(String TAG, String msg)
    {
        if (msg != null && TAG != null) {
            if (isBle()) {
                LogUtils.writeForBle(TAG, msg);
            }
            else {
                LogUtils.writeForClassicBt(TAG, msg);
            }
        }
    }


    public void showChooseApplyDialog()
    {
        final String[] items = {mContext.getString(R.string.left_earbud_only), mContext.getString(R.string.right_earbud_only), mContext.getString(R.string.both_earbuds_in_one_bin), mContext.getString(R.string.both_earbuds_in_two_bins)};
        final int past = daulApply;
        AlertDialog.Builder singleChoiceDialog =
                new AlertDialog.Builder(mContext);
        singleChoiceDialog.setTitle(mContext.getString(R.string.daul_earbuds_ota_apply));
        // 第二个参数是默认选项，此处设置为0
        singleChoiceDialog.setSingleChoiceItems(items, -1,
                new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        daulApply = which;
                    }
                });
        singleChoiceDialog.setPositiveButton("确定",
                        new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            if ((daulApply != APPLY_STEREO)&&(daulApply!=APPLY_STEREO_OLD_VERSION)) {
                                LOG("11111111111111", daulApply + "");
                                SPHelper.putPreference(mContext, Constants.KEY_OTA_DAUL_APPLY_WAY, daulApply);
                                SPHelper.putPreference(mContext, Constants.KEY_OTA_UPGRADE_WAY, daulApply);

                                AlertDialog.Builder alertDialog=new AlertDialog.Builder(mContext);
                                View connect_view = View.inflate(mContext,R.layout.ota_daul_pick_file,null);
                                alertDialog
                                        .setTitle("bin choose")
                                        .setIcon(R.mipmap.ic_launcher)
                                        .setView(connect_view)
                                        .setCancelable(false)
                                        .create();
                                AlertDialog  mshow = alertDialog.show();
                                motaDaulPickFileFragment = new OtaDaulPickFileFragment(exview,connect_view,mshow,daulApply);
                                motaDaulPickFileFragment.setPickFileCallback(motaPickFileCallback);
//                                    Bundle bundle = new Bundle();
//                                    bundle.putInt("apply_type", daulApply);
//                                    motaDaulPickFileFragment.setArguments(bundle);
//                                    motaDaulPickFileFragment.show(getSupportFragmentManager(), OTA_DAUL_PICK_FILE);
                                clearAllBreakPointInfo();
                                resume_enable = false;
                                pick_new_file = true;

                            }
                        }
                });
        singleChoiceDialog.show();
    }
    public void  SendResult(int requestCode, int resultCode, Intent data) {

        motaDaulPickFileFragment.onActivityResult( requestCode, resultCode, data);

    }



    //获取已连接的蓝牙设备名称
    private void getConnectBtDetails(int flag)
    {
        bluetoothAdapter.getProfileProxy(mContext, new BluetoothProfile.ServiceListener()
        {
            @Override
            public void onServiceDisconnected(int profile)
            {
                Toast.makeText(mContext, profile + "", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onServiceConnected(int profile, BluetoothProfile proxy)
            {
                List<BluetoothDevice> mDevices = proxy.getConnectedDevices();
                if (mDevices != null && mDevices.size() > 0) {
                    for (BluetoothDevice device : mDevices) {
                        if(device.getName().toUpperCase().contains("EVOCO") || device.getName().toUpperCase().contains("VERSO"))
                        {
                            saveLastDeviceName(device.getName());
                            saveLastDeviceAddress(device.getAddress());
                            mDevice = BtHelper.getRemoteDevice(mContext, device.getAddress().toString());
                            updateConnectedBtName(device.getName());
                            updateConnectedBtAddress(device.getAddress());
                            LOG("getConnectBtDetails", "mDevice" + mDevice + "," + device.getAddress());
                            return;
                        }

                    }
                }
                else {
                    updateConnectedBtAddress("请在手机端和耳机配对，以使用相应功能");
                    updateConnectedBtName("--");
                }
            }
        }, flag);

    }

    //获取已连接的蓝牙设备状态
    public int getConnectBt()
    {
        if (bluetoothAdapter == null) {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }
        int a2dp = bluetoothAdapter.getProfileConnectionState(BluetoothProfile.A2DP);
        int headset = bluetoothAdapter.getProfileConnectionState(BluetoothProfile.HEADSET);
        int health = bluetoothAdapter.getProfileConnectionState(BluetoothProfile.HEALTH);
        int flag = -1;
        if (a2dp == BluetoothProfile.STATE_CONNECTED) {
            flag = a2dp;
        }
        else if (headset == BluetoothProfile.STATE_CONNECTED) {
            flag = headset;
        }
        else if (health == BluetoothProfile.STATE_CONNECTED) {
            flag = health;
        }
        LOG("getConnectBt flag", flag + "");
        return flag;
    }

    protected void leftVolUp(){}
    protected void leftVolDown(){}
    protected void rightVolUp(){}
    protected void rightVolDown(){}
    protected void getDeviceInfo(){}
    protected void setPresetMild(){}
    protected void setPresetMildToSever(){}
    protected void setPresetSever(){}
    protected void setModeAggressive(){}
    protected void setModeAutomatic(){}
    protected void setModePassthrough(){}

    protected String byteToString(byte data){
        switch (data){
            case (byte) 0x00:
                return "0";
            case (byte) 0x01:
                return "1";
            case (byte) 0x02:
                return "2";
            case (byte) 0x03:
                return "3";
            case (byte) 0x04:
                return "4";
            case (byte) 0x05:
                return "5";
            case (byte) 0x06:
                return "6";
            case (byte) 0x07:
                return "7";
            case (byte) 0x08:
                return "8";
            case (byte) 0x09:
                return "9";
            case (byte) 0x0A:
                return "10";
            case (byte) 0x0B:
                return "11";
            case (byte) 0x0C:
                return "12";
            case (byte) 0x0D:
                return "13";
            case (byte) 0x0E:
                return "14";
            case (byte) 0x0F:
                return "15";
            case (byte) 0x10:
                return "16";
            case (byte) 0x11:
                return "17";
            default:
                return "Error";
        }
    }

    protected void customCommand(byte data[]){
        String currentContext = mcurrentVersionDetails;
        String leftBattery = byteToString(data[0])+"0%";
        String rightBattery = byteToString(data[1])+"0%";
        String leftVol = byteToString(data[2]);
        String rightVol = byteToString(data[3]);
        String leftPreset = byteToString(data[4]);
        String rightPreset = byteToString(data[5]);
        String runningMode = byteToString(data[6]);
        currentContext=currentContext+"\nLeft battery status: "+leftBattery;
        currentContext=currentContext+"\nRight battery status: "+rightBattery;
        currentContext=currentContext+"\nLeft volume status: "+leftVol;
        currentContext=currentContext+"\nRight volume status: "+rightVol;
        currentContext=currentContext+"\nLeft preset: "+leftPreset;
        currentContext=currentContext+"\nRight preset: "+rightPreset;
        currentContext=currentContext+"\nRunning Mode: "+runningMode;
        updateVersion(currentContext);
    }
    private Handler timer_handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 0:
                    // 移除所有的msg.what为0等消息，保证只有一个循环消息队列再跑
                    timer_handler.removeMessages(0);
                    Log.i("timer_handler","time counter_jtt" );
                    // app的功能逻辑处理
                    getDeviceInfo();
                    // 再次发出msg，循环更新
                    timer_handler.sendEmptyMessageDelayed(0, 1000);
                    break;

                case 1:
                    // 直接移除，定时器停止
                    timer_handler.removeMessages(0);
                    break;

                default:
                    break;
            }
        }
    };

    public  void init_progress_view()
    {

        AlertDialog.Builder alertDialog=new AlertDialog.Builder(mContext);
        View connect_view = View.inflate(mContext,R.layout.ota_progress,null);
        alertDialog
                .setTitle("OTA Progress")
                .setIcon(R.mipmap.ic_launcher)
                .setView(connect_view)
                .setCancelable(false)
                .create();
        AlertDialog show=alertDialog.show();
        mOtaProgress=connect_view.findViewById(R.id.ota_progress);
        mOtaStatus=connect_view.findViewById(R.id.ota_status);
        mUpdateStatic=connect_view.findViewById(R.id.update_static);
        progress_show=alertDialog;
        progress_ok=connect_view.findViewById(R.id.ok_progress);
        progress_cancel=connect_view.findViewById(R.id.cancel_progress);
        progress_ok.setVisibility(View.GONE);
        progress_cancel.setVisibility(View.GONE);

        progress_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               show.dismiss();

            }
        });
        progress_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show.dismiss();
            }
        });

//        mota=connect_view.findViewById(R.id.progress_info);
//        progress_info_list=connect_view.findViewById(R.id.progress_info);

    }

}
