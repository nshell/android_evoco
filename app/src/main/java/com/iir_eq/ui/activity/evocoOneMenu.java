package com.iir_eq.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.room.Room;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.iir_eq.R;
import com.iir_eq.db.AppDatabase;
import com.iir_eq.util.backendLibrary;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class evocoOneMenu extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private RequestQueue mRequestQ;
    private String TAG = "evocoOneMenu";

    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private Context context;
    Handler uiThreadHandler = new Handler(){
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle bundle = msg.getData();
        }
    };

    private Timer timer;
    private TimerTask task;

    private final int START_OTA=0;

    @BindView(R.id.evoco_one_menu_toolbar) Toolbar mToolBar;
    @BindView(R.id.evoco_one_menu_hearing_loss) TextView mAudiogram;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evoco_one_menu);
        setSupportActionBar((Toolbar)findViewById(R.id.evoco_one_menu_toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();
        mRequestQ = Volley.newRequestQueue(this);
        context = this;
        sharedPref = context.getSharedPreferences(getString(R.string.evoco_one_preference_file_key), Context.MODE_PRIVATE);
        editor = sharedPref.edit();
    }

    @OnClick(R.id.evoco_one_menu_hearing_loss)
    public void audiogramSectionOnClick(View v){
        Intent toAudiogram = new Intent(this,evocoOneAudiogramActivity.class);
        startActivity(toAudiogram);
    }

    @OnClick(R.id.evoco_one_menu_update)
    public void OTASectionOnClick(View v){
        Intent toOTA = new Intent(this, evocoOneOTA.class);
        startActivityForResult(toOTA,START_OTA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCcde, Intent data){

        super.onActivityResult(requestCode,resultCcde,data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
