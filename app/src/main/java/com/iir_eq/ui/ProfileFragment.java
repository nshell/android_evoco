package com.iir_eq.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.iir_eq.R;
import com.iir_eq.bluetooth.callback.Startpickfilecallback;
import com.iir_eq.ui.activity.LeOtaActivity;
import com.iir_eq.ui.activity.MainActivity;
import com.iir_eq.ui.fragment.OtaDaulPickFileFragment;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;


public class ProfileFragment extends Fragment implements Startpickfilecallback {
    private static int state_file;
    private LeOtaActivity ota;
    public static ProfileFragment pr;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle){
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ota= MainActivity.ota;
        pr=this;
        Log.i("PickFileCallback", "onCreateView:_jtt");


        Button upload_btn=(Button)view.findViewById(R.id.uploadbtn);
        upload_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: pop up window and upload an audiogram, start an intent
                Log.i("upload a audiogram", "onClick: upload");
                ota.showChooseApplyDialog();



            }
        });


        return view;
    }

//    private final OtaDaulPickFileFragment.PickFileCallback mPickFileCallback = request -> {
//        Log.i("PickFileCallback", "PickFileOk:_jtt");
//        startActivityForResult(new Intent(getContext(), FilePickerActivity.class), request);
//    };
        @Override
    public void PickFileCallback(int request)
        {
            Log.i("PickFileCallback", "PickFileOk:_jtt");
            startActivityForResult(new Intent(getContext(), FilePickerActivity.class), request);
        }

    @Override
    public void startset(String setting) {
        startActivity(new Intent(setting));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("OtaDaulPickFileFragment", "onActivityResult");

        if (requestCode == OtaDaulPickFileFragment.REQUEST_OTA_DAUL_LEFT_FILE || requestCode == OtaDaulPickFileFragment.REQUEST_OTA_DAUL_RIGHT_FILE) {
            ota.SendResult(requestCode, resultCode, data);
        }
    }




}
