package com.iir_eq.ui;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.iir_eq.ui.activity.LeOtaActivity;
import com.iir_eq.ui.activity.LeScanActivity;

import com.iir_eq.R;
import com.iir_eq.ui.activity.MainActivity;
import com.iir_eq.util.FileUtils;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.OnClick;


public class ControlFragment extends Fragment implements View.OnClickListener{
    private View controlfrg;
    private LeScanActivity lescan;
    private LeOtaActivity ota;
    private static final int REQUEST_DEVICE = 0x01;
    TextView mAddress;
    TextView mName;
    public static final int RESULT_OK = -1;
    protected BluetoothDevice mDevice;
    protected  byte left_preset=0x00;
    protected byte right_preset=0x00;
    private static final  boolean is_left=true;


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.preset_dropdownbtn:
                initPopwindow(0);
                break;
            case R.id.battery_dropdown:
                initPopwindow(1);
                break;
            case R.id.volume_dropdown:
                initPopwindow(2);
                break;
                case R.id.auto_btn:
                initPopwindow(3);
                break;

        }

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
//        Log.i("ControlFragment", "onActivityResult_jtt");
//        if (requestCode == ota.REQUEST_OTA_FILE) {
//          ota.onPickFile(resultCode, data);
//    }
//        else if (requestCode == REQUEST_DEVICE) {
//
//         //   ota.onPickDevice(resultCode, data);
//            ota.ota_info_log_path = FileUtils.writeOtaInfoReport();
//            Log.i("connectDevice","mcurrentVersionDetails_jtt");
//            ota.connectDevice();
//        }

    }

    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        Log.i("onCreate", "onCreate: jtt");
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle){
        View view=inflater.inflate(R.layout.fragment_control, container, false);
        Log.i("onCreateView", "onCreateView:jtt ");
        ota= MainActivity.ota;
        lescan=MainActivity.lescan;

        ImageButton preset_btn=(ImageButton)view.findViewById(R.id.preset_dropdownbtn);
        ImageButton volume_btn=(ImageButton)view.findViewById(R.id.volume_dropdown);
        ImageButton battery_btn=(ImageButton)view.findViewById(R.id.battery_dropdown);

        ImageButton auto=(ImageButton)view.findViewById(R.id.auto_btn);
        ImageButton aggressive=(ImageButton)view.findViewById(R.id.aggressive_btn);
        ImageButton lower_power=(ImageButton)view.findViewById(R.id.lowpower_btn);

        mName = view.findViewById(R.id.devices_text);
        mAddress =view.findViewById(R.id.deviceaddress_text);
        battery_btn.setOnClickListener(this);
        volume_btn.setOnClickListener(this);
        preset_btn.setOnClickListener(this);
        if(ota.denoise_mode == 0)
        {
            aggressive.setBackgroundResource(R.drawable.radius_btn_active);
            auto.setBackgroundResource(R.drawable.radius_btn_white);
            lower_power.setBackgroundResource(R.drawable.radius_btn_white);
        }else if(ota.denoise_mode == 1)
        {
            aggressive.setBackgroundResource(R.drawable.radius_btn_white);
            auto.setBackgroundResource(R.drawable.radius_btn_active);
            lower_power.setBackgroundResource(R.drawable.radius_btn_white);

        }else if(ota.denoise_mode == 2)
        {
            aggressive.setBackgroundResource(R.drawable.radius_btn_white);
            auto.setBackgroundResource(R.drawable.radius_btn_white);
            lower_power.setBackgroundResource(R.drawable.radius_btn_active);

        }

        auto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ota.setModeAutomatic();
                aggressive.setBackgroundResource(R.drawable.radius_btn_white);
                auto.setBackgroundResource(R.drawable.radius_btn_active);
                lower_power.setBackgroundResource(R.drawable.radius_btn_white);

            }
        });
        aggressive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            ota.setModeAggressive();
                aggressive.setBackgroundResource(R.drawable.radius_btn_active);
                auto.setBackgroundResource(R.drawable.radius_btn_white);
                lower_power.setBackgroundResource(R.drawable.radius_btn_white);
            }
        });
        lower_power.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ota.setModePassthrough();
                aggressive.setBackgroundResource(R.drawable.radius_btn_white);
                auto.setBackgroundResource(R.drawable.radius_btn_white);
                lower_power.setBackgroundResource(R.drawable.radius_btn_active);
            }
        });


        controlfrg=view;
        return view;
    }


    private void initPopwindow(int case_select){

      if(case_select==0) {
          ota.getDeviceInfo();
          View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_preset, null, false);
          final PopupWindow popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
          popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));

          popupWindow.showAsDropDown(controlfrg.findViewById(R.id.preset_text));

          RadioGroup leftRadio = (RadioGroup) view.findViewById(R.id.left_radio);
          RadioGroup rightRadio = (RadioGroup) view.findViewById(R.id.right_radio);


              switch (ota.master_preset) {
                  case 0x00:
                      leftRadio.check(R.id.left_mild);
                      break;
                  case 0x01:
                      leftRadio.check(R.id.left_moderate);
                      break;
                  case 0x02:
                      leftRadio.check(R.id.left_strong);
                      break;
                  case 0x03:
                      leftRadio.check(R.id.left_profound);
                      break;
                  default:
                      leftRadio.check(R.id.left_mild);
                      break;
              }
          switch (ota.slave_preset) {
              case 0x00:
                  rightRadio.check(R.id.right_mild);
                  break;
              case 0x01:
                  rightRadio.check(R.id.right_moderate);
                  break;
              case 0x02:
                  rightRadio.check(R.id.right_strong);
                  break;
              case 0x03:
                  rightRadio.check(R.id.right_profound);
                  break;
              default:
                  rightRadio.check(R.id.right_mild);
                  break;
          }


          leftRadio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
              @Override
              public void onCheckedChanged(RadioGroup group, int checkedId) {
                  Log.i("Radiogroupcheck", "onCheckedChanged: leftradio");
                  switch(leftRadio.getCheckedRadioButtonId())
                  {
                      case R.id.left_mild:
                          ota.setPresetMinimal(is_left);
                          ota.master_preset=0x00;
                              break;
                      case R.id.left_moderate:
                          ota.setPresetMild(is_left);
                          ota.master_preset=0x01;
                          break;
                      case R.id.left_strong:
                          ota.setPresetMildToSever(is_left);
                          ota.master_preset=0x02;
                          break;
                      case R.id.left_profound:
                          ota.setPresetSever(is_left);
                          ota.master_preset=0x03;
                          break;
                  }

              }
          });
          rightRadio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
              @Override
              public void onCheckedChanged(RadioGroup group, int checkedId) {
                  Log.i("Radiogroupchecked", "onCheckedChanged: rightradio");
                  switch(rightRadio.getCheckedRadioButtonId())
                  {
                      case R.id.right_mild:
                          ota.setPresetMinimal(!is_left);
                          ota.slave_preset=0x00;
                          break;
                      case R.id.right_moderate:
                          ota.setPresetMild(!is_left);
                          ota.slave_preset=0x01;
                          break;
                      case R.id.right_strong:
                          ota.setPresetMildToSever(!is_left);
                          ota.slave_preset=0x02;
                          break;
                      case R.id.right_profound:
                          ota.setPresetSever(!is_left);
                          ota.slave_preset=0x03;
                          break;

                  }
              }
          });
      }



        if (case_select==1){
            View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_battery, null, false);
            final PopupWindow popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
            popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));
            popupWindow.showAsDropDown(controlfrg.findViewById(R.id.battery_text));
            TextView right_battery_status=view.findViewById(R.id.right_battery_status);
            TextView left_battery_status=view.findViewById(R.id.left_battery_status);
            right_battery_status.setText("battery status:"+(ota.slave_battery_level+1)*10+"%");
            left_battery_status.setText("battery status:"+(ota.master_battery_level+1)*10+"%");

        }


        if (case_select==2){
            View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_volume, null, false);
            final PopupWindow popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
            popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));
            popupWindow.showAsDropDown(controlfrg.findViewById(R.id.volume_text));
            Button left_up=view.findViewById(R.id.left_up);
            Button right_up=view.findViewById(R.id.right_up);
            Button left_down=view.findViewById(R.id.left_down);
            Button right_down=view.findViewById(R.id.right_down);
            TextView left_volume=view.findViewById(R.id.left_volume);
            TextView right_volume=view.findViewById(R.id.right_volume);
            right_volume.setText("Right Volume "+ota.slave_volume_level+"%");
            left_volume.setText("Left Volume "+ota.master_volume_level+"%");
            left_up.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ota.leftVolUp();
                    ota.master_volume_level++;
                    if(ota.master_volume_level>17)ota.master_volume_level=17;
                    left_volume.setText("Left Volume "+ota.master_volume_level+"%");

                }
            });
            right_up.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ota.rightVolUp();
                    ota.slave_volume_level++;
                    if(ota.slave_volume_level>17)ota.slave_volume_level=17;
                    right_volume.setText("Right Volume "+ota.slave_volume_level+"%");

                }
            });
            left_down.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ota.leftVolDown();
                    if(ota.master_volume_level>0)ota.master_volume_level--;

                    left_volume.setText("Left Volume "+ota.master_volume_level+"%");
                }
            });
            right_down.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ota.rightVolDown();
                    if(ota.slave_volume_level>0)ota.slave_volume_level--;
                    right_volume.setText("Right Volume "+ota.slave_volume_level+"%");
                }
            });
        }


        if (case_select==3){

//            Log.i("Radiogroupcheck", "initial lescanactivity");
//            View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_devices, null, false);
//            final PopupWindow popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
//            popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));
//            popupWindow.showAsDropDown(controlfrg.findViewById(R.id.devices_text));
//            //lescan.LescaInit(LeScanActivity.MODE_OTA,view,popupWindow,ota);


        }
    }



}
