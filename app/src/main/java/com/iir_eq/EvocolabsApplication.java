package com.iir_eq;
import com.evocolabs.app.EvocoOneBleService;
import com.iir_eq.ui.activity.evocoOneControl;
import com.shakebugs.shake.Shake;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.iir_eq.util.FileUtils;

public class EvocolabsApplication extends Application implements  Application.ActivityLifecycleCallbacks{

	private  Context applicationContext;
	CrashExceptionHandler crashExceptionHandler ;
	private int activityStartCount = 0;

	@Override
	public void onCreate() {
		super.onCreate();
		Shake.start(this);
		Shake.getReportConfiguration().setShowFloatingReportButton(false);
		applicationContext = getApplicationContext();
		FileUtils.updateMainPath(applicationContext);
		crashExceptionHandler = CrashExceptionHandler.getInstance();
		crashExceptionHandler.init(applicationContext);
		registerActivityLifecycleCallbacks(this);

	}

	@Override
	public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

	}

	@Override
	public void onActivityStarted(Activity activity) {
		activityStartCount++;
	}

	@Override
	public void onActivityResumed(Activity activity) {

	}

	@Override
	public void onActivityPaused(Activity activity) {

	}

	@Override
	public void onActivityStopped(Activity activity) {
		activityStartCount--;
		if (activityStartCount == 0){
			sendControl(EvocoOneBleService.NOTIFY_APP_BACKGROUND);
		}
	}

	public void sendControl(int controlCode){
		Log.d("EvocoApplication", "sendControl: ");
		Intent broadCastIntent= new Intent();
		broadCastIntent.setAction(EvocoOneBleService.EVOCO_ONE_BLE_SERVICE_ACTION);
		broadCastIntent.putExtra(EvocoOneBleService.CONTROL_COMMAND_FIELD, controlCode);
		sendBroadcast(broadCastIntent);
	}

	@Override
	public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

	}

	@Override
	public void onActivityDestroyed(Activity activity) {

	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
	}
}
