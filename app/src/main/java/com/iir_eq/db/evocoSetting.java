package com.iir_eq.db;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class evocoSetting {
    @PrimaryKey
    public String key;
    public String value;
}
