package com.iir_eq.db;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

public interface evocoSettingDAO {
    @Query("SELECT * FROM evocoSetting")
    List<evocoSetting> getAll();

    @Query("SELECT * FROM evocoSetting WHERE evocoSetting.`key` = :key LIMIT 1")
    evocoSetting findValueByKey(String key);

    @Update
    evocoSetting updateSetting(evocoSetting theSetting);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(evocoSetting... evocoSettings);

    @Delete
    void delete(evocoSetting single_setting);
}
