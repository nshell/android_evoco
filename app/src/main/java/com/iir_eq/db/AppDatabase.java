package com.iir_eq.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {evocoSetting.class}, version = 1)
public abstract  class AppDatabase extends RoomDatabase {
    public abstract evocoSettingDAO settingDao();
}
