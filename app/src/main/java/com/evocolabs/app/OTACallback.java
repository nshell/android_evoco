package com.evocolabs.app;

public interface OTACallback {
    public void onOtaProgressUpdated(int progress);
    public void onOtaStateChanged(int state);
}
