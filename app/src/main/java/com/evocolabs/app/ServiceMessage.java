package com.evocolabs.app;

public class ServiceMessage {
    public static final String EVOCO_ONE_UI_ACTION = "com.evocolabs.app.UI_THREAD_BROADCAST_ACTION";
    public static final int MSG_SERVICE_STATUS = 0x01;
    public static final int MSG_CONNECTION_STATUS = 0x02;
    public static final int MSG_GENERAL_INFO = 0x03;
    public static final int MSG_FIRMWARE_INFO = 0x04;
    public static final int MSG_OTA_PROGRESS = 0x05;
    public static final int MSG_OTA_STATUS = 0x06;
}