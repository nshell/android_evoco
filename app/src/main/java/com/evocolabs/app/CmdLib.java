package com.evocolabs.app;

import android.util.Log;

import com.iir_eq.util.ArrayUtil;

public class CmdLib {
    // BLE SEND DATA THROUGH LITTLE ENDIAN THUS WE NEED TO REVERSE OUR COMMAND
    public static final String TAG="CmdLib";
    public static final byte[] CMD_GET_GENERAL_STATUS = ArrayUtil.hexStringToByte("00000101");
    public static final byte[] CMD_GET_HARDWARE_INFO = ArrayUtil.hexStringToByte("00000201");
    public static final byte[] CMD_NOTIFY_APP_BACKGROUND = ArrayUtil.hexStringToByte("00000301");
    public static final byte[] CMD_ACKNOWLEDGE_RECEIVE = ArrayUtil.hexStringToByte("00000401");
    public static final byte[] CMD_SET_LEFT_VOLUME_UP = ArrayUtil.hexStringToByte("00000102");
    public static final byte[] CMD_SET_RIGHT_VOLUME_UP = ArrayUtil.hexStringToByte("00000202");
    public static final byte[] CMD_SET_TOGGLE_STREAM = ArrayUtil.hexStringToByte("00000502");
    public static final byte[] CMD_SET_LEFT_PRESET_MINIMAL = ArrayUtil.hexStringToByte("00000103");
    public static final byte[] CMD_SET_RIGHT_PRESET_MINIMAL = ArrayUtil.hexStringToByte("00000203");
    public static final byte[] CMD_SET_LEFT_PRESET_MILD = ArrayUtil.hexStringToByte("00000303");
    public static final byte[] CMD_SET_RIGHT_PRESET_MILD = ArrayUtil.hexStringToByte("00000403");
    public static final byte[] CMD_SET_LEFT_PRESET_MODERATE = ArrayUtil.hexStringToByte("00000503");
    public static final byte[] CMD_SET_RIGHT_PRESET_MODERATE = ArrayUtil.hexStringToByte("00000603");
    public static final byte[] CMD_SET_LEFT_PRESET_MODERATELY_SEVER = ArrayUtil.hexStringToByte("00000703");
    public static final byte[] CMD_SET_RIGHT_PRESET_MODERATELY_SEVER = ArrayUtil.hexStringToByte("00000803");
    public static final byte[] CMD_SET_DENOISE_MODE_AGGRESSIVE = ArrayUtil.hexStringToByte("00000903");
    public static final byte[] CMD_SET_DENOISE_MODE_AUTOMATIC = ArrayUtil.hexStringToByte("00000A03");
    public static final byte[] CMD_SET_DENOISE_MODE_PASSTHROUGH = ArrayUtil.hexStringToByte("00000B03");

    public static byte[] CMD_SET_LEFT_VOLUME_VALUE(int VolumeValue){
        if (VolumeValue<=0x0f){
            return ArrayUtil.hexStringToByte("000" + Integer.toHexString(VolumeValue).toUpperCase() + "0602");
        }else{
            return ArrayUtil.hexStringToByte("00" + Integer.toHexString(VolumeValue).toUpperCase() + "0602");
        }
    }

    public static byte[] CMD_SET_RIGHT_VOLUME_VALUE(int VolumeValue){
        if (VolumeValue<=0x0f){
            return ArrayUtil.hexStringToByte("000" + Integer.toHexString(VolumeValue).toUpperCase() + "0702");
        }else{
            return ArrayUtil.hexStringToByte("00" + Integer.toHexString(VolumeValue).toUpperCase() + "0702");
        }
    }

    public static byte[] CMD_SET_LEFT_GAIN_TABLE(String gain_table, int version){
        Log.d(TAG, "CMD_SET_LEFT_GAIN_TABLE: ");
        String strGainTable = "";
        Integer integerVersion = version;
        String[] gainTableArray = gain_table.split(",");
        for (String item:gainTableArray){
            strGainTable = String.format("%02X",intToByteArray(Integer.parseInt(item))[3]) + strGainTable;
        }
        byte[] x = ArrayUtil.hexStringToByte(strGainTable+ String.format("%02X",integerVersion.byteValue()) + "0C03");
        return x;
    }

    public static byte[] CMD_SET_RIGHT_GAIN_TABLE(String gain_table, int version){
        Log.d(TAG, "CMD_SET_RIGHT_GAIN_TABLE: ");
        String strGainTable = "";
        Integer integerVersion = version;
        String[] gainTableArray = gain_table.split(",");
        String tmpString = "";
        int i=0;
        for (String item:gainTableArray){
            i++;
            tmpString =  String.format("%02X",intToByteArray(Integer.parseInt(item))[3]);
            Log.d(TAG, "CMD_SET_RIGHT_GAIN_TABLE: "+tmpString+"; "+i);
            strGainTable = tmpString + strGainTable;
        }
        Log.d(TAG, "CMD_SET_RIGHT_GAIN_TABLE: versionCode: "+String.format("%02X",integerVersion.byteValue()));
        byte[] x = ArrayUtil.hexStringToByte(strGainTable+ String.format("%02X",integerVersion.byteValue()) + "0D03");
        return x;
    }

    public static byte[] intToByteArray(int a) {
        return new byte[] {
                (byte) ((a >> 24) & 0xFF),
                (byte) ((a >> 16) & 0xFF),
                (byte) ((a >> 8) & 0xFF),
                (byte) (a & 0xFF)
        };
    }
}
