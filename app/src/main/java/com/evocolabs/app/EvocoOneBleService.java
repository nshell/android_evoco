package com.evocolabs.app;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.iir_eq.R;
import com.iir_eq.bluetooth.LeConnector;
import com.iir_eq.bluetooth.LeManager;
import com.iir_eq.bluetooth.callback.LeConnectCallback;
import com.iir_eq.bluetooth.callback.ScanCallback;
import com.iir_eq.bluetooth.scanner.BtScanner;
import com.iir_eq.bluetooth.scanner.LeScannerCompat;
import com.iir_eq.contants.Constants;
import com.iir_eq.ui.activity.OtaActivity;
import com.iir_eq.ui.activity.SppOtaActivity;
import com.iir_eq.util.ArrayUtil;
import com.iir_eq.util.FileUtils;
import com.iir_eq.util.FirebaseTokenHelper;
import com.iir_eq.util.FirebaseTokenManager;
import com.iir_eq.util.backendLibrary;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static android.webkit.ConsoleMessage.MessageLevel.LOG;

public class EvocoOneBleService<bleConnected> extends Service implements ScanCallback, LeConnectCallback, OTACallback {

    private static String TAG = "EvocoOneBleService";
    private BluetoothDevice mDevice;
    private String mDeviceName;
    private String mDeviceAddress;
    private LeManager mLeManager;
    private BtScanner mScanner;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private Context thisContext;
    private CmdHandler mCmdHandler;
    private Handler mScanHandler;
    private Handler mDelayHandler;
    private HandlerThread mCmdThread;
    private BroadcastReceiver mBroadcastReceiver;
    private IntentFilter mFilter;
    private SppOtaActivity ota;
    private boolean mWritten = true;
    private int sendMsgFailCount = 0;
    private int mMtu;
    private boolean checkGeneralStatusFlag = false;
    private boolean checkHardwareInfoFlag = false;
    private int currentRecordedConnectionStatus = CONNECTION_STATUS_DISCONNECTED;
    private boolean waitingGeneralStatus = false;
    private boolean waitingHardwareStatus = false;
    private boolean waitingConnectSavedDeviceFlag = false;
    private boolean waitingFWMasterLoggingSyncEnd = false;
    private boolean waitingFWSlaveLoggingSyncEnd = false;
    private boolean activeDisconnect = false;
    private final Object bleConnectStateLock = new Object();

    protected static final int CMD_START_SCAN = 0x01;
    protected static final int CMD_STOP_SCAN = 0x02;
    protected static final int CMD_SEND_FW_LOG = 0x13;
    protected static final int CMD_CONNECT = 0x80;
    protected static final int CMD_DISCONNECT = 0x81;
    protected static final int CMD_LOAD_FILE = 0x82;
    protected static final int CMD_START_OTA = 0x83;
    protected static final int CMD_OTA_NEXT = 0x84;
    protected static final int CMD_SEND_FILE_INFO = 0x85;
    protected static final int CMD_LOAD_FILE_FOR_NEW_PROFILE = 0x86;
    protected static final int CMD_RESEND_MSG = 0x88;
    protected static final int CMD_LOAD_FILE_FOR_NEW_PROFILE_SPP = 0x89;
    protected static final int CMD_RESUME_OTA_CHECK_MSG = 0x8C;   //resume
    protected static final int CMD_RESUME_OTA_CHECK_MSG_RESPONSE = 0x8D; //resume back
    protected static final int CMD_SEND_HW_INFO = 0x8E;   //   read current version
    protected static final int CMD_READ_CURRENT_VERSION_RESPONSE = 0x8F; // read version response
    protected static final int CMD_APPLY_THE_IMAGE_MSG = 0x99;   //   apply the image
    protected static final int CMD_APPLY_CHANGE = 0x9A;   //   apply the image
    protected static final int CMD_OVERWRITING_CONFIRM = 0x9B;
    protected static final int CMD_LOAD_OTA_CONFIG = 0x90;
    protected static final int CMD_START_OTA_CONFIG = 0x91;
    protected static final int CMD_OTA_CONFIG_NEXT = 0x92;
    protected static final int CMD_READY_FLASH_CONTENT = 0x93;
    protected static final int CMD_GET_BUILD_INFO_ADDRESS = 0x9C;
    protected static final int CMD_STOP_FLASH_CONTENT = 0x9D;

    protected static final int DEFAULT_MTU = 512;
    public static final String EVOCO_ONE_BLE_SERVICE_ACTION = "com.evocolabs.app.EVOCO_ONE_BLE_SERVICE_BROADCAST_RECEIVER";

    public static final String CONTROL_COMMAND_FIELD = "CONTROL_COMMAND";
    public static final String VOLUME_VALUE_FIELD = "VOLUME_VALUE";
    public static final String OTA_PROGRESS_FIELD = "OTA_PROGRESS";
    public static final String OTA_STATUS_FIELD = "OTA_STATUS";
    public static final int CHECK_SERVICE_ALIVE = 0x01;
    public static final int SET_STOP_SERVICE = 0x02;
    public static final int CHECK_CONNECTION_STATUS = 0x03;
    public static final int START_CONNECT = 0x0f;
    public static final int CHECK_GENERAL_STATUS = 0x11;
    public static final int CHECK_HARDWARE_INFO = 0x12;
    public static final int NOTIFY_APP_BACKGROUND = 0x13;
    public static final int SET_LEFT_VOLUME_UP = 0x21;
    public static final int SET_RIGHT_VOLUME_UP = 0x22;
    public static final int SET_LEFT_VOLUME_DOWN = 0x23;
    public static final int SET_RIGHT_VOLUME_DOWN = 0x24;
    public static final int TOGGLE_STREAMING_MODE = 0x25;
    public static final int SET_LEFT_VOLUME_VALUE = 0x26;
    public static final int SET_RIGHT_VOLUME_VALUE = 0x27;
    public static final int SET_MUSIC_PLAY = 0x28;
    public static final int SET_MUSIC_PAUSE = 0x29;
    public static final int SET_MUSIC_REWIND = 0x2A;
    public static final int SET_MUSIC_FAST_FORWARD = 0x2B;
    public static final int SET_LEFT_PRESET_MINIMAL = 0x31;
    public static final int SET_RIGHT_PRESET_MINIMAL = 0x32;
    public static final int SET_LEFT_PRESET_MILD = 0x33;
    public static final int SET_RIGHT_PRESET_MILD = 0x34;
    public static final int SET_LEFT_PRESET_MODERATE = 0x35;
    public static final int SET_RIGHT_PRESET_MODERATE = 0x36;
    public static final int SET_LEFT_PRESET_MODERATELY_SEVER = 0x37;
    public static final int SET_RIGHT_PRESET_MODERATELY_SEVER = 0x38;
    public static final int SET_DENOISE_MODE_AGGRESSIVE = 0x39;
    public static final int SET_DENOISE_MODE_AUTOMATIC = 0x3A;
    public static final int SET_DENOISE_MODE_PASSTHROUGH = 0x3B;
    public static final int SET_LEFT_GAIN_TABLE = 0x3C;
    public static final int SET_RIGHT_GAIN_TABLE = 0x3D;
    public static final int CONTROL_START_OTA = 0x50;
    public static final int CONTROL_STOP_OTA = 0x51;

    public static final int CONNECTION_STATUS_DISCONNECTED = 0x00;
    public static final int CONNECTION_STATUS_DISCONNECTING = 0x04;
    public static final int CONNECTION_STATUS_CONNECTED = 0x01;
    public static final int CONNECTION_STATUS_CONNECTING = 0x02;
    public static final int CONNECTION_STATUS_SCANNING = 0x03;


    protected BtScanner getBtScanner() {
        return LeScannerCompat.getLeScanner(this);
    }

    public EvocoOneBleService() {
    }

    @Override
    public void onDestroy(){
        ota.stopOta();
        mLeManager.close();
        this.unregisterReceiver(mBroadcastReceiver);
        mCmdThread.quit();
    }



    /**********
     * Service Extended Functions
     ***********/
    @Override
    public void onCreate(){
        mLeManager = LeManager.getLeManager();
        mScanner = getBtScanner();
        mScanHandler = new Handler();
        mDelayHandler = new Handler();
        thisContext = this;
        sharedPref = thisContext.getSharedPreferences(getString(R.string.evoco_one_preference_file_key), Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        currentRecordedConnectionStatus = CONNECTION_STATUS_DISCONNECTED;
        ota = new SppOtaActivity();
        ota.initContext(this);

    }



    @Override
    public int onStartCommand(Intent intent,int arg1,int arg2){
        //检测是否有往期设备，如果有往期设备则直接进行连接。如果无往期设备则先扫描
        mCmdThread = new HandlerThread("EvocoOneBleService");
        mCmdThread.start();
        mCmdHandler = new CmdHandler(mCmdThread.getLooper());
        boolean connectFlag = false;
        connectPastDevice();
        mBroadcastReceiver = new BleControlBroadcastReceiver();
        mFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        mFilter.addAction(EVOCO_ONE_BLE_SERVICE_ACTION);
        this.registerReceiver(mBroadcastReceiver, mFilter);
        mLeManager.addConnectCallback(this);
        return START_STICKY;
    }

    private void connectPastDevice(){
        String prevAddress = sharedPref.getString("evoco_one_device_address","--");
        if (mLeManager.isConnected()){
            sendConnectionStateChanged(CONNECTION_STATUS_CONNECTED);
            Log.d(TAG, "connect: ALREADY CONNECTED");
        }else {
            if (prevAddress != "--") {
                sendConnectionStateChanged(CONNECTION_STATUS_CONNECTING);
                waitingConnectSavedDeviceFlag = mLeManager.connect(thisContext,prevAddress);
                if (!waitingConnectSavedDeviceFlag){
                    connect();
                }
            }else{
                connect();
            }
        }
    }

    private void connect(){
        sendConnectionStateChanged(CONNECTION_STATUS_SCANNING);
        mScanner.startScan(this);
    }

    private void disconnect(){
        mLeManager.close();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }




    /**********
    * Scan Callbacks
    ***********/
    @Override
    public void onScanStart(){
        mScanHandler.postDelayed(()->{
            mScanner.stopScan();
            if (currentRecordedConnectionStatus==CONNECTION_STATUS_SCANNING){
                sendConnectionStateChanged(CONNECTION_STATUS_DISCONNECTED);
            }
        },10000);
    }

    @Override
    public void onScanFinish(){
        mScanner.close();
    }

    @Override
    public void onFound(final BluetoothDevice device, final int rssi, final byte[] scanRecord){
        Log.d(TAG, "onFoundDevice, Device Name: " + device.getName() + " , Device Address: " + device.getAddress());
        String thisDeviceName = device.getName();
        String thisDeviceAddress = device.getAddress();
        if(thisDeviceName!=null && thisDeviceName.contains("verso_A")){
            mScanner.stopScan();
            //mCmdHandler.removeMessages(CMD_STOP_SCAN);
            sendConnectionStateChanged(CONNECTION_STATUS_CONNECTING);
            if(mLeManager.connect(this,device)){
                mDevice = device;
                mDeviceName = thisDeviceName;
                mDeviceAddress = thisDeviceAddress;
            }else{
                mLeManager.close();
                sendConnectionStateChanged(CONNECTION_STATUS_DISCONNECTED);
                Toast.makeText(thisContext,"connect device error",Toast.LENGTH_LONG).show();
            }
        }
    }


    /**********
     * Handling Message From UI Thread
     ***********/
    public class BleControlBroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context,Intent intent){
            Log.d(TAG, "onReceive: Get Broadcast Message: ");
            int command = intent.getIntExtra(CONTROL_COMMAND_FIELD,0xFF);
            switch (command){
                case CHECK_SERVICE_ALIVE:
                    Log.d(TAG, "onReceive - Case CHECK_SERVICE_ALIVE: ");
                    Bundle tmpBundle = new Bundle();
                    tmpBundle.putInt("connection_status",currentRecordedConnectionStatus);
                    sendBroadcastToUI(ServiceMessage.MSG_SERVICE_STATUS, tmpBundle);
                    break;
                case CHECK_CONNECTION_STATUS:
                    Log.d(TAG, "onReceive - Case CHECK_CONNECTION_STATUS: ");
                    Bundle connectionBundle = new Bundle();
                    connectionBundle.putInt("connection_status", currentRecordedConnectionStatus);
                    sendBroadcastToUI(ServiceMessage.MSG_CONNECTION_STATUS,connectionBundle);
                    break;
                case SET_STOP_SERVICE:
                    Log.d(TAG, "onReceive - Case : SET_STOP_SERVICE");
                    EvocoOneBleService.this.stopSelf();
                    break;
                case CHECK_GENERAL_STATUS:
                    waitingGeneralStatus = true;
                    Log.d(TAG, "onReceive - Case CHECK_GENERAL_STATUS: " + sendData(CmdLib.CMD_GET_GENERAL_STATUS));
                    break;
                case START_CONNECT:
                    Log.d(TAG, "onReceive - START_CONNECT: ");
                    if(!mLeManager.isConnected()){
                        connectPastDevice();
                    }else{
                        mLeManager.close();
                        connect();
                    }
                    break;
                case CHECK_HARDWARE_INFO:
                    waitingHardwareStatus = true;
                    Log.d(TAG, "onReceive - Case GET_HARDWARE_INFO: " + sendData(CmdLib.CMD_GET_HARDWARE_INFO));
                    break;
                case SET_LEFT_VOLUME_VALUE:
                    int left_vol = intent.getIntExtra(VOLUME_VALUE_FIELD,0);
                    Log.d(TAG, "onReceive - Case SET_LEFT_VOLUME_VALUE: " + sendData(CmdLib.CMD_SET_LEFT_VOLUME_VALUE(left_vol)));
                    break;
                case SET_RIGHT_VOLUME_VALUE:
                    int right_vol = intent.getIntExtra(VOLUME_VALUE_FIELD,0);
                    Log.d(TAG, "onReceive - Case SET_RIGHT_VOLUME_VALUE: " +sendData(CmdLib.CMD_SET_RIGHT_VOLUME_VALUE(right_vol)));
                    break;
                case TOGGLE_STREAMING_MODE:
                    boolean res = sendData(CmdLib.CMD_SET_TOGGLE_STREAM);
                    Log.d(TAG, "onReceive - Case TOGGLE_STREAMING_MODE: " + res);
                    break;
                case SET_DENOISE_MODE_AGGRESSIVE:
                    Log.d(TAG, "onReceive - Case SET_DENOISE_MODE_AGGRESSIVE: "+sendData(CmdLib.CMD_SET_DENOISE_MODE_AGGRESSIVE));
                    break;
                case SET_DENOISE_MODE_AUTOMATIC:
                    Log.d(TAG, "onReceive - Case SET_DENOISE_MODE_AUTOMATIC: "+sendData(CmdLib.CMD_SET_DENOISE_MODE_AUTOMATIC));
                    break;
                case SET_DENOISE_MODE_PASSTHROUGH:
                    Log.d(TAG, "onReceive - Case SET_DENOISE_MODE_PASSTHROUGH: "+sendData(CmdLib.CMD_SET_DENOISE_MODE_PASSTHROUGH));
                    break;
                case SET_LEFT_GAIN_TABLE:
                    Bundle leftGainBundle = intent.getExtras();
                    boolean sendLeftGainRes = sendData(CmdLib.CMD_SET_LEFT_GAIN_TABLE(leftGainBundle.getString("gain_table"),leftGainBundle.getInt("version")));
                    Log.d(TAG, "onReceive - Case SET_LEFT_GAIN_TABLE: " + sendLeftGainRes);
                    break;
                case SET_RIGHT_GAIN_TABLE:
                    Bundle rightGainBundle = intent.getExtras();
                    boolean sendRightGainRes = sendData(CmdLib.CMD_SET_RIGHT_GAIN_TABLE(rightGainBundle.getString("gain_table"),rightGainBundle.getInt("version")));
                    Log.d(TAG, "onReceive - Case SET_RIGHT_GAIN_TABLE: " + sendRightGainRes);
                    break;
                case SET_LEFT_PRESET_MINIMAL:
                    Log.d(TAG, "onReceive SET_LEFT_PRESET_MINIMAL: "+sendData(CmdLib.CMD_SET_LEFT_PRESET_MINIMAL));
                    break;
                case SET_RIGHT_PRESET_MINIMAL:
                    Log.d(TAG, "onReceive SET_RIGHT_PRESET_MINIMAL: "+sendData(CmdLib.CMD_SET_RIGHT_PRESET_MINIMAL));
                    break;
                case SET_LEFT_PRESET_MILD:
                    Log.d(TAG, "onReceive SET_LEFT_PRESET_MILD: "+sendData(CmdLib.CMD_SET_LEFT_PRESET_MILD));
                    break;
                case SET_RIGHT_PRESET_MILD:
                    Log.d(TAG, "onReceive SET_RIGHT_PRESET_MILD: "+sendData(CmdLib.CMD_SET_RIGHT_PRESET_MILD));
                    break;
                case SET_LEFT_PRESET_MODERATE:
                    Log.d(TAG, "onReceive SET_LEFT_PRESET_MODERATE: "+sendData(CmdLib.CMD_SET_LEFT_PRESET_MODERATE));
                    break;
                case SET_RIGHT_PRESET_MODERATE:
                    Log.d(TAG, "onReceive SET_RIGHT_PRESET_MODERATE: "+sendData(CmdLib.CMD_SET_RIGHT_PRESET_MODERATE));
                    break;
                case SET_LEFT_PRESET_MODERATELY_SEVER:
                    Log.d(TAG, "onReceive SET_LEFT_PRESET_MODERATELY_SEVER: "+sendData(CmdLib.CMD_SET_LEFT_PRESET_MODERATELY_SEVER));
                    break;
                case SET_RIGHT_PRESET_MODERATELY_SEVER:
                    Log.d(TAG, "onReceive SET_RIGHT_PRESET_MODERATELY_SEVER: "+sendData(CmdLib.CMD_SET_RIGHT_PRESET_MODERATELY_SEVER));
                    break;
                case CONTROL_START_OTA:
                    Log.d(TAG, "onReceive CONTROL_START_OTA: ");
                    startOta();
                    break;
                case CONTROL_STOP_OTA:
                    Log.d(TAG, "onReceive CONTROL_STOP_OTA: ");
                    destroyOta();
                case NOTIFY_APP_BACKGROUND:
                    Log.d(TAG, "onReceive NOTIFY_APP_BACKGROUND: ");
                    notifyAppBackground();
                default:
                    Log.d(TAG, "onReceive - UNKOWN COMAND: "+command);
                    break;
            }
        }
    };
    /**********
     * Handle Sending Broadcast To UI Thread
     ***********/
    private void sendBroadcastToUI(int serviceCode,Bundle bundle){
        Intent broadcastToUI = new Intent();
        broadcastToUI.putExtra("SERVICE_MESSAGE",serviceCode);
        broadcastToUI.setAction(ServiceMessage.EVOCO_ONE_UI_ACTION);
        broadcastToUI.putExtras(bundle);
        sendBroadcast(broadcastToUI);
    }

    private void sendConnectionStateChanged(int connectionCode){
        synchronized (bleConnectStateLock){
            Bundle connectionStateBundle = new Bundle();
            connectionStateBundle.putInt("connection_status",connectionCode);
            sendBroadcastToUI(ServiceMessage.MSG_CONNECTION_STATUS, connectionStateBundle);
            currentRecordedConnectionStatus = connectionCode;
        }
    }

    /**********
     * leConnect SendData
     ***********/
    //TODO: Make Current SendData a Queue Manager, make a True SEND_DATA thread which send data with interval.
    //Queue Manager: Notify Thread manager when recevice message, and add the message into the Queue.
    //Thread Manager: Check if the sendData Thread is working, if not working, start
    //SendData Thread: Get data in Queue with Interval. if data not null, send it; if data is null, stop self.
    private boolean sendData(byte[] data){
        Log.d(TAG, "sendData mConnector.write(data  , isResponse) send true mWritten reset to false "+" data "+ ArrayUtil.toHex(data));
        if(mLeManager.isConnected()){
            if (mLeManager.write(data)) {
                mWritten = false;
                return true;
            }else{
                sendMsgFailCount++;
                Log.d(TAG, "sendData mConnector.write(data) return false failCount = "+sendMsgFailCount );

            }
        }
        return false;
    }

    /**********
     * leConnect/leManager Callbacks
     ***********/
    @Override
    public void onServicesDiscovered(int status){
        Log.d(TAG, "onServicesDiscovered: "+"get into this Funcion");
        if (mLeManager.setWriteCharacteristic(Constants.OTA_SERVICE_OTA_UUID, Constants.OTA_CHARACTERISTIC_OTA_UUID)) {
            if (!mLeManager.requestMtu(DEFAULT_MTU)) {
                Log.d(TAG, "requestMtu result false");
                enableCharacteristicNotification();
            } else {
                Log.d(TAG, "requestMtu DEFAULT_MTU = "+DEFAULT_MTU);
//                updateInfo(R.string.configing_mtu);
            }
        } else {
            Log.d(TAG, "onServicesDiscovered error service");
            mCmdHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mLeManager.close();
                }
            },1000);
        }
    };

    private void enableCharacteristicNotification() {
        if (!mLeManager.enableCharacteristicNotify(Constants.OTA_SERVICE_OTA_UUID, Constants.OTA_CHARACTERISTIC_OTA_UUID, Constants.OTA_DESCRIPTOR_OTA_UUID)) {
            Log.d(TAG, "enableCharacteristicNotification false ");
            mLeManager.refresh();
            mLeManager.close();
        }else{
            Log.d(TAG, "enableCharacteristicNotification return true");
        }
    }

    @Override
    public void onCharacteristicNotifyEnabled(int status){
        Log.d(TAG, "onCharacteristicNotifyEnabled: Success");
        sendConnectionStateChanged(CONNECTION_STATUS_CONNECTED);
        mCmdHandler.postDelayed(new Runnable() {
            @Override
            public void run(){
                synchronized (bleConnectStateLock){
                    waitingGeneralStatus=true;
                    sendData(CmdLib.CMD_GET_GENERAL_STATUS);
                }
            }
        },500);
    }

    @Override
    public void onWritten(int status){

    }

    @Override
    public void onMtuChanged(int status, int mtu){
        if (status == LeConnector.LE_SUCCESS) {
            Log.d(TAG, "onMtuChanged mtu = "+ mtu);
            mMtu = mtu;
            //updateInfo(R.string.config_mtu_successfully);
        } else {
            Log.d(TAG, "onMtuChanged false status = "+status);
            //updateInfo(R.string.config_mtu_failed);
        }
        enableCharacteristicNotification();
    }

    @Override
    public void onConnectionStateChanged(boolean connected){
        // if the connection state changed from disconnected to connected, start trying to discover the service.
        // if the connection lost, or "disconncted" is triggered, try to find out the reason
        //      if the device is previously connected but it become disconnected, try to reconnect.
        //      if the previous state was not "connected", it might be caused by the attemt to connecting to a NOTEXIST device. So try to start Scanning if this kind of connection is failed.
        Log.d(TAG, "onConnectionStateChanged: "+connected);
        if(connected){
            if (!mLeManager.discoverServices()) {
                Log.d(TAG, "discoverServices reture false so bad");
                mLeManager.close();
                sendConnectionStateChanged(CONNECTION_STATUS_DISCONNECTED);
            }else{
                Log.d(TAG, "discoverServices reture true but we need to wait the callback");
                editor.putString("evoco_one_device_address",mDeviceAddress);
                editor.commit();
            }
        }else{
            if(activeDisconnect){
                activeDisconnect=false;
                sendConnectionStateChanged(CONNECTION_STATUS_DISCONNECTED);
            }
            else if (currentRecordedConnectionStatus == CONNECTION_STATUS_CONNECTED && !activeDisconnect){
                Log.d(TAG, "onConnectionStateChanged: get disconnected");
                sendConnectionStateChanged(CONNECTION_STATUS_DISCONNECTED);
                mCmdHandler.postDelayed(()->{connectPastDevice();},1000);
            }else{
                if(waitingConnectSavedDeviceFlag){
                    Log.d(TAG, "onConnectionStateChanged: connect saved device  failed, start scanning");
                    connect();
                    waitingConnectSavedDeviceFlag = false;
                }
            }

        }
    }



    @Override
    public void onReceive(byte[] data){
        if(waitingFWMasterLoggingSyncEnd || waitingFWSlaveLoggingSyncEnd){
            readReceivedLoggingData(data);
        }
        if(waitingGeneralStatus){
            waitingGeneralStatus = false;
            Bundle mDataBundle = new Bundle();
            mDataBundle.putInt("master_battery_level", data[0]);
            mDataBundle.putInt("slave_battery_level", data[1]);
            mDataBundle.putInt("master_volume_level", data[2]);
            mDataBundle.putInt("slave_volume_level", data[3]);
            mDataBundle.putInt("master_preset", data[4]);
            mDataBundle.putInt("slave_preset", data[5]);
            mDataBundle.putInt("denoise_mode", data[6]);
            mDataBundle.putInt("streaming_mode", data[7]);
            mDataBundle.putInt("is_music_playing", data[8]);
            sendBroadcastToUI(ServiceMessage.MSG_GENERAL_INFO,mDataBundle);
            Log.d("onReceive","data.Master_battery:" + data[1]);
        }else if(waitingHardwareStatus){
            waitingHardwareStatus = false;
            Bundle mDataBundle = new Bundle();
            mDataBundle.putInt("generation_number",data[0]);
            mDataBundle.putInt("version_number",data[1]);
            mDataBundle.putInt("build_version",data[2]);
            mDataBundle.putInt("left_gaintable_version",data[3]);
            mDataBundle.putInt("right_gaintable_version",data[4]);
            sendBroadcastToUI(ServiceMessage.MSG_FIRMWARE_INFO, mDataBundle);
            Log.d(TAG, "data.LeftGaintableVersion: "+data[3]);
        }
    }

    /**********
     * OTA PART
     **********/

    public void startOta() {
        activeDisconnect = true;
        disconnect();
        ota.initConfig(this);
        mDelayHandler.postDelayed(()->{
            ota.controlStartOta();
        },3000);
    }

    @Override
    public void onOtaProgressUpdated(int progress) {
        Bundle mBundle = new Bundle();
        mBundle.putInt(OTA_PROGRESS_FIELD,progress);
        sendBroadcastToUI(ServiceMessage.MSG_OTA_PROGRESS,mBundle);
    }

    @Override
    public void onOtaStateChanged(int state) {
        Bundle mBundle = new Bundle();
        mBundle.putInt(OTA_STATUS_FIELD,state);
        sendBroadcastToUI(ServiceMessage.MSG_OTA_STATUS,mBundle);
    }

    public void destroyOta(){
        ota.controlStopOta();
    }

    /**********
     * Firmware Logging
     **********/
    private void notifyAppBackground(){
        sendData(CmdLib.CMD_NOTIFY_APP_BACKGROUND);
        waitingFWMasterLoggingSyncEnd = true;
    }

    private void readReceivedLoggingData(byte[] data){
        // 0x0f0f0f0f means end of the Master logs
        // 0x0e0e0e0e means end of the slave logs
        if(waitingFWMasterLoggingSyncEnd){
            if(ArrayUtil.isEqual(new byte[]{(byte) 0x0f, 0x0f, 0x0f, 0x0f}, data)){
                waitingFWMasterLoggingSyncEnd = false;
                waitingFWSlaveLoggingSyncEnd = true;
            }else{
                FileUtils.updateMainPath(this);
                String path = FileUtils.getFolderPath() + "LOG/";
                FileUtils.isExist(path);
                String filesName = "master.log";
                File file = new File(path + filesName);
                try{
                    if(!file.exists()){
                        file.createNewFile();
                    }
                    FileOutputStream stream = new FileOutputStream(file, true);
                    stream.write(data);
                    stream.close();
                }
                catch (IOException e){
                    e.printStackTrace();
                }
            }
        }else if(waitingFWSlaveLoggingSyncEnd){
            if(ArrayUtil.isEqual(new byte[]{(byte) 0x0e, 0x0e, 0x0e, 0x0e}, data)){
                waitingFWSlaveLoggingSyncEnd = false;
                mCmdHandler.sendEmptyMessage(CMD_SEND_FW_LOG);
            }else{
                FileUtils.updateMainPath(this);
                String path = FileUtils.getFolderPath() + "LOG/";
                FileUtils.isExist(path);
                String filesName = "slave.log";
                File file = new File(path + filesName);
                try{
                    if(!file.exists()){
                        file.createNewFile();
                    }
                    FileOutputStream stream = new FileOutputStream(file, true);
                    stream.write(data);
                    stream.close();
                }
                catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
        sendData(CmdLib.CMD_ACKNOWLEDGE_RECEIVE);
    }

    private void uploadingLog(String mToken, boolean uploadingMaster){
        OkHttpClient client = new OkHttpClient();
        if (mToken == null){
            Log.d(TAG, "startUploadingFwLog: Token not valid");
            return;
        }
        backendLibrary mBackendLib = new backendLibrary();
        String url = mBackendLib.getFullUrl(mBackendLib.FIRMWARE_LOG_UPLOAD,mToken);
        Log.d(TAG, "uploadingLog: url is " + url);
        String filename;
        if (uploadingMaster){
            filename = "master.log";
        }else{
            filename = "slave.log";
        }
        Log.d(TAG, "startUploadingFwLog: Start Uploading");

        File masterLog = new File(FileUtils.getFolderPath() + "LOG/" + filename);
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("fw_logging",filename,
                        RequestBody.create(MediaType.parse("multipart/form-data"),masterLog))
                .build();
        Request mRequest = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        Call call = client.newCall(mRequest);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(TAG, "onFailure: "+e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d(TAG, "onResponse: " + response);
            }
        });
    }

    private void startUploadingFwLog(boolean uploadingMaster) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser mUser = mAuth.getCurrentUser();
        mUser.getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
            @Override
            public void onComplete(@NonNull Task<GetTokenResult> task) {
                if  (task.isSuccessful()){
                    String mToken = task.getResult().getToken();
                    uploadingLog(mToken, uploadingMaster);
                }else{
                    Log.d(TAG, "Get Token onComplete: Get Token Failed");
                }
            }
        });

    }


    /**********
     * Commands
     **********/
    public class CmdHandler extends Handler
    {
        public CmdHandler(Looper looper)
        {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what) {
                case CMD_STOP_SCAN:
                    Log.d(TAG, "handleMessage: NO_DEVICE_FOUND");
                    sendConnectionStateChanged(CONNECTION_STATUS_DISCONNECTED);
                    mScanner.stopScan();
                    mScanner.close();
                    break;
                case CMD_START_SCAN:
                    Log.d(TAG, "handleMessage: START_SCANNING");
                    sendConnectionStateChanged(CONNECTION_STATUS_SCANNING);
                    mScanner.startScan(EvocoOneBleService.this);
                    break;
                case CMD_SEND_FW_LOG:
                    startUploadingFwLog(true);
                    mCmdHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startUploadingFwLog(false);
                        }
                    },5000);
                    break;
                default:
                    break;
            }
        }
    }

}
